import express from 'express';
import chalk from 'chalk';

import contextMiddleware from './contextMiddleware';
import render from './render';

const app = express();

app.use('/admin', express.static('build', { maxAge: '200d' }));
app.use('/admin/assets', express.static('assets', { maxAge: '200d' }));

app.use(contextMiddleware);

app.get('*', render);

app.on('mount', () => {
	const { Console } = require('console');
	const startCons = new Console(process.stdout, process.stderr);
	startCons.log(`[${chalk.gray(new Date(Date.now()).toTimeString().substr(0, 8))}] App is available at ${chalk.cyan.bold('%s')}`, app.mountpath);
});

export default app;
