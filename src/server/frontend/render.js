// @flow

import Helmet from 'react-helmet';
import React from 'react';
import serialize from 'serialize-javascript';
import { renderToStaticMarkup, renderToString } from 'react-dom/server';
import { StaticRouter, matchPath } from 'react-router-dom';
import sjwt from 'jsonwebtoken';

import config from '../config';
import configureStore from '../../common/configureStore';
import Html from './Html';

import { routes } from '../../common/routes';
import { composeAdditionalStaticScripts } from '../../common/lib/additionalScripts';
import createInitialState from './createInitialState';

// createInitialState loads files, so it must be called once.
const initialState = createInitialState();
const currentEnv = config.localMode ? 'local' : config.Env || 'dev';

const createStore = (localsConfig, jwt) => {
	const modifiedConfig = {
		config: {
			env: currentEnv,
			appName: initialState.config.appName,
			appVersion: initialState.config.appVersion,
			baseUrl: localsConfig.baseUrl,
			favicon: localsConfig.favicons,
			logo: localsConfig.logo,
			siteName: localsConfig.siteName,
			imageserver: config.imageserver[currentEnv],
			api: config.api[currentEnv],
			radioContainers: localsConfig.radioContainers,
			listingsTexts: localsConfig.listingsTexts,
			stepsTexts: localsConfig.stepsTexts,
			admin: localsConfig.admin[currentEnv],
			mapTexts: localsConfig.mapTexts,
			editListingTexts: localsConfig.editListingTexts,
			mediaTexts: localsConfig.mediaTexts,
			contactsTexts: localsConfig.contactsTexts,
			preview: localsConfig.preview,
			previewUrl: localsConfig.previewUrl[currentEnv],
			jwt,
			citiesArray: localsConfig.citiesArray,
			localMode: config.localMode,
			tileLayer: config.locationhierarchy.tilelayer[currentEnv],
		},
	};

	return configureStore({
		initialState: {
			...initialState,
			...modifiedConfig,
		},
		platformReducers: {},
		platformStoreEnhancers: {},
	});
};

const fetchDataAsync = async (dispatch, route, uri, configs, jwt) => {
	const { component, fetchParams } = route;

	const promises = ((component && component.fetchActions) || [])
		.map((action) => {
			const dispatchPromises = dispatch(action({ ...fetchParams(uri, configs, jwt) }));

			return 'payload' in dispatchPromises && 'promise' in dispatchPromises.payload
				? dispatchPromises.payload.promise : dispatchPromises;
		});

	await Promise.all(promises);
};

const renderRoutedBody = (req, store, app) => {
	const context = {};

	const html = renderToString(
		<StaticRouter context={context} location={req.url}>
			{app({ store })}
		</StaticRouter>,
	);

	const helmet = Helmet.rewind();
	return { html, helmet };
};

const renderScripts = (
	state,
	manifestJsFilename,
	appJsFilename,
) => `${state
	? `<script>window.__INITIAL_STATE__ = ${serialize(state)};</script>`
	: ''}
	${manifestJsFilename
		? `<script src="${manifestJsFilename}"></script>`
		: ''}
	${appJsFilename
		? `<script src="${appJsFilename}"></script>`
		: ''}`;

const renderHtml = (state, body, assets, extras_head, extras_footer, trace) => {
	if (!config.isProduction) {
		global.webpackIsomorphicTools.refresh();
	}
	let trace_str = '';
	if (trace && trace instanceof Object) {
		trace.endTrace();
		trace_str = trace.renderHtml();
	}
	const footer = `<footer id="footer">
				<div class="legal noselect">
					Casa.it S.r.l. - Sede Legale Milano, Via Borsi, 9 -20143 Milano - C.F. e P.I. 02296530260
				</div>
			</footer>`;
	const scripts = renderScripts(state, assets.manifestJs, assets.appJs);
	const html = renderToStaticMarkup(
		<Html {...{
			appCssFilename: assets.appCss,
			bodyHtml: `<div id="app">${body.html}</div>${footer}${scripts}<!--\n\t${trace_str}\n-->`,
			helmet: body.helmet,
			// flow-disable-next-line
			styleElement: body.styleElement,
			additionalHead: extras_head != null ? extras_head : '',
			additionalFooter: extras_footer != null ? extras_footer : '',
			gtmDataLayer: ''
		}} />,
	);
	return `<!DOCTYPE html>${html}`;
};

const getAssets = (entryName) => {
	const {
		javascript,
		javascript: {
			manifest: manifestJs,
		},
		styles,
	} = global.webpackIsomorphicTools.assets();

	return {
		manifestJs,
		appJs: javascript[entryName],
		appCss: styles[entryName] || styles.search,
	};
};

const render = async (req: Object, res: Object, next: Function) => {

	let projectCookie = '';
	try {
		projectCookie = req.headers.cookie.split(';').filter(o => o.indexOf('projectCookie') !== -1)[0].trim().replace('projectCookie=', '');
	} catch (e) {
		if (currentEnv === 'prod') return res.redirect(301, config.admin.url[currentEnv]);
		projectCookie =
		'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxNiwicmVhbG0iOm51bGwsInVzZXJuYW1lIjoiMTYiLCJlbWFpbCI6ImluZm9AaXN0dWRpb3h4eHh4Lml0IiwiZW1haWxWZXJpZmllZCI6dHJ1ZSwicGhvbmUiOiIwNjc4MzQ0NSIsImN1c3RvbWVyVHlwZUlkIjozLCJjaXR5SWQiOiIwNTgwOTEiLCJjaXR5IjoiUm9tYSIsImFnZW5jeUlkIjoxNiwiYWdlbmN5TmFtZSI6IklNTU9CSUxJQVJFIFNUVURJTyB4eHh4IiwiYWdlbmN5TG9nb1NtYWxsIjoibG9nby8yOTgxNWE5MmM1NjI4MGM4ZDA4MjM3NDQ5MjJkNmQzOSIsImFnZW5jeUxvZ28iOiJsb2dvLzI5ODE1YTkyYzU2MjgwYzhkMDgyMzc0NDkyMmQ2ZDM5IiwiYWdlbmN5TG9nb0xhcmdlIjoibG9nby8yOTgxNWE5MmM1NjI4MGM4ZDA4MjM3NDQ5MjJkNmQzOSIsIm9mZmVyU3RhdGUiOjMsImZsYWdBcHBseUxpc3RpbmdMaW1pdCI6dHJ1ZSwibGlzdGluZ0xpbWl0IjowLCJ3b3JraW5nU2luY2UiOm51bGwsImNhc2FpdFNpbmNlIjpudWxsLCJzdWJzY3JpcHRpb25UeXBlSWQiOjcsInRlcm1PZlVzZUlEIjpudWxsLCJlbnVtQmxvY2tSZWFzb24iOm51bGwsImFnZW5jeVN0YXR1cyI6NywibGF0aXR1ZGUiOjQxLjg5NTQ2NTYsImxvbmdpdHVkZSI6MTIuNDgyMzI0MywiaW5zZXJ0Q29kIjoibG9vcGJhY2siLCJpbnNlcnREdCI6IjIwMTgtMDgtMDFUMDk6MDA6MDAuMDAxWiIsInVwZGF0ZUNvZCI6bnVsbCwidXBkYXRlRHQiOm51bGx9LCJpYXQiOjE1NTk1NTMzNDMsImV4cCI6MTU2MjE0NTM0M30.stMMWyyb_TkcQP0kUuPunS-colcKnILMm7uXo9Jr6O8';
	}

	const pathArray = req.url.split('/');

	if (pathArray.length === 4 && pathArray[3] === 'step-5') {
		const { user } = sjwt.decode(projectCookie);
		user.isPhoneVerified = true;
		const { isPhoneVerified } = user;
		console.log('isPhoneVerified', isPhoneVerified);
		if (isPhoneVerified === false) res.redirect(301, `/listing/${pathArray[2]}/step-4`);
	}

	const store = createStore(res.locals.config, projectCookie);

	if (res.locals.config.baseUrl === '/') {
		return res.redirect(301, '/listings');
	}

	try {
		let matchedRoute: Object = {};

		routes.some((route) => {
			// use `matchPath` here
			// const match = matchPath(req.url, route);
			const match = matchPath(res.locals.config.baseUrl, route);
			if (match) {
				matchedRoute = route;
			}
			return match;
		});

		// refresh routes because some don't process always
		routes.map((route) => {
			// const match = matchPath(req.url, route);
			const match = matchPath(res.locals.config.baseUrl, route);
			return match;
		});

		await fetchDataAsync(
			store.dispatch,
			matchedRoute,
			req.url,
			res.locals.config,
			projectCookie,
		);

		const selectedState = store.getState()[matchedRoute.assets];
		const body = renderRoutedBody(req, store, matchedRoute.component);
		const html = renderHtml(
			store.getState(),
			body,
			getAssets(matchedRoute.assets),
			composeAdditionalStaticScripts(matchedRoute.headScripts,
				{
					selectedState,
					googleAnalyticsId: 'GTM-K2D9',
					config: res.locals.config,
				}),
			composeAdditionalStaticScripts(matchedRoute.footerScripts,
				{
					selectedState,
					config: res.locals.config,
				}),
			res.trace,
		);
		return res.status(200).send(html);
	} catch (error) {
		console.log('RENDER', error);
		return next(error);
	}
};

export default render;
