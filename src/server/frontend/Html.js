// @flow
import React from 'react';

const Html = (
	{
		appCssFilename,
		bodyHtml,
		helmet,
		styleElement,
		additionalHead,
		additionalFooter,
		gtmDataLayer,
	}: {
		appCssFilename: string,
		bodyHtml: string,
		helmet: Object,
		styleElement: string,
		additionalHead: string,
		additionalFooter: string,
		gtmDataLayer: string,
	}
) => (
	<html {...helmet.htmlAttributes.toComponent()}>
		<head>
			{/* eslint-disable-next-line */}
			<style dangerouslySetInnerHTML={{ __html: styleElement }} />
			{helmet.title.toComponent()}
			{helmet.base.toComponent()}
			{helmet.meta.toComponent()}
			{helmet.link.toComponent()}
			{helmet.script.toComponent()}
			{appCssFilename && <link href={appCssFilename} rel="stylesheet" />}
			{additionalHead}
			{gtmDataLayer}
		</head>
		{/* eslint-disable-next-line */}
		<body dangerouslySetInnerHTML={{ __html: bodyHtml }} />
		{additionalFooter}
	</html>
);

export default Html;
