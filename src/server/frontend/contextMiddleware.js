/* eslint-disable import/no-dynamic-require */
import chalk from 'chalk';
import config from '../config';

const getDomainConfiguration = (hostname, url) => {
	const currentDomainKey = 'default';
	const domainConfiguration = config.default;
	const shouldRedirectLocale = false;
	const unknownLocale = false;

	return {
		...domainConfiguration,
		shouldRedirectLocale,
		unknownLocale,
		currentDomainKey,
		selectedLocale: 'it',
		baseUrl: url.replace(/^\/..\//, '/'),
	};
};

export default function contextMiddleware(req, res, next) {
	console.log('contextMiddleware', req.headers.cookie);
	console.log(chalk.bgCyan(chalk.black(`${'='.repeat(30)} START NEW REQUEST ${'='.repeat(30)}`)));
	console.log(`host: ${chalk.bold(chalk.blue(req.hostname))} - url: ${chalk.bold(chalk.blue(req.url))}`);
	res.locals.config = getDomainConfiguration(req.hostname, req.url);
	next();
}
