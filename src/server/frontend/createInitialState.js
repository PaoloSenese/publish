import config from '../config';
import configReducer from '../../common/redux/config/reducer';

const createInitialState = () => ({
	config: {
		...configReducer(),
		appName: config.appName,
		appVersion: config.appVersion,
	},
});

export default createInitialState;
