class TraceRequest {
	constructor() {
		this.trace = {
			req: {
				id: Math.floor(100000000 + Math.random() * 900000000),
			},
			date: new Date().toISOString(),
			start_time: new Date().getTime(),
			start_time2: process.hrtime(),
			qtime: new Date().getTime(),
			qtime2: process.hrtime(),
		};
	}

	parseHrtimeToSeconds() {
		const hrtime = this.trace.start_time2;
		return (hrtime[0] + (hrtime[1] / 1e9)).toFixed(3);
	}

	endTrace() {
		this.trace.qtime = (new Date().getTime()) - this.trace.start_time;
		this.trace.qtime2 = this.parseHrtimeToSeconds(process.hrtime());
		console.log(`TraceRequest :: endTrace ReqId ${this.trace.req.id} Date ${this.trace.date} Took in ${this.trace.qtime2} seconds`);
	}

	renderHtml() {
		const data = [];
		Object.keys(this.trace.req).map((k) => {
			data.push(`RK ${k} - V ${this.trace.req[k]}`);
		});
		Object.keys(this.trace).map((k) => {
			if (k !== 'req') {
				data.push(`R ${k} - V ${this.trace[k]}`);
			}
		});
		return data.join('\n\t');
	}
}


export default function tracerMiddleware(req, res, next) {
	res.trace = new TraceRequest(req);
	return next();
}
