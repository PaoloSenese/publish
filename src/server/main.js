/* eslint-disable import/no-extraneous-dependencies */
import express from 'express';
import chalk from 'chalk';

import errorHandler from './lib/errorHandler';
import api from './proxy';
import services from './services';
import config from './config';

import tracer from './tracerMiddleware';
import frontend from './frontend';

const app = express();

// status health check endpoint
app.get('/status', (req, res) => {
	res.send('WORKING');
});

app.get('/robots.txt', (req, res) => {
	res.type('text/plain');
	res.send('User-agent: *\nDisallow: /');
});

// handler to expose services to the web
app.use('/admin/services/v1', services);
// internal proxy to APIs
app.use('/admin/api/v1', api);

// middleware trace request log and performance
app.use(tracer);

app.use(frontend);
app.use(errorHandler);

const { port } = config;

app.listen(port, () => {
	const { Console } = require('console');
	const startCons = new Console(process.stdout, process.stderr);
	startCons.log(`[${chalk.gray(new Date(Date.now()).toTimeString().substr(0, 8))}] Server started at port ${chalk.blue.bold('%d')}`, port);
});
