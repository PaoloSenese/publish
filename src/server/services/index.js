import express from 'express';
import bodyParser from 'body-parser';
import chalk from 'chalk';

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use('/whoam', (req, res) => {
	res.status(200).format({
		json() {
			res.send({
				host: req.hostname,
				uri: req.url,
			});
		},

		html() {
			res.send(`<h1>Welcome</h1>\nHost:${req.hostname}\n<p>This is the service endpoint responding in <strong>HTML</strong></p>`);
		},

		default() {
			res.send(`Host:${req.hostname}\nUri:${req.url}`);
		},
	});
});

app.on('mount', () => {
	const { Console } = require('console');
	const startCons = new Console(process.stdout, process.stderr);
	startCons.log(`[${chalk.gray(new Date(Date.now()).toTimeString().substr(0, 8))}] Api is available at ${chalk.cyan.bold('%s')}`, app.mountpath);
});

export default app;
