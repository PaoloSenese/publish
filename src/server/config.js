import nconf from 'nconf';
import fs from 'fs';
import path from 'path';

const isProduction = process.env.NODE_ENV === 'production';

// based on http://www.codedependant.net/2015/01/31/production-ready-node-configuration/

const config = {
	isProduction,
	googleAnalyticsId: 'UA-XXXXXXX-X',
	googleMapsId: 'UA-XXXXXXX-X',
	port: process.env.PORT || 3002,
	localMode: process.env.LOCAL_MODE || false,
	conf: './configurations',
};

let startup = nconf
	.argv()
	.env({ separator: '__' })
	.defaults(config);
// get a conf
const configFile = path.resolve(startup.get('conf') || 'project.json');

// purge the start up config
startup.remove('env');
startup.remove('argv');
startup.remove('defaults');
startup = null;

let conf = nconf
	.overrides({ /* something that must always be this way */
		localMode: process.env.LOCAL_MODE && process.env.LOCAL_MODE === 'true',
		appName: require('../../package.json').name,
		appVersion: '1.0',
	})
	.argv()
	.env({ separator: '__' });

if (fs.existsSync(configFile)) {
	if (fs.statSync(configFile).isDirectory()) {
		// if it is a directory, read all json files
		fs.readdirSync(configFile)
			.filter(file => (/\.json$/).test(file))
			.sort((fileA, fileB) => fileA < fileB)
			.forEach((file) => {
				const filepath = path.normalize(path.join(configFile, file));
				conf = conf.file(file, filepath);
			});
	} else {
		// if it is a file, read the file
		conf = conf.file(configFile);
	}
}
// set up defaults
conf.defaults(config);

export default nconf.get();
