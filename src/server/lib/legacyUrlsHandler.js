export default function handler({ hostname, path, query }) {
	if (hostname === 'isNotHandledAnyMore.lu') {
		return {
			code: 404,
		};
	}

	if (path === '/it/should/redirect/') {
		return {
			code: 301,
			url: `/${query ? `?${query}` : ''}`,
		};
	}

	return {
		code: 200,
	};
}
