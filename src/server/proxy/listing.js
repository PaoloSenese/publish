// @flow
import express from 'express';
import request from 'superagent';
import moment from 'moment';
import sjwt from 'jsonwebtoken';
import crypto from 'crypto-js';
import multer from 'multer';

import config from '../config';
import { ListingModel } from './models/listingModel';
import ThankYouModel from './models/thankYouModel';
import { replaceVar, flatten } from '../../common/helpers/utils';
import { decrypt } from './helpers/var';

const router = express.Router();
const currentEnv = config.localMode ? 'local' : config.Env || 'dev';
const hostname = config.publishapi.hostname[currentEnv];
const { path } = config.publishapi;
const hierarchyHost = config.locationhierarchy.hostname[currentEnv];
const { hierarchypath } = config.locationhierarchy;
const securityKey = config.securityKey[currentEnv];
const imageserverupload = config.imageserverupload[currentEnv];

router.route('/').post((req, res) => {
	const { jwt, uri, id } = req.body;
	const { user } = sjwt.decode(decrypt(jwt));

	if (id === 'new') {
		const dataObject = {
			geolocation: {
				suburb: user.city,
				townId: user.cityId,
				lat: user.latitude,
				lng: user.longitude,
				coordinateEnabled: true,
				isAddressVisible: true,
			},
			gardenTypeId: '-1',
			media: [],
		};

		const result = new ListingModel(
			jwt,
			uri,
			dataObject,
		).getListingRecord();

		res.status(200).send(Object.assign({}, result)).end();
	} else {
		const url = `${replaceVar(hostname + path.listing, { listingId: id })}`;
		console.log(url);

		request.get(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);
				const dataObject = response.data.projectProfiles[0];

				new ListingModel(
					jwt,
					uri,
					dataObject,
				).getAsyncListingRecord().then((x) => {
					res.status(200).send(Object.assign({}, x)).end();
				});

			} catch (e) {
				console.log(e);
				const dataObject = {
					geolocation: {
						suburb: user.city,
						townId: user.cityId,
						lat: user.latitude,
						lng: user.longitude,
					},
				};

				const result = new ListingModel(
					jwt,
					uri,
					dataObject,
				).getListingRecord();

				res.status(400).send(result).end();
			}

		});
	}
});

router.route('/savelisting').post((req, res) => {
	const { jwt, dataObject } = req.body;
	const url = dataObject.projectProfileId ? `${replaceVar(hostname + path.listing, { listingId: dataObject.projectProfileId })}` : `${hostname}${path.listings}`;
	const flatDataObject = flatten(dataObject);
	console.log(url);

	if (dataObject.projectProfileId) {
		request.put(url).send(flatDataObject).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);

				if (resReq.statusCode === 200) {
					res.status(200).send({
						response: {
							succeed: true,
							data: response,
							message: 'Dati salvati con successo'
						},
					}).end();
				} else {
					console.log('Error editing listing: ', response.error.message.custom);
					res.status(resReq.statusCode).send({ response: { succeed: false, message: 'Errore nel salvataggio dei dati' } }).end();
				}
			} catch (e) {
				console.log('Error editing listing: ', e);
				res.status(400).send({ response: { succeed: false, message: 'Errore nel salvataggio dei dati' } }).end();
			}
		});
	} else {
		request.post(url).send(flatDataObject).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);
				if (resReq.statusCode === 200) {
					res.status(200).send({
						response: {
							succeed: true,
							data: response,
							message: 'Dati salvati con successo'
						},
					}).end();
				} else {
					console.log('Error inserting listing: ', response.error.message);
					res.status(resReq.statusCode).send({ response: { succeed: false, message: 'Errore nel salvataggio dei dati' } }).end();
				}
			} catch (e) {
				console.log('Error inserting listing: ', e);
				res.status(400).send({ response: { succeed: false, message: 'Errore nel salvataggio dei dati' } }).end();
			}
		});
	}
});

router.route('/buy').post((req, res) => {
	res.status(200).send({
		response: {
			succeed: true,
			message: 'Dati salvati con successo'
		},
	}).end();
});

const upload = multer({
	storage: multer.memoryStorage(),
	limits: { fileSize: 10485760 },
});

router.post('/uploadmedia', upload.single('uploadMedia'), (req, res) => {
	const {
		id, jwt, width, height, size, imageType, prefix,
	} = JSON.parse(req.query.data);
	const { file } = req;

	const fakeName = moment(new Date()).format('YYYY-MM-DDTHH:mm:ss');

	let hash = crypto.MD5(crypto.lib.WordArray.create(req.file.buffer));
	hash = crypto.enc.Base64.stringify(hash);

	let hmac = crypto.HmacSHA1(fakeName, securityKey);
	hmac = crypto.enc.Base64.stringify(hmac);
	hmac = hmac.replace(/\+/g, '-').replace(/\//g, '_');

	request.get(`${hostname}${path.images}${id}`).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReqa, resReqa) => {
		try {
			const response = JSON.parse(resReqa.text);

			if (response.data.listingImages.filter(o => o.hash === hash).length === 0) {
				request.post(`${imageserverupload}image`)
					.set('Content-Type', 'image/jpg')
					.set('FakeName', fakeName)
					.set('Prefix', prefix)
					.set('Key', hmac)
					.send(file.buffer)
					.end((err, resReq) => {

						if (err) {
							console.log('Error uploading image', err);

							res.status(400).send({
								response: {
									succeed: false,
									errcode: 1,
									errmsg: 'Errore nel caricamento dell\'immagine',
									error: null,
									data: null,
								},
							}).end();
						} else {

							request.post(`${hostname}${path.addmedia}${id}/media`)
								.send({
									listingId: id,
									imageType,
									isPrincipal: false,
									url: resReq.body.image.key,
									dimension: size,
									width,
									height,
									hash,
								})
								.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
								.end((errReqx, resReqx) => {
									const responsex = JSON.parse(resReqx.text);

									res.status(200).send({
										response: {
											succeed: true,
											errcode: 0,
											errmsg: null,
											error: null,
											data: responsex,
										},
									}).end();
								});
						}
					});
			} else {
				console.log('Error uploading image');
				res.status(400).send(Object.assign({}, {
					response: {
						succeed: false,
						errcode: 1,
						errmsg: 'l\'immagine esiste',
						error: null,
						data: null,
					},
				})).end();
			}
		} catch (e) {
			console.log('Error uploading image', e);
			res.status(400).send(Object.assign({}, {
				response: {
					succeed: false,
					errcode: 2,
					errmsg: 'Errore nel caricamento dell\'immagine',
					error: null,
					data: null,
				},
			})).end();
		}
	});

});

router.route('/searchtown').post((req, res) => {
	const { search, match } = req.body;
	const url = match ? `${hostname}${path.searchtown}=${search}` : `${hostname}${path.searchtown}[like]=^${search.toLowerCase()
		.split(' ')
		.map(s => s.charAt(0).toUpperCase() + s.substring(1))
		.join(' ')}`;
	console.log(url);

	request.get(url).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			res.status(200).send({
				response: {
					succeed: true,
					data: response,
				},
			}).end();
		} catch (e) {
			res.status(400).send({
				response: {
					succeed: false,
				},
			}).end();
		}
	});
});

router.route('/deletemedia').post((req, res) => {
	const {
		listingId, imageId, jwt,
	} = req.body;
	const url = `${replaceVar(hostname + path.deletemedia, { listingId, imageId })}`;

	request.delete(url)
		.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
		.end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);

				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} catch (e) {
				res.status(400).send({
					response: {
						succeed: false,
					},
				}).end();
			}

		});

});

router.route('/deleteallmedia').post((req, res) => {
	const {
		listingId, imageType, jwt,
	} = req.body;
	const url = `${replaceVar(hostname + path.deleteallmedia, { listingId, imageType })}`;
	console.log(url);
	request.delete(url)
		.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
		.end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);

				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} catch (e) {
				res.status(400).send({
					response: {
						succeed: false,
					},
				}).end();
			}

		});

});

router.route('/movemedia').post((req, res) => {
	const {
		listingId, imageId, jwt, newPos,
	} = req.body;
	const url = `${replaceVar(hostname + path.movemedia, { listingId, imageId, newPos })}`;
	console.log(url);
	request.post(url)
		.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
		.end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);

				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} catch (e) {
				console.log('Error move media: ', e);
				res.status(400).send({
					response: {
						succeed: false,
					},
				}).end();
			}
		});

});

router.route('/rotatemedia').post((req, res) => {
	const {
		id, jwt, imageType, prefix, imgObject,
	} = req.body;

	request.get(req.body.url)
		.end((err, rex) => {

			const fakeName = moment(new Date()).format('YYYY-MM-DDTHH:mm:ss');

			let hash = crypto.MD5(crypto.lib.WordArray.create(rex.body));
			hash = crypto.enc.Base64.stringify(hash);

			let hmac = crypto.HmacSHA1(fakeName, securityKey);
			hmac = crypto.enc.Base64.stringify(hmac);
			hmac = hmac.replace(/\+/g, '-').replace(/\//g, '_');

			request.get(`${hostname}${path.images}${id}`).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
				try {
					const response = JSON.parse(resReq.text);

					if (response.data.listingImages.filter(o => o.hash === hash).length === 0) {

						request.post(`${imageserverupload}image`)
							.set('Content-Type', 'image/jpg')
							.set('FakeName', fakeName)
							.set('Prefix', prefix)
							.set('Key', hmac)
							.send(rex.body)
							.end((errReqy, resReqy) => {

								if (errReqy) {
									console.log('Error uploading image', errReqy);

									res.status(400).send({
										response: {
											succeed: false,
											errcode: 1,
											errmsg: 'Errore nel caricamento dell\'immagine',
											error: null,
											data: null,
										},
									}).end();
								} else {

									request.post(`${hostname}${path.addmedia}${id}/media`)
										.send({
											listingId: id,
											imageId: imgObject.imageId,
											imageType,
											url: resReqy.body.image.key,
											dimension: imgObject.dimension,
											width: imgObject.width,
											height: imgObject.height,
											hash,
										})
										.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
										.end((errReqx, resReqx) => {
											const responsex = JSON.parse(resReqx.text);

											res.status(200).send({
												response: {
													succeed: true,
													errcode: 0,
													errmsg: null,
													error: null,
													data: responsex,
												},
											}).end();
										});

								}
							});

					} else {
						console.log('Error uploading image');
						res.status(400).send(Object.assign({}, {
							response: {
								succeed: false,
								errcode: 1,
								errmsg: 'l\'immagine esiste',
								error: null,
								data: null,
							},
						})).end();
					}
				} catch (e) {
					console.log(`Error uploading image ${e}`);
					res.status(400).send(Object.assign({}, {
						response: {
							succeed: false,
							errcode: 2,
							errmsg: 'Errore nel caricamento dell\'immagine',
							error: null,
							data: null,
						},
					})).end();
				}
			});
		});
});

router.route('/getallmedia').post((req, res) => {
	const { id, jwt } = req.body;
	const url = `${hostname}${path.images}${id}`;
	console.log(url);

	request.get(url)
		.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
		.end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);

				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} catch (e) {
				res.status(400).send({
					response: {
						succeed: false,
					},
				}).end();
			}
		});
});

router.route('/getvacancies').post((req, res) => {
	console.log('getvacancies');
	res.status(200).send({
		response: {
			succeed: true,
			dataObject: [
				{
					id: 1,
					startDt: '05/04/2019',
					endDt: '05/07/2019',
					price: 500,
					frequency: 7,
					disponible: true,
					notes: 'questa è una nota'
				},
				{
					id: 2,
					startDt: '16/05/2019',
					endDt: '01/08/2019',
					price: 500,
					frequency: 1,
					disponible: false,
					notes: 'pure questa'
				}
			]
		},
	}).end();
});

router.route('/savevacancies').post((req, res) => {
	console.log('savevacancies');
	res.status(200).send({
		response: {
			succeed: true,
			message: ''
		},
	}).end();
});

router.route('/deletevacancies').post((req, res) => {
	const { jwt, index } = req.body;
	console.log('deletevacancies', jwt, index);

	let value = [
		{
			id: 1,
			startDt: '05/04/2019',
			endDt: '05/07/2019',
			price: 500,
			frequency: 7,
			disponible: true,
			notes: 'questa è una nota'
		},
		{
			id: 2,
			startDt: '16/05/2019',
			endDt: '01/08/2019',
			price: 500,
			frequency: 1,
			disponible: false,
			notes: 'pure questa'
		}
	];
	value = value.filter((o, i) => i !== index);
	console.log(index, value);

	res.status(200).send({
		response: {
			succeed: true,
			dataObject: value
		},
	}).end();
});

router.route('/sendsms').post((req, res) => {
	console.log('sendsms');
	res.status(200).send({
		response: {
			succeed: true,
			message: ''
		},
	}).end();
});

router.route('/verifycode').post((req, res) => {
	const { jwt, o } = req.body;
	console.log('verifycode', jwt, o);
	res.status(200).send({
		response: {
			succeed: true,
			message: ''
		},
	}).end();
});

router.route('/savecontacts').post((req, res) => {
	const { jwt, o } = req.body;
	console.log('savecontacts', jwt, o);
	res.status(200).send({
		response: {
			succeed: true,
			message: 'Dati salvati'
		},
	}).end();
});

router.route('/reverse').post((req, res) => {
	const { lat, lng, perimeter, type } = req.body;
	let queryString = perimeter === true ? '&perimeter=true' : '';
	queryString += type !== '' ? `&types=${type}` : '';
	const url = `${hierarchyHost + hierarchypath.reverse}/${lat},${lng}?site=it_casa${queryString}`;
	console.log(url);

	request.get(url)
		.end((errReq, resReq) => {
			try {
				const response = resReq.body.data;

				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} catch (e) {
				res.status(400).send({
					response: {
						succeed: false,
					},
				}).end();
			}
		});
});

router.route('/location').post((req, res) => {
	const { locationId, perimeter } = req.body;
	const queryString = perimeter === true ? '&perimeter=true' : '';
	const url = `${hierarchyHost + hierarchypath.location}/${locationId}?site=it_casa${queryString}`;
	console.log(url);

	request.get(url)
		.end((errReq, resReq) => {
			try {
				const response = resReq.body.data;

				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} catch (e) {
				res.status(400).send({
					response: {
						succeed: false,
					},
				}).end();
			}
		});
});

export default router;
