// @flow
import express from 'express';
import request from 'superagent';
import sjwt from 'jsonwebtoken';
import config from '../config';
import { DashboardsModel } from './models/dashboardModel';
import { ListingModel } from './models/listingModel';
import RequestsModel from './models/requestsModel';
import ThankYouModel from './models/thankYouModel';
import { mockData } from './mock/listingsMock';
import { requestsMock } from './mock/requestsMock';
import { replaceVar } from '../../common/helpers/utils';
import { decrypt } from './helpers/var';

const router = express.Router();
const currentEnv = config.localMode ? 'local' : config.Env || 'dev';
const hostname = config.publishapi.hostname[currentEnv];
const { path } = config.publishapi;

const buildQuery = (andCondition, name, value) => {
	switch (name) {
	case 'active':
		andCondition.push({ listingEnabled: value });
		break;
	case 'prod':
		andCondition.push({ productTypeId: value });
		break;
	case 'id':
		andCondition.push({ projectProfileId: value });
		break;
	default:
		andCondition.push(JSON.parse(`{ "${name}": "${value}" }`));
		break;
	}
	return andCondition;
};

router.route('/').post((req, res) => {

	const {
		jwt, uri,
	} = req.body;

	const { user } = sjwt.decode(jwt);
	let dataObject = mockData;

	if (uri === '/thank-you') {
		const result = new ThankYouModel(
			jwt,
			uri,
			{}
		).getThankYouRecord();

		res.status(200).send(Object.assign({}, result)).end();
	} else if (uri === '/requests') {

		const result = new RequestsModel(
			jwt,
			uri,
			requestsMock
		).getRequestsRecord();

		res.status(200).send(Object.assign({}, result)).end();
	} else if (uri.split('/').length === 3 && !Number.isNaN(Number(uri.split('/')[2]))) {
		const id = uri.split('/')[2];
		const url = `${replaceVar(hostname + path.listing, { listingId: id })}/leadsCount`;

		request.get(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);
				const { projectProfiles } = response.data;

				new ListingModel(
					jwt,
					uri,
					projectProfiles[0]
				).getListingWithLeadsRecord(id).then((x) => {
					res.status(200).send(Object.assign({}, x)).end();
				});
			} catch (e) {
				console.log(`Error loading listing details: ${id}`, e);
				const result = new ListingModel(
					jwt,
					uri,
					{
						geolocation: {
							suburb: user.city,
							townId: user.cityId,
							lat: user.latitude,
							lng: user.longitude,
						},
						media: [],
					}
				).getListingRecord();
				res.status(400).send(Object.assign({}, result)).end();
			}

		});
	} else {

		let url = '';
		const checkQuery = uri.split('?');

		const currentPage = uri.split('/').length === 3
			? (checkQuery.length > 1 ? uri.split('/')[2].replace('page-', '').replace(`? ${uri.split('?')[1]}`, '') : uri.split('/')[2].replace('page-', '')) : 1;

		let andCondition = [];

		if (checkQuery.length > 1) {
			if (uri.split('?')[1].indexOf('&') === -1) {
				const name = uri.split('?')[1].split('=')[0];
				const value = uri.split('?')[1].split('=')[1];
				andCondition = buildQuery(andCondition, name, value);
			} else {
				const list = uri.split('?')[1].split('&');

				list.map((item) => {
					const name = item.split('=')[0];
					const value = item.split('=')[1];
					andCondition = buildQuery(andCondition, name, value);
				});
			}
		}

		const query = {};
		if (andCondition.length > 0) {
			andCondition = buildQuery(andCondition, 'userId', user.id);
			query.and = andCondition;
		}

		url = `${hostname}${path.listings}/?filter=${JSON.stringify({
			where: query,
			order: 'lastModifiedDt desc',
			limit: 10,
			skip: ((parseInt(currentPage, 10) - 1) * 10),
		})}`;
		console.log('listings', url);

		request.get(url)
			.set('Authorization', `Bearer jwt ${decrypt(jwt)}`)
			.end((errReq, resReq) => {
				try {
					const response = JSON.parse(resReq.text);
					dataObject = response.data.projectProfiles;

					new DashboardsModel(
						jwt,
						uri,
						dataObject,
						{ total: response.data.total, count: response.data.count, currentPage }
					).getDashboardsRecord().then((x) => {
						res.status(200).send(Object.assign({}, x)).end();
					});
				} catch (e) {
					console.log('Error loading listings ', e);
					new DashboardsModel(
						jwt,
						uri,
						[],
						{}
					).getDashboardsRecord().then((x) => {
						res.status(200).send(Object.assign({}, x)).end();
					});
				}
			});
	}
});

router.route('/update').post((req, res) => {

	const { id, dataObject, jwt } = req.body;

	const url = `${replaceVar(hostname + path.listing, { listingId: id })}`;
	console.log(url);

	request.put(url).send(dataObject).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			if (resReq.statusCode === 200) {
				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} else {
				console.log('Error updating Listing', response.error);
				res.status(resReq.statusCode).send({ response: { succeed: false } }).end();
			}
		} catch (e) {
			console.log('Error updating Listing', e);
			res.status(400).send({ response: { succeed: false } }).end();
		}

	});
});

router.route('/getrequests').post((req, res) => {

	const { id, jwt } = req.body;
	const url = `${replaceVar(hostname + path.listingrequests, { listingId: id })}`;
	console.log(url);

	request.get(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			if (resReq.statusCode === 200) {
				res.status(200).send({
					response: {
						succeed: true,
						data: response.data.specificRequests,
					},
				}).end();
			} else {
				console.log('Error opening Requests', response.error);
				res.status(resReq.statusCode).send({ response: { succeed: false } }).end();
			}
		} catch (e) {
			console.log('Error opening Requests', e);
			res.status(400).send({ response: { succeed: false } }).end();
		}
	});
});

router.route('/getleads').post((req, res) => {
	const {
		id, jwt, startDt, endDt,
	} = req.body;
	const url = `${replaceVar(hostname + path.getleads, { listingId: id, startDt, endDt })}`;
	console.log(url);

	request.get(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			if (resReq.statusCode === 200) {
				res.status(200).send({
					response: {
						succeed: true,
						data: response.data,
					},
				}).end();
			} else {
				console.log('Error getting Leads', response.error);
				res.status(resReq.statusCode).send({ response: { succeed: false } }).end();
			}
		} catch (e) {
			console.log('Error getting Leads', e);
			res.status(400).send({ response: { succeed: false } }).end();
		}
	});
});

router.route('/activatelisting').post((req, res) => {
	const {
		jwt, listingId, depthId
	} = req.body;
	const url = replaceVar(hostname + path.activatelisting, { listingId, depthId });
	console.log(url);

	request.post(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			if (resReq.statusCode === 200) {
				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} else {
				console.log('Error activating Listing', JSON.stringify(response.error));
				res.status(resReq.statusCode).send({ response: { succeed: false } }).end();
			}
		} catch (e) {
			console.log('Error activating Listing', e);
			res.status(400).send({ response: { succeed: false } }).end();
		}
	});
});

router.route('/changestatus').post((req, res) => {
	const { jwt, id, action } = req.body;
	const url = replaceVar(hostname + path.changestatus, { listingId: id, action });
	console.log(url);

	request.post(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			if (resReq.statusCode === 200) {
				res.status(200).send({
					response: {
						succeed: true,
						data: response,
					},
				}).end();
			} else {
				console.log('Error changing status', response.error);
				res.status(resReq.statusCode).send({ response: { succeed: false } }).end();
			}
		} catch (e) {
			console.log('Error changing status', e);
			res.status(400).send({ response: { succeed: false } }).end();
		}
	});

});

router.route('/searchtown').post((req, res) => {
	const { search, match } = req.body;
	const url = match ? `${hostname}${path.searchtown}=${search}` : `${hostname}${path.searchtown}[like]=^${search.toLowerCase()
		.split(' ')
		.map(s => s.charAt(0).toUpperCase() + s.substring(1))
		.join(' ')}`;

	request.get(url).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			res.status(200).send({
				response: {
					succeed: true,
					data: response,
				},
			}).end();
		} catch (e) {
			res.status(400).send({
				response: {
					succeed: false,
				},
			}).end();
		}
	});
});

router.route('/getlistingfull').post((req, res) => {
	const {
		id, jwt, pathLength, currentStep,
	} = req.body;

	if (pathLength !== 5 && currentStep !== 'step-3') {
		res.status(200).send({
			response: {
				succeed: true,
				data: {},
			},
		}).end();
	} else {
		const url = `${replaceVar(hostname + path.listing, { listingId: id })}`;
		console.log(url);

		request.get(url).set('Authorization', `Bearer jwt ${decrypt(jwt)}`).end((errReq, resReq) => {
			try {
				const response = JSON.parse(resReq.text);
				const dataObject = response.data.projectProfiles[0];

				const result = new ListingModel(
					jwt,
					'',
					dataObject
				).getListingRecord();

				res.status(200).send({
					response: {
						succeed: true,
						data: result,
					},
				}).end();
			} catch (e) {
				console.log(e);
				res.status(400).send({ response: { succeed: false } }).end();
			}
		});
	}
});

router.route('/reply').post((req, res) => {
	const { jwt, dataObject } = req.body;

	console.log('reply', jwt, dataObject);

	res.status(200).send({
		response: {
			succeed: true,
			message: 'Risposta inviata'
		},
	}).end();


});

router.route('/deleterequest').post((req, res) => {
	const { jwt, dataObject } = req.body;

	console.log('deleterequest', jwt, dataObject);

	res.status(200).send({
		response: {
			succeed: true,
			message: 'Risposta inviata'
		},
	}).end();


});

router.route('/preview').post((req, res) => {
	const { listingObject } = req.body;
	const url = 'http://esapi.dev.casa.it/listings/v2/preview?site=it_casa&output=v1';
	console.log(url);
	console.log(listingObject);

	request.post(url).send(listingObject).end((errReq, resReq) => {
		try {
			const response = JSON.parse(resReq.text);

			if (resReq.statusCode === 200) {
				res.status(200).send({
					response: {
						succeed: true,
						data: response
					},
				}).end();
			} else {
				console.log('Error creating Preview', response.error);
				res.status(resReq.statusCode).send({ response: { succeed: false } }).end();
			}
		} catch (e) {
			console.log('Error creating Preview', e);
			res.status(400).send({ response: { succeed: false } }).end();
		}
	});
});

export default router;
