export const requestsMock = [
	{
		requestId: 1,
		read: false,
		user: {
			fullName: 'Joe Strummer',
			email: 'joe.strummer@clash.co.uk',
			phone: '+393486009689',
			text: 'My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal your money My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal your money My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal your money My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal your money My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal your money My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal your money My daddy was a bank robber But he never hurt nobody He just loved to live that way And he loved to steal',
			sentDt: '05/12/1977 - 3:45'
		},
		listing: {
			listingId: 36150782,
			imgUrl: 'listing/e51de7aa343ae832ea2c3241fd079e23',
			title: 'Loft in Vendita a Londra',
			price: '50.000',
			address: 'Via F.lli Kennedy, 324 Venegono Superiore',
			roomsNum: 1,
			surface: 25
		},
		answers: [
			{
				sentDt: '05/12/1977 - 3:45',
				text: 'Darling, you gotta let me know. Should I stay or should I go?'
			}
		]
	},
	{
		requestId: 2,
		read: true,
		user: {
			fullName: 'Joey Ramone',
			email: 'joey.ramone@ramones.ny',
			phone: '+393486009689',
			text: 'Hey ho let\'s go! They\'re forming in a straight line They\'re goin through a tight wind The kids are losing their minds Blitzkreig Bop',
			sentDt: '05/12/1977 - 4:30'
		},
		listing: {
			listingId: 36150782,
			imgUrl: 'listing/e51de7aa343ae832ea2c3241fd079e23',
			title: 'Appartamento in Affitto a New York',
			price: '120.000',
			address: 'Via F.lli Kennedy, 324 Venegono Superiore',
			roomsNum: 2,
			surface: 50
		}
	}
];
