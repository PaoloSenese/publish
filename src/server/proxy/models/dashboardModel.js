// @flow
import request from 'superagent';
import config from '../../config';
import { decrypt } from '../helpers/var';
import { replaceVar, transformDepths } from '../../../common/helpers/utils';
import { dataModel } from './listingModel';

const currentEnv = config.localMode ? 'local' : config.Env || 'dev';
const hostname = config.publishapi.hostname[currentEnv];
const { path } = config.publishapi;

class DashboardObject {
	dataObject: Object;
	jwt: string;

	constructor({ dataObject, jwt }: { dataObject: Object, jwt: string }) {
		this.dataObject = dataObject;
		this.jwt = jwt;
	}

	async getListingWithLeads() {
		this.dataObject.totalLeads = this.getTotalLeads();

		return this;
	}

	async getLeads(listingId) {
		const urlLeads = replaceVar(hostname + path.getleadscounts, { listingId });

		return request.get(urlLeads)
			.set('Authorization', `Bearer jwt ${decrypt(this.jwt)}`)
			.then((x) => {
				try {
					const response = JSON.parse(x.text);
					if (x.statusCode === 200) return response.data.listingLeads;
					console.log('Error getLeads', response.error);
					return [];
				} catch (e) {
					console.log('Error getLeads', e);
					return [];
				}
			});
	}

	getTotalLeads() {
		const listing = this.dataObject;
		const totalLeadsCounts = {
			specificRequests: listing.leadsCounters && listing.leadsCounters.specificRequests ? listing.leadsCounters.specificRequests : 0,
			listingViews: listing.leadsCounters && listing.leadsCounters.visualizzazioneAnnuncio ? listing.leadsCounters.visualizzazioneAnnuncio : 0,
			phoneReveals: listing.leadsCounters && listing.leadsCounters.visualizzazioneTelefonoAgenzia ? listing.leadsCounters.visualizzazioneTelefonoAgenzia : 0,
		};

		if (totalLeadsCounts.specificRequests === 0) totalLeadsCounts.specificRequests = '-';
		if (totalLeadsCounts.listingViews === 0) totalLeadsCounts.listingViews = '-';
		if (totalLeadsCounts.phoneReveals === 0) totalLeadsCounts.phoneReveals = '-';

		return totalLeadsCounts;
	}

}

export class DashboardsModel {
	jwt: string;
	dataObject: Array<Object>;
	uri: string;
	paging: Object;

	constructor(
		jwt: string, uri: string, dataObject: Array<Object>, paging: Object,
	) {
		this.jwt = jwt;
		this.dataObject = dataObject;
		this.uri = uri;
		this.paging = paging;
	}

	async getDashboardsRecord() {
		try {
			const returnData = this.dataObject.map(async (item) => {
				const result = await new DashboardObject({ dataObject: item, jwt: this.jwt }).getListingWithLeads();
				return result.dataObject;
			});

			this.dataObject = await Promise.all(returnData);
			this.dataObject = this.dataObject.map(item => dataModel(item));

			const depths = await request.get(hostname + path.getdepths)
				.set('Authorization', `Bearer jwt ${decrypt(this.jwt)}`);

			return Object.assign({}, this, { depthsObject: transformDepths(JSON.parse(depths.text).data.filter(o => o.productTypeEnum !== 26), config.default.radioContainers) });
		} catch (e) {
			return Object.assign({}, this, { depthsObject: [] });
		}
	}

}
