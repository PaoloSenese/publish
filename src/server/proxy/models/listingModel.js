// @flow
import request from 'superagent';
import moment from 'moment';
import config from '../../config';
import { decrypt } from '../helpers/var';
import {
	parseBool, setEmpty, reorderMedia, formatDateIt, replaceVar, getDatesBetween, dateObject, transformDepths
} from '../../../common/helpers/utils';
import { productPricingsFake } from '../../../common/helpers/var';

moment.locale('it');

const currentEnv = config.localMode ? 'local' : config.Env || 'dev';
const hostname = config.publishapi.hostname[currentEnv];
const { path } = config.publishapi;

const transFormDesc = text => text.toLowerCase() === 'annunci platinum' ? 'gold' : 'smart';

export const dataModel = (data: Object) => ({
	airConditionedId: data.airConditionedId ? data.airConditionedId : 0,
	bathNum: data.bathNum ? data.bathNum : 0,
	bodyCorporateFees: data.bodyCorporateFees,
	boxTypeId: data.boxTypeId ? data.boxTypeId : -1,
	buildingUnit: data.buildingUnit ? data.buildingUnit : null,
	conditionId: data.conditionId ? data.conditionId : -1,
	categoryTypeId: data.categoryTypeId ? (data.categoryTypeId === 5 ? 1 : data.categoryTypeId) : 1,
	contractTypeId: data.contractTypeId ? data.contractTypeId : 1,
	depth: {
		productTypeId: data.depth ? data.depth.productTypeId : null,
		description: data.depth ? transFormDesc(data.depth.description) : '',
	},
	description: [
		{
			name: 'listingDescIt',
			value: data.description ? setEmpty(data.description.listingDescIt) : '',
			isRequired: true,
			isValid: true,
			label: 'Italiano',
			abbr: 'IT',
			placeholder: `Inserisci qui la descrizione in ITALIANO

La descrizione del tuo immobile avrà maggiore impatto sui possibili acquirenti se racconterai cosa lo rende speciale:

- quali sono le sue caratteristiche distintive?
- com'è il quartiere?
- come sono i servizi in zona? (scuole, autobus, supermercati)`
		},
		{
			name: 'listingDescEn',
			value: data.description ? setEmpty(data.description.listingDescEn) : '',
			isRequired: false,
			isValid: true,
			label: 'Inglese',
			abbr: 'EN',
			placeholder: 'Descrizione inglese'
		},
		{
			name: 'listingDescFr',
			value: data.description ? setEmpty(data.description.listingDescFr) : '',
			isRequired: false,
			isValid: true,
			label: 'Francese',
			abbr: 'FR',
			placeholder: 'Descrizione francese'
		},
		{
			name: 'listingDescEs',
			value: data.description ? setEmpty(data.description.listingDescEs) : '',
			isRequired: false,
			isValid: true,
			label: 'Spagnolo',
			abbr: 'ES',
			placeholder: 'Descrizione spagnolo'
		},
		{
			name: 'listingDescDe',
			value: data.description ? setEmpty(data.description.listingDescDe) : '',
			isRequired: false,
			isValid: true,
			label: 'Tedesco',
			abbr: 'DE',
			placeholder: 'Descrizione tedesco'
		},
	],
	endDt: setEmpty(data.insertDate),
	energyEfficiencyRatingId: data.energyEfficiencyRatingId ? data.energyEfficiencyRatingId : 0,
	energyEfficiencyValue: setEmpty(data.energyEfficiencyValue),
	energyEfficiencyValueRenew: setEmpty(data.energyEfficiencyValueRenew),
	epeEnum: setEmpty(data.epeEnum),
	epiEnum: setEmpty(data.epiEnum),
	floorNum: data.floorNum ? data.floorNum : null,
	furnishedId: data.furnishedId ? data.furnishedId : 0,
	gardenTypeId: data.gardenTypeId ? data.gardenTypeId : -1,
	geolocation: [
		{
			name: 'coordinateEnabled', value: data.geolocation ? parseBool(!data.geolocation.coordinateEnabled, true) : false,
		},
		{
			name: 'isAddressVisible', value: data.geolocation ? parseBool(!data.geolocation.isAddressVisible, true) : false,
		},
		{
			name: 'lat', value: data.geolocation ? data.geolocation.lat : null,
		},
		{
			name: 'lng', value: data.geolocation ? data.geolocation.lng : null,
		},
		{
			name: 'postCode', value: data.geolocation ? setEmpty(data.geolocation.postCode) : null,
		},
		{
			name: 'streetName',
			value: data.geolocation ? setEmpty(data.geolocation.streetName) : null,
			isRequired: true,
			isValid: true,
			errorMessage: 'Campo obbligatorio'
		},
		{
			name: 'streetNumber', value: data.geolocation ? setEmpty(data.geolocation.streetNumber) : null,
		},
		{
			name: 'suburb',
			value: data.geolocation ? data.geolocation.suburb : null,
			isRequired: true,
			isValid: true,
			errorMessage: 'Campo obbligatorio'
		},
		{
			name: 'townId', value: data.geolocation ? data.geolocation.townId : null,
		},
		{
			name: 'townZone',
			value: {
				townZoneId: data.geolocation && data.geolocation.townZone ? data.geolocation.townZone.townZoneId : null,
				description: data.geolocation && data.geolocation.townZone ? data.geolocation.townZone.description : '',
			},
		},
	],
	hasAttic: data.hasAttic ? data.hasAttic : '',
	hasBalcony: data.hasBalcony ? data.hasBalcony : '',
	hasCellar: data.hasCellar ? data.hasCellar : '',
	hasConcierge: data.hasConcierge ? data.hasConcierge : '',
	hasElevator: parseBool(data.hasElevator),
	hasSwimmingPool: parseBool(data.hasSwimmingPool),
	hasTerrace: data.hasTerrace,
	heatingTypeId: data.heatingTypeId ? data.heatingTypeId : -1,
	insertDt: setEmpty(data.insertDate),
	isArchived: parseBool(data.isArchived),
	isCubeMeters: parseBool(data.isCubeMeters, true),
	isQuiteZeroEnergyeState: parseBool(data.isQuiteZeroEnergyeState, true),
	lastModifiedDt: setEmpty(data.lastModifiedDt),
	leadsCounts: {
		specificRequests: data.leadsCounters && data.leadsCounters.specificRequests ? parseInt(data.leadsCounters.specificRequests, 10) : 0,
		listingViews: data.leadsCounters && data.leadsCounters.visualizzazioneAnnuncio ? parseInt(data.leadsCounters.visualizzazioneAnnuncio, 10) : 0,
		phoneReveals: data.leadsCounters && data.leadsCounters.visualizzazioneTelefonoAgenzia ? parseInt(data.leadsCounters.visualizzazioneTelefonoAgenzia, 10) : 0,
	},
	levelNum: setEmpty(data.levelNum) === -1 ? '' : setEmpty(data.levelNum),
	listingEnabled: data.listingEnabled ? data.listingEnabled : false,
	listingId: setEmpty(data.projectProfileId),
	media: reorderMedia(data.media),
	mqBox: data.mqBox ? data.mqBox : null,
	mqGarden: data.mqGarden ? data.mqGarden : null,
	mqBalcony: data.mqBalcony ? data.mqBalcony : null,
	mqTerrace: data.mqTerrace ? data.mqTerrace : null,
	occupationStateId: data.occupationStateId ? data.occupationStateId : -1,
	parkingNumber: data.parkingNumber ? data.parkingNumber : 0,
	parkingSpaceId: data.parkingSpaceId ? data.parkingSpaceId : -1,
	picture: data.media.length > 0 && data.media.filter(o => o.imageType === 'F').length > 0 && data.media.filter(o => o.isPrincipal === true).length > 0
		? data.media.filter(o => o.isPrincipal === true)[0].url
		: data.media.length > 0 && data.media.filter(o => o.imageType === 'F').length > 0
			? data.media[0].url
			: 'props/placeholder.png`',
	propertyTypeId: data.propertyTypeId ? (data.propertyTypeId === 49 ? 4 : data.propertyTypeId) : ' ',
	publisherListingId: setEmpty(data.publisherListingId),
	price: '120000'.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.'),
	priceMax: data.priceMax,
	roomNum: data.roomNum ? data.roomNum : 4,
	surface: data.surface ? data.surface : 85,
	totalLeads: data.totalLeads ? data.totalLeads : [],
	yearOfBuilding: data.yearOfBuilding ? data.yearOfBuilding : null,
});

export class ListingModel {
	jwt: string;
	uri: string;
	dataObject: Object;

	constructor(jwt: string, uri: string, dataObject: Object) {
		this.jwt = jwt;
		this.uri = uri;
		this.dataObject = dataModel(dataObject);
	}

	getListingRecord() {
		return Object.assign({}, this, {
			listingDetail: {
				hasMedia: false,
			},
		});
	}

	async getAsyncListingRecord() {
		const url = `${replaceVar(hostname + path.listing, { listingId: this.dataObject.listingId })}`;

		const listingDetail = await request.get(url)
			.set('Authorization', `Bearer jwt ${decrypt(this.jwt)}`);

		let dataObject = { media: [] };
		if (listingDetail.statusCode === 200) {
			const response = JSON.parse(listingDetail.text);
			const { projectProfiles } = response.data;
			dataObject = {
				media: projectProfiles[0].media,
			};
		}

		const depths = await request.get(hostname + path.getdepths)
			.set('Authorization', `Bearer jwt ${decrypt(this.jwt)}`);
		const depthsObject = transformDepths(JSON.parse(depths.text).data.filter(o => o.productTypeEnum !== 26), config.default.radioContainers);

		return Object.assign({}, this, {
			listingDetail: {
				hasMedia: dataObject.media.length > 0,
			},
			depthsObject,
			productPricings: productPricingsFake
		});
	}

	async getListingWithLeadsRecord(id: number) {
		const startDt = formatDateIt(formatDateIt(moment().subtract(1, 'months').format('DD/MM/YYYY')));
		const endDt = formatDateIt('');
		const url = `${replaceVar(hostname + path.getleads, { listingId: id, startDt, endDt })}`;

		const listingLeads = await request.get(url)
			.set('Authorization', `Bearer jwt ${decrypt(this.jwt)}`);

		const viewsArray = getDatesBetween(new Date(dateObject(startDt)), new Date());
		if (listingLeads.statusCode === 200) {
			JSON.parse(listingLeads.text).data.map((item) => {
				if (item.propertyView) {
					viewsArray[item.date] = item.propertyView;
				}
			});
		}

		const depths = await request.get(hostname + path.getdepths)
			.set('Authorization', `Bearer jwt ${decrypt(this.jwt)}`);
		const depthsObject = transformDepths(JSON.parse(depths.text).data.filter(o => o.productTypeEnum !== 26), config.default.radioContainers);

		return Object.assign({}, this, {
			listingLeads: viewsArray,
			depthsObject,
			productPricings: productPricingsFake
		});
	}
}
