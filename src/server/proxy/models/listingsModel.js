// @flow
import { dataModel } from './listingModel';

export const listingsDataModel = (data: Array<Object>) => {
	const returnData = [];
	data.map((item) => {
		returnData.push(dataModel(item));
	});
	return returnData;
};

class listingsModel {
	jwt: string;
	dataObject: Array<Object>;
	uri: string;

	constructor(jwt: string, dataObject: Array<Object>, uri: string) {
		this.jwt = jwt;
		this.dataObject = listingsDataModel(dataObject);
		this.uri = uri;
	}

	getListingsRecord() {
		return this;
	}
}

export default listingsModel;
