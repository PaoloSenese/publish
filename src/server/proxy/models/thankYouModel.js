// @flow
class ThankYouModel {
	jwt: string;
	uri: string;
	dataObject: Object;

	constructor(
		jwt: string, uri: string, dataObject: Object
	) {
		this.jwt = jwt;
		this.uri = uri;
		this.dataObject = { itemid: 1 };
	}

	getThankYouRecord() {
		return this;
	}
}

export default ThankYouModel;
