// @flow
class RequestsModel {
	jwt: string;
	uri: string;
	dataObject: Object;

	constructor(
		jwt: string, uri: string, dataObject: Object
	) {
		this.jwt = jwt;
		this.uri = uri;
		this.dataObject = dataObject;
	}

	getRequestsRecord() {
		return this;
	}
}

export default RequestsModel;
