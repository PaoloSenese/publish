// @flow
import bodyParser from 'body-parser';
import express from 'express';
import chalk from 'chalk';

import listing from './listing';
import listings from './listings';

const app = express();

app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({
	limit: '50mb',
	extended: true,
	parameterLimit: 50000,
}));

app.use('/listing', listing);
app.use('/listings', listings);

app.on('mount', () => {
	// flow-disable-next-line
	const { Console } = require('console');
	const startCons = new Console(process.stdout, process.stderr);
	startCons.log(`[${chalk.gray(new Date(Date.now()).toTimeString().substr(0, 8))}] Internal Api is available at ${chalk.cyan.bold('%s')}`, app.mountpath);
});

export default app;
