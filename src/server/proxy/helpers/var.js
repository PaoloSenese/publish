import sjwt from 'jsonwebtoken';
import config from '../../config';

const currentEnv = config.localMode ? 'local' : config.Env || 'dev';
const secret = process.env.JWT_SECRET || config.auth[currentEnv].secret;

export const decrypt = (jwt) => {
	const jwtVer = sjwt.verify(jwt, secret, (err) => {
		if (err) return null;
		return jwt;
	});

	return jwtVer;
};
