require('babel-register');
require('babel-polyfill');

const IsoTools = require('webpack-isomorphic-tools');
const rootDir = require('path').resolve(__dirname, '..', '..');
const assets = require('../../webpack/assets').default;

if (!process.env.NODE_ENV) {
	throw new Error('Environment variable NODE_ENV must be set to development or production.');
}

// http://bluebirdjs.com/docs/why-bluebird.html
global.Promise = require('../common/configureBluebird');

global.webpackIsomorphicTools = new IsoTools(assets).server(rootDir, () => {
	require('./main');
});
