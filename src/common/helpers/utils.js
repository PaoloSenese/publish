// @flow
import moment from 'moment';

moment.locale('it');

export const parseBool = (val: any, noNull: ?boolean) => {
	if (noNull) return val !== null && val !== undefined ? val : false;
	return val !== null && val !== undefined ? val.toString() : '';
};

export const replaceBool = (val: any) => val === '' ? null : (val === 'true' || val === 'on' || val === true);

export const replaceInt = (val: any) => val && val !== '' ? parseFloat(val) : null;

export const formatCurrency = (val: string) => val && val !== '' ? parseFloat(val.toString().replace(/[.]/g, '')) : null;

export const setEmpty = (val: any) => val !== null && val !== undefined ? val : '';

export const replaceVar = (template: string, variables: Object) => template.replace(new RegExp('{([^{]+)}', 'g'), (_unused, varName) => variables[varName]);

export const flatten = (input: Object) => {
	const output = {};
	Object.keys(input).forEach((key) => {
		if (key.toString() !== 'jwt' && key.toString() !== 'suburb' && key.toString() !== 'zone' && key.toString() !== 'region' && key.toString() !== 'province' && key !== 'listingId' && key !== 'projectProfileId') {
			const value = input[key];
			if (typeof value === 'object' && value !== null) {
				Object.keys(input[key]).forEach((keyChild) => {
					if (keyChild.toString() !== 'suburb' && keyChild.toString() !== 'zone' && keyChild.toString() !== 'region' && keyChild.toString() !== 'province' && keyChild !== 'townZone') {
						const valueChild = input[key][keyChild];
						output[keyChild] = valueChild;
					} else if (keyChild === 'townZone') {
						Object.keys(input[key][keyChild]).forEach((keyChildChild) => {
							if (keyChildChild.toString() === 'townZoneId') {
								const valueChildChild = input[key][keyChild][keyChildChild];
								output[keyChildChild] = valueChildChild;
							}
						});
					}
				});
			} else {
				output[key] = value === 'null' ? null : value;
			}
		}
	});
	return output;
};

export const reorderMedia = (media: Array<Object>) => {
	media.sort((a, b) => {
		const orderA = a.order;
		const orderB = b.order;

		if (orderA < orderB) return -1;
		if (orderA > orderB) return 1;
		return 0;
	});

	return media;
};

export const formatDateIt = (date: string) => {
	if (moment(date, 'DD/MM/YYYY').isValid()) return date;

	const d = date ? new Date(date) : new Date();
	const day = d.getDate();
	const month = d.getMonth() + 1;
	const year = d.getFullYear();
	let nd = `${day}/${month}/${year}`;

	nd = nd.replace(/\d+/g, m => m.length === 1 ? '0'.substr(m.length - 1) + m : m);

	return nd;
};

export const getDatesBetween = (startDate: Date, endDate: Date) => {
	const dates = {};

	let currentDate = new Date(
		startDate.getFullYear(),
		startDate.getMonth(),
		startDate.getDate(),
	);

	while (currentDate <= endDate) {
		const day = currentDate.getDate();
		const month = currentDate.getMonth() + 1;
		const year = currentDate.getFullYear();
		let nd = `${year}-${month}-${day}`;

		nd = nd.replace(/\d+/g, m => m.length === 1 ? '0'.substr(m.length - 1) + m : m);
		dates[nd] = 0;

		currentDate = new Date(
			currentDate.getFullYear(),
			currentDate.getMonth(),
			currentDate.getDate() + 1, // Will increase month if over range
		);
	}

	return dates;
};

export const dateObject = (date: string) => {
	const dateParts = date.split('/');
	return new Date(parseInt(dateParts[2], 10), parseInt(dateParts[1], 10) - 1, parseInt(dateParts[0], 10));
};

export const transformDepths = (data: Array<Object>, radioContainers: Object) => {
	data = data && data.length > 0 ? data.map(item => ({ productType: item.productTypeEnum, productId: item.productId, name: item.productDesc })) : [];

	/* TO BE REMOVED - this is the product array as expected */
	data.push({ productType: 17, productId: 1046, name: 'silver', period: 6 });
	data.push({ productType: 20, productId: 1047, name: 'gold', period: 6 });
	data.push({ productType: 16, productId: 1048, name: 'bronze', period: 3 });
	data.push({ productType: 16, productId: 1048, name: 'bronze', period: 6 });
	data.push({ productType: 15, productId: 1049, name: 'standard' });
	/* TO BE REMOVED */

	// group by productType
	const depthsObjectGroup = [];
	const map = new Map();
	for (const item of data) {
			if (!map.has(item.productType)) {
					map.set(item.productType, true);
					depthsObjectGroup.push(item);
			}
	}

	// merge with configuration and order by order key
	const depthsObjectTrans = depthsObjectGroup.map((x) => {
		const resturnObject = radioContainers.radioDepth.filter(o => o.productType === x.productType)[0];
		return Object.assign({}, resturnObject, x);
	}).sort((a, b) => {
		const keyA = a.order;
		const keyB = b.order;
		if (keyA < keyB) return -1;
		if (keyA > keyB) return 1;
		return 0;
	});

	return depthsObjectTrans;
};

export const arrayToObject = (e: any) => {
	const newObject = {};
	e.map((item) => {
		newObject[item.name] = item.value;
	});
	return newObject;
};

export const makeId = () => {
	let text = '';
	const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
	for (let i = 0; i < 5; i += 1) {
		text += possible.charAt(Math.floor(Math.random() * possible.length));
	}
	return text;
};

export const notEmpty = (val: any) => val !== null && val !== undefined && val !== '';

export const rebuildSimpleListingState = (f: Object) => {
	const returnObject = {
		description: f.description,
		projectProfileId: f.listingId,
	};
	return returnObject;
};

export const rebuildListingState = (f: Object) => {
	const returnObject = {
		description: f.description,
		energyEfficiencyRatingId: replaceInt(f.energyEfficiencyRatingId),
		energyEfficiencyValue: parseFloat(f.energyEfficiencyValue.replace(',', '.')),
		energyEfficiencyValueRenew: parseFloat(f.energyEfficiencyValueRenew.replace(',', '.')),
		epeEnum: replaceInt(f.epeEnum),
		epiEnum: replaceInt(f.epiEnum),
		gardenTypeId: replaceInt(f.gardenTypeId),
		geolocation: f.geolocation,
		isCubeMeters: replaceBool(f.isCubeMeters),
		isQuiteZeroEnergyeState: replaceBool(f.isQuiteZeroEnergyeState),
		levelNum: replaceInt(f.levelNum) === null ?	-1 : replaceInt(f.levelNum),
		heatingTypeId: replaceInt(f.heatingTypeId),
		hasElevator: replaceBool(f.hasElevator),
		hasSwimmingPool: replaceBool(f.hasSwimmingPool),
		numberOfUnits: replaceInt(f.numberOfUnits),
		projectProfileId: f.listingId,
		publisherListingId: f.publisherListingId,
	};
	return returnObject;
};

export const camelToTitle = (str: string, name: string) => str === null || str === undefined ?
	name.replace(/([a-z\d])([A-Z])/g, '$1  $2')
		.replace(/([A-Z]+)([A-Z][a-z\d]+)/g, '$1 $2')
		.replace(/\w\S*/g, txt => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase())
	: str;

export const sumClasses = (classes: Array<string>) => {
	const filteredClasses = classes.filter(o => o !== '' && o !== undefined);
	let returnEach = '';
	filteredClasses.map((item, i) => {
		returnEach += i === filteredClasses.length - 1 ? `${item}` : `${item} `;
		return null;
	});
	return returnEach;
};

export const logLocalOnly = (myObject: Object, localMode: boolean) => {
	if (localMode) console.log(myObject);
};

export const getViewport = (viewport: Object) => {
	/* eslint-disable-next-line */
	if (viewport.height === '' && document && document.documentElement) return { width: document.documentElement.clientWidth, height: document.documentElement.clientHeight };
	return viewport;
};
