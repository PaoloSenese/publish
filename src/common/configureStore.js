// @flow
import { applyMiddleware, compose, createStore } from 'redux';
import configureMiddleware from './configureMiddleware';
import configureReducer from './configureReducer';

const configureStore = (options: Object) => {
	const {
		initialState,
		platformDeps = {},
		platformMiddleware = [],
		platformReducers = {},
		platformStoreEnhancers = [],
	} = options;

	const reducer = configureReducer(platformReducers);

	const middleware = configureMiddleware(initialState, platformDeps, platformMiddleware);

	const store = createStore(
		reducer,
		initialState,
		compose(applyMiddleware(...middleware), ...platformStoreEnhancers),
	);

	// Enable hot reloading for reducers.
	// flow-disable-next-line
	if (module.hot && typeof module.hot.accept === 'function') {
		// Webpack for some reason needs accept with the explicit path.
		// flow-disable-next-line
		module.hot.accept('./configureReducer', () => {

			// flow-disable-next-line
			const configReducer = require('./configureReducer');
			// $FlowIssue: check it
			store.replaceReducer(configReducer(platformReducers));
		});
	}

	return store;
};

export default configureStore;
