import React from 'react';

export default function fetch(...actions) {
	return Wrapped => class Fetch extends React.PureComponent<any, any> {

		static fetchActions = actions;

		render() {
			return <Wrapped {...this.props} />;
		}

	};
}
