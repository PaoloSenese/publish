// @flow
/* global window */

export { default as fetch } from './fetch';

export const canUseDOM = !!(
	typeof window !== 'undefined' && window.document && window.document.createElement
);
