// @flow
export const FETCH_LISTINGS_CLIENT = 'FETCH_LISTINGS_CLIENT';
export const FETCH_LISTINGS = 'FETCH_LISTINGS';
export const FETCH_LISTINGS_START = 'FETCH_LISTINGS_START';
export const FETCH_LISTINGS_SUCCESS = 'FETCH_LISTINGS_SUCCESS';
export const FETCH_LISTINGS_ERROR = 'FETCH_LISTINGS_ERROR';

const fetchFn = (fetch: Function, url: string, verb: string, params: Object) => fetch(url, {
	method: verb,
	headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
	body: JSON.stringify(params),
});

export const fetchListings = (params: Object) => {
	const body = {
		uri: params.uri,
		jwt: params.jwt,
	};
	return ({ fetch }: { fetch: Function }) => ({
		type: FETCH_LISTINGS,
		payload: {
			promise: fetchFn(fetch, '/admin/api/v1/listings', 'POST', body)
				.then(response => response.json())
				.catch(err => console.error('error', err)),
		},
	});
};

export const FETCH_DELETE = 'FETCH_DELETE';
export const FETCH_DELETE_START = 'FETCH_DELETE_START';
export const FETCH_DELETE_SUCCESS = 'FETCH_DELETE_SUCCESS';
export const FETCH_DELETE_ERROR = 'FETCH_DELETE_ERROR';

export const fetchDelete = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'FETCH_DELETE',
	payload: {
		promise: fetch('/admin/api/v1/listings/delete', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'FETCH_DELETE',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const FETCH_UPDATE = 'FETCH_UPDATE';
export const FETCH_UPDATE_START = 'FETCH_UPDATE_START';
export const FETCH_UPDATE_SUCCESS = 'FETCH_UPDATE_SUCCESS';
export const FETCH_UPDATE_ERROR = 'FETCH_UPDATE_ERROR';

export const fetchUpdate = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'FETCH_UPDATE',
	payload: {
		promise: fetch('/admin/api/v1/listings/update', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'FETCH_UPDATE',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const GET_REQUESTS_START = 'GET_REQUESTS_START';
export const GET_REQUESTS_SUCCESS = 'GET_REQUESTS_SUCCESS';
export const GET_REQUESTS_ERROR = 'GET_REQUESTS_ERROR';

export const getRequests = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'GET_REQUESTS',
	payload: {
		promise: fetch('/admin/api/v1/listings/getrequests', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'FETCH_UPDATE',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const ACTIVATE_LISTING_START = 'ACTIVATE_LISTING_START';
export const ACTIVATE_LISTING_SUCCESS = 'ACTIVATE_LISTING_SUCCESS';
export const ACTIVATE_LISTING_ERROR = 'ACTIVATE_LISTING_ERROR';

export const activateListing = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'ACTIVATE_LISTING',
	payload: {
		promise: fetch('/admin/api/v1/listings/activatelisting', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'ACTIVATE_LISTING',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const CHANGE_STATUS_START = 'CHANGE_STATUS_START';
export const CHANGE_STATUS_SUCCESS = 'CHANGE_STATUS_SUCCESS';
export const CHANGE_STATUS_ERROR = 'CHANGE_STATUS_ERROR';

export const changeStatus = (params: Object) => {
	console.log(params);

	return ({ fetch }: { fetch: Function }) => ({
		type: 'CHANGE_STATUS',
		payload: {
			promise: fetch('/admin/api/v1/listings/changestatus', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
				body: JSON.stringify(params),
			})
				.then(response => response.json().then(data => ({
					response: data.response,
					type: 'CHANGE_STATUS',
				})))
				.catch(err => console.log('error', err)),
		},
	});
};

export const GET_LEADS_START = 'GET_LEADS_START';
export const GET_LEADS_SUCCESS = 'GET_LEADS_SUCCESS';
export const GET_LEADS_ERROR = 'GET_LEADS_ERROR';

export const getLeads = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'GET_LEADS',
	payload: {
		promise: fetch('/admin/api/v1/listings/getleads', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'GET_LEADS',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const SEARCH_TOWN_ERROR = 'SEARCH_TOWN_ERROR';
export const SEARCH_TOWN_START = 'SEARCH_TOWN_START';
export const SEARCH_TOWN_SUCCESS = 'SEARCH_TOWN_SUCCESS';

export const searchTown = (params: Object) => {
	const body = {
		search: params.search,
		match: params.match,
		jwt: params.jwt,
	};

	return ({ fetch }: { fetch: Function }) => ({
		type: 'SEARCH_TOWN',
		payload: {
			promise: fetch('/admin/api/v1/listing/searchtown', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
				body: JSON.stringify(body),
			}).then(response => response.json())
				.catch(err => console.log('error', err)),
		},
	});
};

export const GET_LISTING_START = 'GET_LISTING_START';
export const GET_LISTING_SUCCESS = 'GET_LISTING_SUCCESS';
export const GET_LISTING_ERROR = 'GET_LISTING_ERROR';

export const getListingFull = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'GET_LISTING',
	payload: {
		promise: fetch('/admin/api/v1/listings/getlistingfull', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'GET_LISTING',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const SEND_REPLY_START = 'SEND_REPLY_START';
export const SEND_REPLY_SUCCESS = 'SEND_REPLY_SUCCESS';
export const SEND_REPLY_ERROR = 'SEND_REPLY_ERROR';

export const sendReply = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'SEND_REPLY',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listings/reply', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const DELETE_REQUEST_START = 'DELETE_REQUEST_START';
export const DELETE_REQUEST_SUCCESS = 'DELETE_REQUEST_SUCCESS';
export const DELETE_REQUEST_ERROR = 'DELETE_REQUEST_ERROR';

export const deleteRequest = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'DELETE_REQUEST',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listings/deleterequest', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const PREVIEW_START = 'PREVIEW_START';
export const PREVIEW_SUCCESS = 'PREVIEW_SUCCESS';
export const PREVIEW_ERROR = 'PREVIEW_ERROR';

export const preview = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'PREVIEW',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listings/preview', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});
