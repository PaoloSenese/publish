// @flow
import * as actions from './actions';

const initialState = {};

export default function homeReducer(state: Object = initialState, action: { type: string, payload: any }) {
	switch (action.type) {
	case actions.FETCH_LISTINGS_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.FETCH_LISTINGS_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.FETCH_LISTINGS_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.FETCH_DELETE_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.FETCH_DELETE_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.FETCH_DELETE_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.FETCH_UPDATE_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.FETCH_UPDATE_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.FETCH_UPDATE_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.GET_REQUESTS_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.GET_REQUESTS_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.GET_REQUESTS_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.ACTIVATE_LISTING_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.ACTIVATE_LISTING_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.ACTIVATE_LISTING_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.CHANGE_STATUS_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.CHANGE_STATUS_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.CHANGE_STATUS_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.GET_LEADS_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.GET_LEADS_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.GET_LEADS_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.SEARCH_TOWN_START: {
		return Object.assign({}, state);
	}

	case actions.SEARCH_TOWN_SUCCESS: {
		return Object.assign({}, state, action.payload, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.SEARCH_TOWN_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.GET_LISTING_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.GET_LISTING_SUCCESS: {
		return Object.assign({}, action.payload, state, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.GET_LISTING_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.SEND_REPLY_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.SEND_REPLY_SUCCESS: {
		return Object.assign({}, state, action.payload, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.SEND_REPLY_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.DELETE_REQUEST_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.DELETE_REQUEST_SUCCESS: {
		return Object.assign({}, state, action.payload, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.DELETE_REQUEST_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	case actions.PREVIEW_START: {
		return Object.assign({}, state, {
			isFetching: true,
			errorMessage: '',
		});
	}

	case actions.PREVIEW_SUCCESS: {
		return Object.assign({}, state, action.payload, {
			isFetching: false,
			errorMessage: '',
		});
	}

	case actions.PREVIEW_ERROR: {
		return Object.assign({}, state, {
			isFetching: false,
			errorMessage: action.payload.message,
		});
	}

	default: {
		return state;
	}
	}
}
