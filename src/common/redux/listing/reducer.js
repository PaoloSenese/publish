// @flow
import * as actions from './actions';

const initialState = {};

export default function homeReducer(state: Object = initialState, action: { type: string, payload: any }) {
	switch (action.type) {
		case actions.FETCH_LISTING_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.FETCH_LISTING_SUCCESS: {
			return Object.assign({}, action.payload, state, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.FETCH_LISTING_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.FETCH_SAVE_LISTING_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.FETCH_SAVE_LISTING_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.FETCH_SAVE_LISTING_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.FETCH_BUY_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.FETCH_BUY_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.FETCH_BUY_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.SEARCH_TOWN_START: {
			return Object.assign({}, state);
		}

		case actions.SEARCH_TOWN_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.SEARCH_TOWN_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.DELETE_MEDIA_START: {
			return Object.assign({}, state);
		}

		case actions.DELETE_MEDIA_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.DELETE_MEDIA_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.DELETE_ALL_MEDIA_START: {
			return Object.assign({}, state);
		}

		case actions.DELETE_ALL_MEDIA_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.DELETE_ALL_MEDIA_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.MOVE_MEDIA_START: {
			return Object.assign({}, state);
		}

		case actions.MOVE_MEDIA_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.MOVE_MEDIA_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.FETCH_DELETE_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.FETCH_DELETE_SUCCESS: {
			return Object.assign({}, action.payload, state, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.FETCH_DELETE_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.ROTATE_MEDIA_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.ROTATE_MEDIA_SUCCESS: {
			return Object.assign({}, action.payload, state, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.ROTATE_MEDIA_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.GET_MEDIA_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.GET_MEDIA_SUCCESS: {
			return Object.assign({}, action.payload, state, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.GET_MEDIA_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.GET_VACANCIES_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.GET_VACANCIES_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.GET_VACANCIES_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.SAVE_VACANCIES_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.SAVE_VACANCIES_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.SAVE_VACANCIES_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.DELETE_VACANCIES_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.DELETE_VACANCIES_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.DELETE_VACANCIES_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.SEND_SMS_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.SEND_SMS_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.SEND_SMS_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.VERIFY_CODE_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.VERIFY_CODE_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.VERIFY_CODE_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.SAVE_CONTACTS_START: {
			return Object.assign({}, state, {
				isFetching: true,
				errorMessage: '',
			});
		}

		case actions.SAVE_CONTACTS_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.SAVE_CONTACTS_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.GET_ZONE_START: {
			return Object.assign({}, state);
		}

		case actions.GET_ZONE_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.GET_ZONE_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		case actions.LOCATION_START: {
			return Object.assign({}, state);
		}

		case actions.LOCATION_SUCCESS: {
			return Object.assign({}, state, action.payload, {
				isFetching: false,
				errorMessage: '',
			});
		}

		case actions.LOCATION_ERROR: {
			return Object.assign({}, state, {
				isFetching: false,
				errorMessage: action.payload.message,
			});
		}

		default: {
			return state;
		}
	}
}
