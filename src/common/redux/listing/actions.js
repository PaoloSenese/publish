// @flow
import { rebuildListingState, rebuildSimpleListingState } from '../../helpers/utils';

export const FETCH_LISTING = 'FETCH_LISTING';
export const FETCH_LISTING_START = 'FETCH_LISTING_START';
export const FETCH_LISTING_SUCCESS = 'FETCH_LISTING_SUCCESS';
export const FETCH_LISTING_ERROR = 'FETCH_LISTING_ERROR';

const fetchFn = (fetch: Function, url: string, verb: string, params: Object) => fetch(url, {
	method: verb,
	headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
	body: JSON.stringify(params),
});

export const fetchListing = (params: Object) => {
	const uriSplit = params.uri.split('/');

	const body = {
		uri: params.uri,
		jwt: params.jwt,
		id: uriSplit[2],
	};

	return ({ fetch }: { fetch: Function }) => ({
		type: FETCH_LISTING,
		payload: {
			promise: fetchFn(fetch, '/admin/api/v1/listing', 'POST', body)
				.then(response => response.json())
				.catch(err => console.error('error', err)),
		},
	});
};

export const FETCH_SAVE_LISTING_START = 'FETCH_SAVE_LISTING_START';
export const FETCH_SAVE_LISTING_SUCCESS = 'FETCH_SAVE_LISTING_SUCCESS';
export const FETCH_SAVE_LISTING_ERROR = 'FETCH_SAVE_LISTING_ERROR';

export const fetchSaveListing = (params: Object) => {

	params.dataObject = params.simple ? rebuildSimpleListingState(params.dataObject) : rebuildListingState(params.dataObject);

	return ({ fetch }: { fetch: Function }) => ({
		type: 'FETCH_SAVE_LISTING',
		payload: {
			search: 'something',
			promise: fetchFn(fetch, '/admin/api/v1/listing/savelisting', 'POST', params)
				.then(response => response.json())
				.catch(err => console.log('error', err)),
		},
	});
};

export const FETCH_BUY_START = 'FETCH_BUY_START';
export const FETCH_BUY_SUCCESS = 'FETCH_BUY_SUCCESS';
export const FETCH_BUY_ERROR = 'FETCH_BUY_ERROR';

export const fetchBuy = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'FETCH_BUY',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/buy', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const SEARCH_TOWN_ERROR = 'SEARCH_TOWN_ERROR';
export const SEARCH_TOWN_START = 'SEARCH_TOWN_START';
export const SEARCH_TOWN_SUCCESS = 'SEARCH_TOWN_SUCCESS';

export const searchTown = (params: Object) => {
	const body = {
		search: params.search,
		match: params.match,
		jwt: params.jwt,
	};

	return ({ fetch }: { fetch: Function }) => ({
		type: 'SEARCH_TOWN',
		payload: {
			promise: fetch('/admin/api/v1/listing/searchtown', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
				body: JSON.stringify(body),
			})
				.then(response => response.json())
				.catch(err => console.log('error', err)),
		},
	});
};

export const DELETE_MEDIA_ERROR = 'DELETE_MEDIA_ERROR';
export const DELETE_MEDIA_START = 'DELETE_MEDIA_START';
export const DELETE_MEDIA_SUCCESS = 'DELETE_MEDIA_SUCCESS';

export const deleteMedia = (params: Object) => {
	const body = {
		listingId: params.listingId,
		imageId: params.imageId,
		jwt: params.jwt,
	};

	return ({ fetch }: { fetch: Function }) => ({
		type: 'DELETE_MEDIA',
		payload: {
			promise: fetch('/admin/api/v1/listing/deletemedia', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
				body: JSON.stringify(body),
			})
				.then(response => response.json())
				.catch(err => console.log('error', err)),
		},
	});
};

export const DELETE_ALL_MEDIA_ERROR = 'DELETE_ALL_MEDIA_ERROR';
export const DELETE_ALL_MEDIA_START = 'DELETE_ALL_MEDIA_START';
export const DELETE_ALL_MEDIA_SUCCESS = 'DELETE_ALL_MEDIA_SUCCESS';

export const deleteAllMedia = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'DELETE_ALL_MEDIA',
	payload: {
		promise: fetch('/admin/api/v1/listing/deleteallmedia', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const MOVE_MEDIA_ERROR = 'MOVE_MEDIA_ERROR';
export const MOVE_MEDIA_START = 'MOVE_MEDIA_START';
export const MOVE_MEDIA_SUCCESS = 'MOVE_MEDIA_SUCCESS';

export const moveMedia = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'MOVE_MEDIA',
	payload: {
		promise: fetch('/admin/api/v1/listing/movemedia', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const FETCH_DELETE_START = 'FETCH_DELETE_START';
export const FETCH_DELETE_SUCCESS = 'FETCH_DELETE_SUCCESS';
export const FETCH_DELETE_ERROR = 'FETCH_DELETE_ERROR';

export const fetchDelete = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'FETCH_DELETE',
	payload: {
		promise: fetch('/admin/api/v1/listings/delete', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'FETCH_DELETE',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const ROTATE_MEDIA_START = 'ROTATE_MEDIA_START';
export const ROTATE_MEDIA_SUCCESS = 'ROTATE_MEDIA_SUCCESS';
export const ROTATE_MEDIA_ERROR = 'ROTATE_MEDIA_ERROR';

export const rotateMedia = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'ROTATE_MEDIA',
	payload: {
		promise: fetch('/admin/api/v1/listing/rotatemedia', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'ROTATE_MEDIA',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const GET_MEDIA_START = 'GET_MEDIA_START';
export const GET_MEDIA_SUCCESS = 'GET_MEDIA_SUCCESS';
export const GET_MEDIA_ERROR = 'GET_MEDIA_ERROR';

export const getMedia = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'GET_MEDIA',
	payload: {
		promise: fetch('/admin/api/v1/listing/getallmedia', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
			body: JSON.stringify(params),
		})
			.then(response => response.json().then(data => ({
				response: data.response,
				type: 'GET_MEDIA',
			})))
			.catch(err => console.log('error', err)),
	},
});

export const GET_VACANCIES_START = 'GET_VACANCIES_START';
export const GET_VACANCIES_SUCCESS = 'GET_VACANCIES_SUCCESS';
export const GET_VACANCIES_ERROR = 'GET_VACANCIES_ERROR';

export const getVacancies = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'GET_VACANCIES',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/getvacancies', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const SAVE_VACANCIES_START = 'SAVE_VACANCIES_START';
export const SAVE_VACANCIES_SUCCESS = 'SAVE_VACANCIES_SUCCESS';
export const SAVE_VACANCIES_ERROR = 'SAVE_VACANCIES_ERROR';

export const saveVacancies = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'SAVE_VACANCIES',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/savevacancies', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const DELETE_VACANCIES_START = 'DELETE_VACANCIES_START';
export const DELETE_VACANCIES_SUCCESS = 'DELETE_VACANCIES_SUCCESS';
export const DELETE_VACANCIES_ERROR = 'DELETE_VACANCIES_ERROR';

export const deleteVacancies = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'DELETE_VACANCIES',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/deletevacancies', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const SEND_SMS_START = 'SEND_SMS_START';
export const SEND_SMS_SUCCESS = 'SEND_SMS_SUCCESS';
export const SEND_SMS_ERROR = 'SEND_SMS_ERROR';

export const sendSms = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'SEND_SMS',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/sendsms', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const VERIFY_CODE_START = 'VERIFY_CODE_START';
export const VERIFY_CODE_SUCCESS = 'VERIFY_CODE_SUCCESS';
export const VERIFY_CODE_ERROR = 'VERIFY_CODE_ERROR';

export const verifyCode = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'VERIFY_CODE',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/verifycode', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const SAVE_CONTACTS_START = 'SAVE_CONTACTS_START';
export const SAVE_CONTACTS_SUCCESS = 'SAVE_CONTACTS_SUCCESS';
export const SAVE_CONTACTS_ERROR = 'SAVE_CONTACTS_ERROR';

export const saveContacts = (params: Object) => ({ fetch }: { fetch: Function }) => ({
	type: 'SAVE_CONTACTS',
	payload: {
		search: 'something',
		promise: fetchFn(fetch, '/admin/api/v1/listing/savecontacts', 'POST', params)
			.then(response => response.json())
			.catch(err => console.log('error', err)),
	},
});

export const GET_ZONE_START = 'GET_ZONE_START';
export const GET_ZONE_SUCCESS = 'GET_ZONE_SUCCESS';
export const GET_ZONE_ERROR = 'GET_ZONE_ERROR';

export const getZone = (params: Object) => {
	return ({ fetch }: { fetch: Function }) => ({
		type: 'GET_ZONE',
		payload: {
			promise: fetch('/admin/api/v1/listing/reverse', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
				body: JSON.stringify(params),
			})
				.then(response => response.json())
				.catch(err => console.log('error', err)),
		},
	});
};

export const LOCATION_START = 'LOCATION_START';
export const LOCATION_SUCCESS = 'LOCATION_SUCCESS';
export const LOCATION_ERROR = 'LOCATION_ERROR';

export const getLocation = (params: Object) => {
	return ({ fetch }: { fetch: Function }) => ({
		type: 'LOCATION',
		payload: {
			promise: fetch('/admin/api/v1/listing/location', {
				method: 'POST',
				headers: { 'Content-Type': 'application/json', Connection: 'keep-alive' },
				body: JSON.stringify(params),
			})
				.then(response => response.json())
				.catch(err => console.log('error', err)),
		},
	});
};
