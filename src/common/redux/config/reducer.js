// @flow
const initialState = {
	appName: '',
	appVersion: '',
};

const reducer = (state: Object = initialState) => state;

export default reducer;
