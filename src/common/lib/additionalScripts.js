// @flow
import React from 'react';

/*
* ANALYTICS
*/

/* eslint-disable */
export const analytics = ({ googleAnalyticsId }: { googleAnalyticsId: string }) => (
	<script key="analytics" type="text/javascript" dangerouslySetInnerHTML={{ __html: `!function(e,t,a,n,r){e[n]=e[n]||[],e[n].push({"gtm.start":(new Date).getTime(),event:"gtm.js"});var g=t.getElementsByTagName(a)[0],m=t.createElement(a),s="dataLayer"!=n?"&l="+n:"";m.async=!0,m.src="https://www.googletagmanager.com/gtm.js?id="+r+s,g.parentNode.insertBefore(m,g)}(window,document,"script","dataLayer","${googleAnalyticsId}")` }} />
);
/* eslint-enable */

export const analyticsIframe = ({ googleAnalyticsId }: { googleAnalyticsId: string }) => (
	<noscript>
		<iframe src={`https://www.googletagmanager.com/ns.html?id=${googleAnalyticsId}`} width="0" height="0" style={{ display: 'none', visibility: 'hidden' }} title="analyticsIframe" />
	</noscript>
);

/* GOOGLE MAPS */
export const gmaps = () => (
	<script
		key="gmaps" 
		type="text/javascript"
		src="https://maps.googleapis.com/maps/api/js?v=3.36&client=gme-casaitsrl&language=it&channel=casa-portal&libraries=places,geometry&components=country:IT" />
);

export const staticAssets = () => (
	<link type="text/css" rel="stylesheet" href="/admin/assets/style/style.css?v=1" />
);

export const staticAssetsDepthForm = () => (
	<link type="text/css" rel="stylesheet" href="/admin/assets/style/depth-form.css?v=1" />
);

export const composeAdditionalStaticScripts = (scripts: any, ...args: any) => (scripts && scripts.map(item => item.length > 0 ? item(...args) : item())) || null;
