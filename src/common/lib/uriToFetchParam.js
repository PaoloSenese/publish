// @flow
/* eslint-disable dot-notation */

export const uriToListingFetchParams = (uri: string, configs: Object, jwt: string) => ({
	uri,
	configs,
	jwt,
});

export const uriToListingsFetchParams = (uri: string, configs: Object, jwt: string) => ({
	uri,
	configs,
	jwt,
});

export const uriToRequetsFetchParams = (uri: string, configs: Object, jwt: string) => ({
	uri,
	configs,
	jwt,
});
