function isPlaceholder(a) {
	return a != null && typeof a === 'object' && a['@@functional/placeholder'] === true;
}

/**
 * Optimized internal one-arity curry function.
 *
 * @private
 * @category Function
 * @param {Function} fn The function to curry.
 * @return {Function} The curried function.
 */
function curry1(fn) {
	return function f1(a) {
		console.log('curry1', a, arguments);
		if (arguments.length === 0 || isPlaceholder(a)) {
			return f1;
		}
		return fn.apply(this, arguments);
	};
}

/**
 * Optimized internal two-arity curry function.
 *
 * @private
 * @category Function
 * @param {Function} fn The function to curry.
 * @return {Function} The curried function.
 */
function curry2(fn) {
	return function f2(a, b) {
		switch (arguments.length) {
		case 0:
			return f2;
		case 1:
			return isPlaceholder(a) ? f2 : curry1(_b => fn(a, _b));
		default:
			return isPlaceholder(a) && isPlaceholder(b) ? f2 : isPlaceholder(a) ? curry1(_a => fn(_a, b)) : isPlaceholder(b) ? curry1(_b => fn(a, _b)) : fn(a, b);
		}
	};
}

/**
 * Returns a partial copy of an object containing only the keys specified. If
 * the key does not exist, the property is ignored.
 *
 * @func
 * @memberOf R
 * @since v0.1.0
 * @category Object
 * @sig [k] -> {k: v} -> {k: v}
 * @param {Array} names an array of String property names to copy onto a new object
 * @param {Object} obj The object to copy from
 * @return {Object} A new object with only properties from `names` on it.
 * @see R.omit, R.props
 * @example
 *
 *			R.pick(['a', 'd'], {a: 1, b: 2, c: 3, d: 4}); //=> {a: 1, d: 4}
 *			R.pick(['a', 'e', 'f'], {a: 1, b: 2, c: 3, d: 4}); //=> {a: 1}
 */
export const pick = curry2((names, obj) => {
	console.log('pick');
	const result = {};
	let idx = 0;
	while (idx < names.length) {
		if (names[idx] in obj) {
			result[names[idx]] = obj[names[idx]];
		}
		idx += 1;
	}
	return result;
});
