// @flow
import promiseMiddleware from 'redux-promise-middleware';
import { createLogger } from 'redux-logger';
import createFetch from './createFetch';

// Like redux-thunk, but with just one argument for dependencies.
const injectMiddleware = deps => ({ dispatch, getState }: { dispatch: Function, getState: Function }) => (next: Function) => (action: Function) => next(
	typeof action === 'function'
		? action({ ...deps, dispatch, getState })
		: action,
);

const configureMiddleware = (
	initialState: Object,
	platformDeps: Object,
	platformMiddleware: any,
) => {
	// Remember to set SERVER_URL for deploy.
	const serverUrl = process.env.SERVER_URL
		|| (process.env.IS_BROWSER ? '' : `http://127.0.0.1:${process.env.PORT || 3002}`);

	const middleware = [
		injectMiddleware({
			...platformMiddleware,
			fetch: createFetch(serverUrl),
			now: () => Date.now(),
		}),
		promiseMiddleware({
			promiseTypeSuffixes: ['START', 'SUCCESS', 'ERROR'],
		}),
	];

	// Enable logger only for browser and development.
	const enableLogger = process.env.NODE_ENV !== 'production' && process.env.IS_BROWSER;

	// Logger must be the last middleware in chain.
	if (enableLogger) {
		const logger = createLogger({
			collapsed: true,
		});
		middleware.push(logger);
	}

	return middleware;
};

export default configureMiddleware;
