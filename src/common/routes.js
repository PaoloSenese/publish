// @flow
import ListingApp from '../client/containers/ListingApp';
import ListingsApp from '../client/containers/ListingsApp';

import {
	uriToListingFetchParams, uriToListingsFetchParams
} from './lib/uriToFetchParam';
import {
	analytics,
	analyticsIframe,
	gmaps,
	staticAssets,
	staticAssetsDepthForm
} from './lib/additionalScripts';

export const routes = [
	{
		path: /^(\/listing\/)(\d+\/)(step-)(2|3|4|5)*$/,
		exact: true,
		component: ListingApp,
		assets: 'listing',
		fetchParams: uriToListingFetchParams,
		headScripts: [analytics, analyticsIframe, staticAssets, staticAssetsDepthForm],
		footerScripts: [],
	},
	{
		path: /^(\/listing\/)(new|(\d+\/)(step-1))*$/,
		exact: true,
		component: ListingApp,
		assets: 'listing',
		fetchParams: uriToListingFetchParams,
		headScripts: [analytics, analyticsIframe, gmaps, staticAssets],
		footerScripts: [],
	},
	{
		path: /^(\/listings|\/requests|\/profile|\/thank-you)(\/\d+|.*)*$/,
		exact: true,
		component: ListingsApp,
		assets: 'listings',
		fetchParams: uriToListingsFetchParams,
		headScripts: [analytics, analyticsIframe, staticAssets],
		footerScripts: [],
	},
];
