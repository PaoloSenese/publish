// @flow
import { combineReducers } from 'redux';
import config from './redux/config/reducer';
import listing from './redux/listing/reducer';
import listings from './redux/listings/reducer';

const configureReducer = (platformReducers: Object) => {
	const reducer = combineReducers({
		...platformReducers,
		config,
		listing,
		listings,
	});

	return reducer;
};

export default configureReducer;
