// @flow
import React from 'react';
import PropTypes from 'prop-types';
import sjwt from 'jsonwebtoken';

import UiHandlerHOC from '../app/UiHandler';
import App from '../app/App';
import Header from '../components/common/header/Header';
import Steps from './Steps';
import SwitchComponents from './SwitchComponents';

// flow-disable-next-line
import './ListingPage.scss';

const el = React.createElement;

class ListingPageComponent extends React.Component<any, any> {

	state = {
		location: this.props.history.location,
		dataObject: this.props.listing.dataObject,
		paging: this.props.listing.paging,
		loadSpinner: false,
	}

	componentWillReceiveProps() {
		const { location } = window;
		const pathArray = location.pathname.split('/');

		if (pathArray.length === 5 && pathArray[3] === 'step-3' && Number.isNaN(Number(pathArray[4])) && pathArray[4] !== 'new') {
			const { total, currentPage } = this.state.paging;
			const wrongPaging = total <= 10 && currentPage > 1;

			if (wrongPaging) {
				location.href = `/listing/${location.pathname.split('/')[2]}/step-3/page-1${this.state.location.search}`;
			}

			if (this.state.loadSpinner === false && (this.props.history.location.search !== this.state.location.search || this.props.history.location !== this.state.location || parseInt(currentPage, 10) !== parseInt(pathArray[4].replace('page-', ''), 10))) {
				this.setState({
					location: this.props.history.location,
					loadSpinner: true
				});

				const { config } = this.props;
				const { jwt } = config;
				const { pathname, search } = this.props.history.location;
				const uri = `${pathname}${search}`;

				this.props.actions.fetchListing({
					jwt, uri
				}).then((e) => {
					this.setState({
						dataObject: e.value.dataObject,
						paging: e.value.paging,
						loadSpinner: false,
					});
				});
			}
		}
	}

	render() {
		const { device, location, listing, actions, viewport, config, history, utils } = this.props;
		const { imageserver, radioContainers, mapTexts,
			editListingTexts, mediaTexts, contactsTexts, stepsTexts, jwt, citiesArray, localMode, tileLayer } = config;
		const { listingDetail, depths, depthsObject, productPricings } = listing;
		const { hasMedia } = listingDetail;
		const { paging, dataObject } = this.state;
		const pathArray = location.pathname.split('/');
		const currentStep = location.pathname.split('/')[3] ? location.pathname.split('/')[3] : 'step-1';
		const baseUrl = location.pathname.replace(currentStep, '');
		const { user } = sjwt.decode(jwt);
		const progress = (parseInt(currentStep.replace('step-', ''), 10) / 5) * 100;

		return el(App, Object.assign({}, this.props, { title: pathArray[2] === 'new' ? 'Publisher - Nuovo Annuncio' : `Publisher - Modifica Annuncio ${pathArray[2]}` }),
			el(Header, {
				user,
				device,
				adminUrl: config.admin,
				links: [
					{ name: 'Annunci', path: '/listings', sub: [] },
					{ name: 'Richieste', path: '/requests', sub: [] },
					{ name: 'Profilo', path: '/profile', sub: [] }
				],
				imageserver
			}),
			el('div', { className: 'container' },
				currentStep === 'thank-you' ? null : el('div', { className: 'progress-bar-label box-shadow', style: { left: `calc(${progress}% - ${progress === 100 ? `33px` : '15px'})` } }, `${progress}%`),
				el('div', { className: 'progress-bar' },
					currentStep !== 'thank-you' ?
						el('div', { className: 'progress-bar-red', style: { width: `${progress}%` } })
						:
						el('div', { className: 'progress-bar-green', style: { width: '100%' } })),
				el('div', { className: pathArray[3] === 'publish' ? 'depth-container' : 'project' },
					el('div', { className: 'project-cont' },
						el(Steps, { currentStep, baseUrl, stepsObject: stepsTexts, isNew: location.pathname.split('/')[2] === 'new', hasMedia, contactsTexts }),
						el(SwitchComponents, {
							currentStep,
							baseUrl,
							stepsObject: stepsTexts,
							actions,
							dataObject,
							device,
							viewport,
							pathname: location.pathname,
							jwt,
							imageserver,
							radioContainers,
							mapTexts,
							editListingTexts,
							mediaTexts,
							history,
							paging,
							depths,
							citiesArray,
							localMode,
							tileLayer,
							utils,
							depthsObject,
							productPricings
						})))));
	}
}

ListingPageComponent.propTypes = {
	listing: PropTypes.instanceOf(Object).isRequired,
	device: PropTypes.string.isRequired,
	actions: PropTypes.instanceOf(Object).isRequired,
};

const ListingPage = UiHandlerHOC(ListingPageComponent, 'fetchListing');
export default ListingPage;
