// @flow
import React from 'react';
import Form from 'client-form';
import { arrayMove } from 'react-sortable-hoc';
import MediaContainer from './media/MediaContainer';
import Alert from '../components/common/CustomControls/Alert';
// flow-disable-next-line
import './Media.scss';

const el = React.createElement;

const mediaArrays = media => [
	{ label: 'Foto', imgLimit: 50, imageType: 'F', prefix: 'listing', items: media ? media.filter(o => o.imageType === 'F') : [] },
	{ label: 'Planimetrie', imgLimit: 4, imageType: 'P', prefix: 'fp', items: media ? media.filter(o => o.imageType === 'P') : [] },
];

class Media extends React.Component<any, any> {

	state = {
		media: mediaArrays(this.props.item.media),
		sortStart: false,
		sortFpStart: false,
		handleAction: false,
		deleteError: null,
		loadSpinner: false,
		alertHeight: 365,
		icon: '',
		whichAction: '',
		actionTextBlack: '',
		actionTextRed: '',
		actionTitle: '',
		listing: this.props.item,
		confirmAction: null
	};

	getMedia(mediaObject: Object) {
		this.props.actions.getMedia(mediaObject).then((x) => {
			const media = x.value.response.data.data.listingImages;

			this.setState({
				media: mediaArrays(media),
			});
		});
	}

	onSortEnd(e: Object) {
		// flow-disable-next-line
		document.body.style.overflow = 'unset';

		const { media } = this.state;
		// flow-disable-next-line
		const { imageId } = this.state.movedItem.items[e.oldIndex];
		const index = this.state.sortStart ? 0 : 1;
		const newArray = arrayMove(media[index].items, e.oldIndex, e.newIndex);
		media[index].items = newArray;

		this.setState({
			isLoading: true,
		});

		this.props.actions.moveMedia({
			listingId: media[index].items[e.oldIndex].listingId,
			imageId,
			jwt: this.props.jwt,
			newPos: e.newIndex,
		}).then(() => {
			if (this.state.sortStart) {
				this.setState({
					sortStart: false,
					isLoading: false,
				});
			} else if (this.state.sortFpStart) {
				this.setState({
					sortFpStart: false,
					isLoading: false,
				});
			}
		});
	}

	onSortStart(item: Object) {
		// flow-disable-next-line
		document.body.style.overflow = 'hidden';

		if (item.imageType === 'F') {
			this.setState({
				movedItem: item,
				sortStart: true
			});
		} else if (item.imageType === 'P') {
			this.setState({
				movedItem: item,
				sortFpStart: true
			});
		}

		const itemCount = document.getElementsByClassName('item').length;
		const e = document.getElementsByClassName('item')[itemCount - 1];
		e.className += ' hide-actions';
	}

	deleteAllMedia(imageType: string) {
		this.setState({
			isLoading: true,
		});

		this.props.actions.deleteAllMedia({
			listingId: this.state.media.filter(o => o.imageType === imageType)[0].items[0].listingId,
			imageType,
			jwt: this.props.jwt,
		}).then((x) => {
			if (x.value.response.succeed) {
				const { media } = this.state;
				const mediaUpdate = [
					{ label: 'Foto', imgLimit: 50, imageType: 'F', prefix: 'listing', items: imageType === 'F' ? [] : media.filter(o => o.imageType === 'F') },
					{ label: 'Planimetrie', imgLimit: 4, imageType: 'P', prefix: 'fp', items: imageType === 'P' ? [] : media.filter(o => o.imageType === 'P') },
				];

				this.setState({
					media: mediaUpdate,
					isLoading: false,
					deleteError: false,
				});
			} else {
				this.setState({
					isLoading: false,
					deleteError: true
				});
			}
		});
	}

	openActionOverlay(which: string) {
		// flow-disable-next-line
		document.body.style.overflow = 'hidden';

		this.setState({
			handleAction: true,
			whichAction: which,
			actionTitle: 'Confermi l\'eliminazione?',
			actionTextRed: 'Attenzione! L\'operazione è irreversibile.',
			actionTextBlack: <span>Cliccando su <strong>conferma</strong> tutte le immagini verranno eliminate<br /> e non sarà più possibile recuperarle.</span>,
			icon: '/admin/assets/img/icons/icon-bin-big.svg',
			deleteError: null,
			alertHeight: 365,
			confirmAction: () => { this.deleteAllMedia(which); },
			hideActions: false
		});
	}

	closeActionOverlay() {
		if (this.state.handleAction === true) {
			// flow-disable-next-line
			document.body.style.overflow = 'unset';
			this.setState({
				handleAction: false,
				whichAction: null,
				actionTitle: null,
				actionTextRed: null,
				actionTextBlack: null,
				deleteError: null
			});
		}
	}

	resetAction() {
		if (this.state.deleteError !== null) {
			// flow-disable-next-line
			document.body.style.overflow = 'unset';
			this.setState({
				handleAction: false,
				whichAction: null,
				actionTitle: null,
				actionTextRed: null,
				actionTextBlack: null,
				deleteError: null
			});
		}
	}

	openInfoOverlay() {
		// flow-disable-next-line
		document.body.style.overflow = 'hidden';

		this.setState({
			handleAction: true,
			actionTitle: 'Consigli per le tue foto',
			actionTextBlack: 'we need a text here',
			icon: '/admin/assets/img/icons/icon-bin-big.svg',
			deleteError: null,
			alertHeight: 365,
			hideActions: true
		});
	}

	render() {
		const {
			actions, viewport, jwt, imageserver,
			mediaTexts, editListingTexts
		} = this.props;
		const {
			handleAction, deleteError,
			actionTitle, actionTextRed, actionTextBlack, whichAction,
			loadSpinner, icon, alertHeight,
			listing, confirmAction, hideActions
		} = this.state;

		return el('div', { className: 'project-media' },
			el('div', { className: 'project-media-title' }, 'Foto e descrizione'),
			el('div', { className: 'project-media-subtitle' }, 'Aggiungi foto, video, planimetrie e una descrizione del tuo immobile per migliorare l\'efficacia del tuo annuncio'),
			el('div', { className: 'description' },
				el('span', {}, 'Un immobile con molte foto è più attraente e sarà classificato meglio nell\'elenco dei risultati. Se non hai foto ora, puoi sempre aggiungerle in un secondo momento.'),
				el('a', { onClick: () => { this.openInfoOverlay(); }, style: { color: '#e4002b', cursor: 'pointer' } }, 'Consigli per le foto'),
			),
			el('div', { className: 'project-media-content' },
				this.state.media.map(item => el('div', {
					className: this.state.isLoading ? 'media-container first-media-container loader' : 'media-container first-media-container',
					key: `media_${item.imageType}`,
					style: item.imageType !== 'F' ? { marginTop: 30, paddingTop: 0 } : { }
				},
				el(MediaContainer, {
					id: this.props.item.listingId,
					items: item.items,
					label: item.label,
					imageType: item.imageType,
					prefix: item.prefix,
					inputLabel: 'https://www.youtube.com/watch?v=',
					axis: 'xy',
					distance: 5,
					viewport,
					lockToContainerEdges: true,
					sortStart: item.imageType === 'F' ? this.state.sortStart : this.state.sortFpStart,
					onSortStart: () => { this.onSortStart(item); },
					onSortEnd: (e) => { this.onSortEnd(e); },
					jwt,
					imageserver,
					actions,
					limit: item.imgLimit - item.items.length,
					imgLimit: item.imgLimit,
					mediaTexts,
					deleteAllMedia: (o) => { this.openActionOverlay(o); },
					getMedia: (o) => { this.getMedia(o); },
					disableAutoscroll: true
				},
				item.imageType === 'P' ?
					el('div', { style: { fontSize: 12, }, className: 'noselect' },
						el('div', { className: 'description media-container-swap' }, mediaTexts.genericTexts[1]),
						el('div', { className: 'description' },
							el('strong', {}, mediaTexts.genericTexts[4]),
							`${mediaTexts.genericTexts[5]} ${mediaTexts.genericTexts[6]}`
						))
					:
					null))),
				el(Form, {
					key: 'xxx',
					controls: [
						{
							control: 'text',
							type: 'hidden',
							name: 'listingId',
							onlyNumber: false,
							isRequired: false,
							value:	listing.listingId,
							isValid: true,
							style: {
								display: 'none',
							}
						},
						{
							control: 'label',
							name: 'title3',
							className: 'noselect',
							content: el('div', { style: { position: 'relative', height: 24, paddingTop: 30 } },
								el('div', { style: { position: 'absolute', bottom: 0, fontSize: 18 } }, editListingTexts.descTitle)),
							style: { fontWeight: 700, marginBottom: 15, marginTop: 30, height: 24, width: '100%', clear: 'both' },
						},
						{
							control: 'label',
							name: 'subdesc',
							className: 'noselect',
							content: editListingTexts.descSubTitle,
							style: { height: 'auto', clear: 'both', width: '100%', fontSize: 12, color: '#697684', fontWeight: 500, marginBottom: 30 },
						},
						{
							control: 'tabTextArea',
							name: 'description',
							label: { style: { display: 'none' } },
							value:	listing.description,
							className: 'tabTextArea',
							isRequired: true,
							isValid: true,
							errorMessage: 'La descrizione in italiano è obbligatoria',
							valueAsObject: true,
							limitChar: 4000,
							style: { margin: 0 }
						}
					],
					beforeButton: el('div', {
						className: 'btn btn-white',
						style: { float: 'left', cursor: 'pointer', height: 40, lineHeight: '40px', width: 100, textAlign: 'center', marginTop: 30 },
						/* eslint-disable-next-line */
						onClick: () => { location.href = '/listing'; }
					}, 'Indietro'),
					buttonContainerStyle: { float: 'right', width: 'auto', marginTop: 30 },
					sendButton: {
						text: 'Salva e prosegui',
						style: { width: 'auto', float: 'left' }
					},
					sendForm: (o) => {
						console.log(o);
						/* eslint-disable */
						const promise = new Promise((resolve, reject) => {
							this.props.actions.fetchSaveListing({ jwt: this.props.jwt, dataObject: o, simple: true })
								.then((x) => {
									if (x.value.response.succeed) {
										resolve({ succeed: true, message: x.value.response.message });
										setTimeout(() => {
											//window.location.href = `/listing/${listing.listingId}/step-4`;
										}, 1000);
									} else {
										reject({ succeed: false, message: x.value.response.message });
									}
								});
							});
							return promise;
						/* eslint-enable */
					},
					formStyle: { padding: '40px 0px 0px', width: '100%', display: 'inline-block' }
				}),
				handleAction ?
					el(Alert, {
						closeActionOverlay: () => { this.closeActionOverlay(); },
						viewport,
						actionTitle,
						resetAction: () => { this.resetAction(); },
						handleAction,
						deleteError,
						confirmAction,
						whichAction,
						actionTextRed,
						actionTextBlack,
						successMessage: whichAction === 'F' || whichAction === 'P' || whichAction === 'G' ? 'Immagini eliminate' : whichAction === 'V' || whichAction === 'W' ? 'Video eliminati' : 'Capitolato eliminato',
						icon,
						loadSpinner,
						alertHeight,
						hideActions
					})
					: null
			));
	}
}

export default Media;
