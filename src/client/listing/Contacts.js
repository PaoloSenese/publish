// @flow
import React from 'react';
import Form from 'client-form';
import PhoneValidator from './components/PhoneValidator';
// flow-disable-next-line
import './Contacts.scss';

const el = React.createElement;

class Contacts extends React.Component<any, any> {

	shouldComponentUpdate(nextProps: Object) {
		if (nextProps.viewport !== this.props.viewport) return true;
		return false;
	}

	render() {
		const { viewport, actions, jwt, pathname } = this.props;

		return el(Form, {
			controls: [
				{
					control: 'label',
					content: el('div', { className: 'step-title' },
						el('div', {}, 'Contatti'),
						el('div', {}, 'Inserisci i recapiti dove vuoi essere contattato dalle persone interessate a questo annuncio')
					),
					style: { width: '100%', clear: 'both', marginBottom: 30 }
				},
				{
					control: 'text',
					type: 'text',
					name: 'firstName',
					isRequired: true,
					value: 'Joe'
				},
				{
					control: 'text',
					type: 'text',
					name: 'lastName',
					isRequired: true,
					value: 'Strummer',
					className: 'container-field-right',
				},
				{
					control: 'text',
					type: 'text',
					name: 'email',
					isRequired: true,
					value: 'joe.strummer@clash.co.uk',
				},
				{ control: 'label', style: { width: '100%', clear: 'both' } },
				{
					control: 'external',
					component: PhoneValidator,
					name: 'phoneValidator',
					key: 'phoneValidator',
					exclude: true,
					viewport,
					jwt,
					actions,
					prefix: '+39',
					phoneNumber: '3486009689'
				},
				{
					control: 'check',
					name: 'hideMobile',
					label: { text: 'Non mostrare sull\'annuncio' },
					value: false,
					className: 'all-width check'
				},
				{ control: 'label', style: { width: '100%', clear: 'both', marginBottom: 30 } }
			],
			beforeButton: el('div', {
				className: 'btn btn-white',
				style: {
					float: 'left',
					cursor: 'pointer',
					height: 40,
					lineHeight: '40px',
					width: 100,
					textAlign: 'center',
				},
				/* eslint-disable-next-line */
				onClick: () => { location.href = '/listings'; }
			}, 'Annulla'),
			sendButton: {
				text: 'Salva e prosegui',
				style: { width: 'auto', float: 'right' }
			},
			sendForm: (o) => {
				/* eslint-disable */
				console.log(o);
				var promise = new Promise((resolve, reject) => {
					this.props.actions.saveContacts({ jwt, o })
						.then((x) => {
							setTimeout(() => {
								if (x.value.response.succeed) {
									resolve({ succeed: true, message: x.value.response.message });
									window.location.href = pathname.replace('step-4', 'step-5');
								} else {
									reject({ succeed: false, message: x.value.response.message });
								}
							}, 2000);
						});
				});
				return promise;
				/* eslint-enable */
			},
			buttonContainerStyle: {
				width: 'auto',
				float: 'right'
			},
			formClassName: 'contacts'
		});
	}
}

export default Contacts;
