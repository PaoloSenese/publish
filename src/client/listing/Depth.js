// @flow
import React from 'react';
import Form from 'client-form';
import CustomRadioDepth from './components/CustomRadioDepth';
import PricingCustomObject from './components/PricingCustomObject';

// flow-disable-next-line
import './Depth.scss';

const el = React.createElement;

class Depth extends React.Component<any, any> {

	state = {
		randomKey: this.props.utils.makeId(),
		selectedDepth: this.props.item.depth.productTypeId,
	};

	render() {
		const { actions, jwt, radioContainers, depthsObject, productPricings } = this.props;
		const { selectedDepth } = this.state;
		const productPricingsOptions = productPricings.map(x => ({
			label: `${x.period} mesi`,
			value: x.price,
			type: [x.productType],
			selectedClassName: 'selected option-override',
			customObject: el(PricingCustomObject, {
				discount: `-${parseInt((x.originalPrice / 100) * (x.originalPrice - x.price), 10)}%`,
				period: `${x.period} mesi`,
				price: x.price,
				originalPrice: x.originalPrice })
		}));
console.log('depthsObject', depthsObject);
		return el(Form, {
			formStyle: { clear: 'both' },
			key: this.state.randomKey,
			controls: [
				{
					control: 'label',
					content: el('div', { className: 'step-title' },
						el('div', {}, 'Formato dell\'annuncio visibilità e durata')),
					style: { width: '100%', clear: 'both', marginBottom: 30 }
				},
				{
					control: 'label',
					content: el('div', { style: { marginLeft: 10 } },
						el('div', { style: { fontSize: 14, fontWeight: 700 } }, 'Qual\'è il tuo obiettivo?'),
						el('div', {}, 'Possiamo offrirti la soluzione migliore per raggiungere il tuo scopo')),
				},
				{
					control: 'radio',
					name: 'object',
					label: { text: '' },
					options: [
						{ value: 1, label: 'Vendere il prima possibile', style: { float: 'left' }, selectedClassName: 'option-override' },
						{ value: 2, label: 'Vendere al miglior prezzo', style: { float: 'left' }, selectedClassName: 'option-override' },
					],
					value: 1
				},
				{ control: 'label', content: radioContainers.title, style: { fontSize: 14, fontWeight: 700, clear: 'both', marginLeft: 10 } },
				{
					control: 'radio',
					name: 'depth',
					label: { text: '' },
					options: depthsObject.map(x => ({
						name: x.name,
						value: x.productType,
						className: 'custom-radio-options',
						selectedClassName: 'selected option-override',
						customObject: el(CustomRadioDepth, Object.assign({}, radioContainers.radioDepth.filter(o => o.productType === x.productType)[0], { value: x.productType, selectedDepth }))
					})),
					highlightSel: true,
					value: selectedDepth,
					hideRadio: false,
					className: 'custom-depth-container',
				},
				{
					control: 'radio',
					name: 'price',
					label: { text: 'Seleziona la durata', style: { fontSize: 14, height: 30 } },
					options: productPricingsOptions.filter(o => o.type.indexOf(selectedDepth) !== -1),
					value: 55,
					optionIf: [
						{
							field: 'depth',
							options: productPricingsOptions
						},
					],
					hide: selectedDepth === 15,
					hideIf: [
						{ field: 'depth', regEx: /^15$/ }
					],
					className: 'custom-radio-pricing all-width',
					hideRadio: true,
					isRequired: true,
					errorMessage: 'Devi selezionare il periodo',
					style: { marginTop: 10, maxWidth: 335 }
				},
				{ control: 'label', style: { width: '100%', clear: 'both' } },
				{
					control: 'radio',
					name: 'paymentMethod',
					label: { text: 'Scegli il metodo di pagamento', style: { fontSize: 14, height: 30 } },
					options: [
						{
							label: '',
							value: 1,
							customObject: el('div', { className: 'pricing-container' },
								el('div', { className: 'pay' },
									el('img', { src: '/admin/assets/img/group-11.png' })),
								el('div', {},
									el('svg', { width: 24, height: 24, viewBox: '0 0 24 24' },
										el('circle', { className: 'ext', cx: 12, cy: 12, r: 9, stroke: 'rgb(216, 216, 223)', strokeWidth: 2 }),
										el('circle', { className: 'int', cx: 12, cy: 12, r: 4 })))),
							selectedClassName: 'selected option-override'
						},
						{
							label: '',
							value: 2,
							customObject: el('div', { className: 'pricing-container' },
								el('div', { className: 'pay' },
									el('img', { src: '/admin/assets/img/pay-pal-full-color-horizontal.png' })),
								el('div', {},
									el('svg', { width: 24, height: 24, viewBox: '0 0 24 24' },
										el('circle', { className: 'ext', cx: 12, cy: 12, r: 9, stroke: 'rgb(216, 216, 223)', strokeWidth: 2 }),
										el('circle', { className: 'int', cx: 12, cy: 12, r: 4 })))),
							selectedClassName: 'selected option-override'
						}
					],
					value: 2,
					default: 2,
					hide: selectedDepth === 15,
					hideIf: [
						{ field: 'depth', regEx: /^15$/ }
					],
					className: 'custom-radio-pricing all-width',
					hideRadio: true,
					isRequired: true,
					errorMessage: 'Devi selezionare un metodo di pagamento',
					style: { marginTop: 20, maxWidth: 335 }
				},
				{
					control: 'label',
					content: '',
					style: {
						clear: 'both',
						width: '100%',
						paddingBottom: 20,
						borderBottom: '1px solid #d8dbdf'
					}
				}
			],
			formClassName: 'custom-depth-form',
			beforeButton: el('div', {
				className: 'btn btn-white',
				style: {
					float: 'left',
					cursor: 'pointer',
					height: 40,
					lineHeight: '40px',
					width: 100,
					textAlign: 'center',
					marginTop: 20
				},
				/* eslint-disable-next-line */
				onClick: () => { location.href = '/listings'; }
			}, 'Annulla'),
			sendButton: {
				text: 'Salva e prosegui',
				style: { width: 'auto', float: 'right' }
			},
			buttonContainerStyle: {
				width: 'auto',
				float: 'right',
				padding: '20px 0px',
			},
			sendForm: (o) => {
				console.log(o);
				/* eslint-disable */
				const promise = new Promise((resolve, reject) => {
					actions.fetchBuy({ jwt, dataObject: o })
						.then((x) => {
							setTimeout(() => {
								if (x.value.response.succeed) {
									resolve({ succeed: true, message: x.value.response.message });
								} else {
									reject({ succeed: false, message: x.value.response.message });
								}
							}, 2000);
						});
					});
					return promise;
					/* eslint-enable */
			},
		});
	}
}

export default Depth;
