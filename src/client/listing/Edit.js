// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Form from 'client-form';

import CustomMap from '../components/common/CustomControls/CustomMap';
import { tipologies } from '../../common/helpers/var';

// flow-disable-next-line
import './Edit.scss';

const el = React.createElement;

class Edit extends React.Component<any, any> {

	static defaultProps = {
		item: null
	};

	state = {
		randomKey: this.props.utils.makeId(),
		listing: this.props.item,
	};

	shouldComponentUpdate(nextProps: Object) {
		if (this.props.viewport.width !== nextProps.viewport.width) return true;
		return false;
	}

	render() {
		const { actions, jwt, viewport, mapTexts, citiesArray, localMode, tileLayer, utils } = this.props;
		const { listing } = this.state;

		return el(Form, {
			key: this.state.randomKey,
			controls: [
				{
					control: 'label',
					content: el('div', { className: 'step-title' },
						el('div', {}, 'Informazioni'),
						el('div', {}, 'Inserisci le informazioni generali del tuo immobile')),
					style: { width: '100%', clear: 'both', marginBottom: 30, height: 40 }
				},
				{
					control: 'text',
					type: 'hidden',
					name: 'listingId',
					onlyNumber: false,
					isRequired: false,
					value:	listing.listingId,
					isValid: true,
					style: { display: 'none' }
				},
				{
					control: 'text',
					type: 'hidden',
					name: 'listingEnabled',
					onlyNumber: false,
					isRequired: false,
					value:	listing.listingEnabled.toString(),
					isValid: true,
					style: { display: 'none' }
				},
				{
					control: 'radio',
					name: 'contractTypeId',
					label: { text: 'Contratto' },
					value: listing.contractTypeId ? listing.contractTypeId : 2,
					hideRadio: false,
					options: [
						{ value: 1, label: 'Affitto', style: { width: 137, float: 'left' }, selectedClassName: 'option-override' },
						{ value: 2, label: 'Vendita', style: { width: 137, float: 'left' }, selectedClassName: 'option-override' },
					],
				},
				{
					control: 'check',
					name: 'hasPurchaseOption',
					label: { text: 'Opzione d\'acquisto' },
					value: listing.hasPurchaseOption ? listing.hasPurchaseOption : false,
					hideCheck: true,
					hide: parseFloat(listing.contractTypeId) === 2,
					hideIf: [
						{ field: 'contractTypeId', regEx: /^2$/ }
					],
				},
				{
					control: 'radio',
					name: 'categoryTypeId',
					label: { text: 'Categoria' },
					value: listing.categoryTypeId ? listing.categoryTypeId : 1,
					hideRadio: false,
					options: [
						{ value: 1, label: 'Residenziale', style: { width: 137, float: 'left' }, selectedClassName: 'option-override' },
						{ value: 2, label: 'Vacanze', style: { width: 137, float: 'left' }, selectedClassName: 'option-override' },
						{ value: 4, label: 'Commerciale', style: { width: 137, float: 'left' }, selectedClassName: 'option-override' },
					],
					className: 'all-width'
				},
				{
					control: 'select',
					name: 'propertyTypeId',
					label: { text: 'Tipologia immobile' },
					options: tipologies.filter(o => o.type.indexOf(parseFloat(listing.categoryTypeId ? listing.categoryTypeId : 1)) !== -1),
					value: listing.propertyTypeId ? listing.propertyTypeId : ' ',
					optionIf: [
						{
							field: 'categoryTypeId',
							options: tipologies
						},
					],
					isRequired: true,
					errorMessage: 'Campo obbligatorio',
				},
				{
					control: 'select',
					name: 'occupationStateId',
					label: { text: 'Stato al rogito' },
					options: [
						{ value: -1, label: 'Seleziona...', className: 'first' },
						{ value: 1, label: 'occupato', className: 'central' },
						{ value: 2, label: 'libero', className: 'central' },
						{ value: 3, label: 'nuda proprietà', className: 'central' },
						{ value: 4, label: 'affittato', className: 'last' },
					],
					default: -1,
					value:	listing.occupationStateId ? listing.occupationStateId : -1,
					className: 'container-field-right',
					hide: parseFloat(listing.contractTypeId) === 1,
					hideIf: [
						{ field: 'contractTypeId', regEx: /^1$/ }
					],
				},
				{
					control: 'label',
					content: 'Indirizzo immobile',
					className: 'step-separator noselect'
				},
				{
					control: 'external',
					component: CustomMap,
					name: 'geolocation',
					key: 'geolocation',
					google: this.props.google,
					value:	listing.geolocation,
					className: 'map',
					actions,
					jwt,
					isValid: true,
					mapTexts,
					valueAsObject: true,
					isRequired: true,
					exclude: false,
					viewport,
					citiesArray,
					localMode,
					tileLayer,
					listingId: listing.listingId,
					utils
				},
				{
					control: 'label',
					name: 'checkseparator',
					style: {
						clear: 'both',
						width: '100%',
					},
				},
				{
					control: 'label',
					name: 'final',
					style: { height: 50, clear: 'both' },
				},
			],
			beforeButton: el('div', {
				className: 'btn btn-white',
				style: { float: 'left', cursor: 'pointer', height: 40, lineHeight: '40px', width: 100, textAlign: 'center' },
				/* eslint-disable-next-line */
				onClick: () => { location.href = '/listings'; }
			}, 'Annulla'),
			sendButton: {
				text: 'Salva e prosegui',
				style: { width: 'auto', float: 'right' }
			},
			sendForm: (o) => {
				/* eslint-disable */
				const promise = new Promise((resolve, reject) => {
					this.props.actions.fetchSaveListing({ jwt: this.props.jwt, dataObject: o })
						.then((x) => {
							if (x.value.response.succeed) {
								resolve({ succeed: true, message: x.value.response.message });

								if (x.value.response.data.inserted) {
									this.props.history.push(`/listing/${x.value.response.data.inserted}/step-1`);
								}

								setTimeout(() => {
									if (x.value.response.data.inserted) {
										window.location.href = `/listing/${x.value.response.data.inserted}/step-2`;
									} else {
										window.location.href = this.props.pathname.replace('step-1', 'step-2');
									}
								}, 1000);
							} else {
								reject({ succeed: false, message: x.value.response.message });
							}
						});
					});
					return promise;
					/* eslint-enable */
			},
			formClassName: 'project-edit'
		});
	}
}

Edit.propTypes = {
	actions: PropTypes.instanceOf(Object).isRequired,
	item: PropTypes.instanceOf(Object),
};

Edit.defaultProps = {
	item: null,
};

export default Edit;
