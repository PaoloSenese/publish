// @flow
import React from 'react';
import PropTypes from 'prop-types';

const Contacts = (props: Object) => {
	const { title, text, phone, email, hours } = props;
	const { emailText, emailAddress } = email;

	return (
		<div className="project-steps-info">
			<div className="project-steps-info-title noselect">{title}</div>
			<div className="project-steps-info-content">
				<div className="project-steps-info-content-text noselect">
					<div>{text}</div>
					<div>{hours}</div>
				</div>
				<div className="project-steps-info-content-contacts">
					<div>
						{phone}
					</div>
					<div className="noselect">
						<a style={{ textDecoration: 'underline', color: '#333f48' }} href={`mailto:${emailAddress}`}>{emailText}</a>
					</div>
				</div>
			</div>
		</div>
	);
};

Contacts.propTypes = {
	title: PropTypes.string,
	text: PropTypes.string,
	phone: PropTypes.string,
	email: PropTypes.instanceOf(Object),
	hours: PropTypes.string
};

Contacts.defaultProps = {
	title: '',
	text: '',
	phone: '',
	email: '',
	hours: ''
};

export default Contacts;
