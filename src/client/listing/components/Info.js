// @flow
import React from 'react';
import PropTypes from 'prop-types';
import ClickOutHandler from 'react-onclickout';

const el = React.createElement;

class Info extends React.Component<any, any> {

	static defaultProps = {
		text: ''
	};

	state = {
		openInfo: false
	}

	openInfo(val: boolean) {
		this.setState({
			openInfo: val,
		});
	}

	render() {
		const { text } = this.props;
		const { openInfo } = this.state;

		return el('div', {},
			el('div', { style: { float: 'right', right: 5, position: 'absolute', top: 0 } },
				el('img', {
					src: '/admin/assets/img/icons/outline-info-24px.svg',
					style: { width: 23, cursor: 'pointer' },
					onClick: () => { this.openInfo(true); },
					alt: '' })),
			openInfo ?
				el(ClickOutHandler, { onClickOut: () => { this.openInfo(false); } },
					el('div', { className: 'profiled-overlay-container' },
						el('div', { className: 'profiled-overlay box-shadow noselect', style: { background: '#fff', marginTop: 6 } },
							el('div', { style: { fontWeight: 300, fontSize: 12, textAlign: 'justify', lineHeight: '16px' } }, text))))
				:
				null);
	}
}

Info.propTypes = {
	text: PropTypes.string,
};

Info.defaultProps = {
	text: '',
};

export default Info;
