// @flow
import React from 'react';
import PropTypes from 'prop-types';
import sjwt from 'jsonwebtoken';
import Form from 'client-form';
import Alert from '../../components/common/CustomControls/Alert';
// flow-disable-next-line
import './PhoneValidator.scss';

const el = React.createElement;

class PhoneValidator extends React.Component<any, any> {

	static defaultProps = {
		viewport: { width: '', height: '' },
		prefix: '',
		phoneNumber: ''
	};

	constructor(props: Object) {
		super(props);
		const { jwt, prefix, phoneNumber } = props;
		const { user } = sjwt.decode(jwt);
		user.isPhoneVerified = true;
		const { isPhoneVerified } = user;

		this.state = {
			openModal: false,
			isPhoneVerified,
			editNumber: false,
			prefix,
			phoneNumber
		};
	}

	shouldComponentUpdate(nextProps: Object, nextState: Object) {
		if (nextState.openModal !== this.state.openModal) return true;
		if (nextState.isPhoneVerified !== this.state.isPhoneVerified) return true;
		if (nextState.editNumber !== this.state.editNumber) return true;
		if (nextState.prefix !== this.state.prefix) return true;
		if (nextState.phoneNumber !== this.state.phoneNumber) return true;
		return false;
	}

	openModal() {
		const { jwt, actions } = this.props;
		actions.sendSms({
			jwt,
		}).then((x) => {
			console.log(x);
			this.setState({
				openModal: true
			});
		});
	}

	closeModal() {
		this.setState({
			openModal: false
		});
	}

	editNumber() {
		this.setState({
			editNumber: true,
			isPhoneVerified: false
		});
	}

	render() {
		const { viewport, jwt, actions } = this.props;
		const { openModal, isPhoneVerified, editNumber, prefix, phoneNumber } = this.state;

		return el('div', { className: 'phone-validator-container' },
			isPhoneVerified === true && editNumber === false ?
				el('div', { className: 'phone-validator-valid' },
					el('label', {}, 'Numero'),
					el('div', { className: 'phone-validator-valid-field' }, `${prefix}${phoneNumber}`),
					el('div', { style: { height: 24, width: 24, position: 'absolute', top: 30, right: 18 } },
						el('svg', { width: 24, height: 24, viewBox: '0 0 31 31' },
							el('circle', { cx: 14, cy: 14, r: 14, fill: '#00c694' }),
							el('polyline', { fill: 'none', points: '6,14 11,20 23,8', style: { fill: 'none', stroke: '#fff', strokeWidth: 2 } })
						)
					)
				)
				:
				el(React.Fragment, {},
					el('div', { className: 'phone-validator-field' },
						el('label', {}, 'Prefisso'),
						el('input', { type: 'text', name: 'prefix', value: prefix, onChange: (e) => { this.setState({ prefix: e.target.value }); } }),
						el('div', { className: 'phone-validator-error' }, ''),
					),
					el('div', { className: 'phone-validator-field' },
						el('label', {}, 'Numero'),
						el('input', { type: 'text', name: 'phoneNumber', value: phoneNumber, onChange: (e) => { this.setState({ phoneNumber: e.target.value }); } }),
						el('div', { className: 'phone-validator-error' }, ''),
					),
				),
			el('button', {
				className: 'btn',
				onClick: () => {
					if (isPhoneVerified === false) {
						this.openModal();
					}
					if (isPhoneVerified === true) {
						this.editNumber();
					}
				} }, isPhoneVerified === true ? 'Modifica' : 'Verifica'),
			openModal ?
				el(Alert, {
					closeActionOverlay: () => { this.closeModal(); },
					resetAction: null,
					viewport,
					actionTitle: 'Verifica numero',
					handleAction: null,
					deleteError: null,
					actionTextRed: '',
					actionTextBlack: el(Form, {
						key: Math.random(),
						controls: [
							{
								control: 'label',
								content: el('div', {}, 'Ti abbiamo inviato un SMS con il codice di verifica.',
									el('br', {}), el('span', {}, 'Inseriscilo nel campo qui sotto')),
								style: { textAlign: 'center', marginBottom: 20 }
							},
							{
								control: 'text',
								name: 'verificationCode',
								label: { text: 'Codice di verifica' },
								placeholder: 'Inserisci qui il codice'
							}
						],
						sendButton: {
							text: 'Verifica',
						},
						sendForm: (o) => {
							/* eslint-disable */
							var promise = new Promise((resolve, reject) => {
								actions.verifyCode({
									jwt, o
								}).then((x) => {
									setTimeout(() => {
										if (x.value.response.succeed) {
											resolve({ succeed: true, message: 'Codice valido' });
										} else {
											reject({ succeed: false, message: 'Errore' });
										}
									}, 1000);
								});
							});
							return promise;
							/* eslint-enable */
						},
						formClassName: 'phone-validator-form'
					}),
					icon: '/admin/assets/img/icons/mobile-alt-solid.svg',
					alertHeight: 430,
					hideActions: true,
					textBlackStyle: { marginTop: 55 }
				}, '')
				: null);
	}
}

PhoneValidator.propTypes = {
	jwt: PropTypes.string.isRequired,
	actions: PropTypes.instanceOf(Object).isRequired,
	viewport: PropTypes.instanceOf(Object),
	prefix: PropTypes.string,
	phoneNumber: PropTypes.string
};

PhoneValidator.defaultProps = {
	viewport: { width: '', height: '' },
	prefix: '',
	phoneNumber: ''
};

export default PhoneValidator;
