// @flow
import React from 'react';
import Alert from '../../components/common/CustomControls/Alert';
import VacanciesPop from './VacanciesPop';
import { vacancies } from '../../../common/helpers/var';
// flow-disable-next-line
import './Vacancies.scss';

const el = React.createElement;

class Vacancies extends React.Component<any, any> {

	state = {
		value: [],
		formKey: Math.random(),
		openModal: false,
		selectedItem: null
	};

	componentDidMount() {
		const { actions, jwt } = this.props;

		actions.getVacancies({ jwt })
			.then((x) => {
				this.setState({
					value: x.value.response.dataObject
				});
			});
	}

	deleteRow(index: number) {
		const { actions, jwt } = this.props;

		actions.deleteVacancies({ jwt, index })
			.then((x) => {
				this.setState({
					value: x.value.response.dataObject,
					formKey: Math.random()
				});
			});
	}

	closeOverlay() {
		// flow-disable-next-line
		document.body.style.overflow = 'unset';
		this.setState({
			openModal: false,
			selectedItem: null
		});
	}

	editItem(item: Object) {
		// flow-disable-next-line
		document.body.style.overflow = 'hidden';
		this.setState({
			openModal: true,
			selectedItem: item
		});
	}

	render() {
		const { value, formKey, openModal, selectedItem } = this.state;
		const { actions, jwt, viewport } = this.props;

		return el('div', { className: 'vacancies' },
			el('div', { style: { width: '100%', clear: 'both', fontSize: 14, fontWeight: 700, lineHeight: '40px' } }, 'Tabella prezzi disponibilit\xE0'),
			el('div', { className: 'vacancies-head' },
				el('div', { className: 'vacancies-cell' }, 'Inizio'),
				el('div', { className: 'vacancies-cell' }, 'Fine'),
				el('div', { className: 'vacancies-cell-small' }, 'Prezzo'),
				el('div', { className: 'vacancies-cell' }, 'Disponibile'),
				el('div', { className: 'vacancies-cell' }, 'Frequenza'),
				el('div', { className: 'vacancies-cell' }, 'Note')
			),
			value.map((item, i) => el('div', { key: `vac_${i}`, className: 'vacancies-row' },
				el('div', { className: 'vacancies-cell' }, item.startDt),
				el('div', { className: 'vacancies-cell' }, item.endDt),
				el('div', { className: 'vacancies-cell-small' }, item.price),
				el('div', { className: 'vacancies-cell' }, item.disponible === true ? 'Sì' : 'No'),
				el('div', { className: 'vacancies-cell' }, vacancies.filter(o => o.value === item.frequency)[0].label),
				el('div', { className: 'vacancies-cell' }, item.notes),
				el('div', { className: 'vacancies-bin', onClick: () => { this.deleteRow(i); } },
					el('div', { className: 'delete-button', style: i > 0 ? { marginTop: 0 } : {} },
						el('svg', { width: '14', height: '17', viewBox: '0 0 14 17' },
							el('g', { fill: '#949da2', fillRule: 'evenodd' },
								el('path', { d: 'M11.515 15.447a.515.515 0 0 1-.496.48H3.222a.559.559 0 0 1-.527-.5L2.045 4.67H1.024l.653 10.82c.049.805.744 1.463 1.546 1.463h7.797c.802 0 1.484-.659 1.516-1.464l.424-10.819h-1.021l-.423 10.778zM4.86 1.466c0-.239.2-.44.438-.44h3.344c.238 0 .438.201.438.44v1.016H4.86V1.466zm8.57 1.016H10.1V1.466C10.1.66 9.445 0 8.643 0H5.298C4.496 0 3.84.66 3.84 1.466v1.016H.51c-.282 0-.51.23-.51.513 0 .284.228.513.51.513h12.92c.282 0 .51-.23.51-.513a.512.512 0 0 0-.51-.513z' })
							)
						)
					)
				),
				el('div', { className: 'vacancies-bin', onClick: () => { this.editItem(item); } },
					el('div', { className: 'delete-button', style: i > 0 ? { marginTop: 0 } : {} },
						el('svg', { width: 16, height: 16, viewBox: '0 0 16 16' },
							el('path', { fill: '#949da2', fillRule: 'evenodd', d: 'M1.492 12.394l8.296-8.316 2.129 2.131-8.306 8.3H1.492v-2.115zM12.061 1.798L14.2 3.934l-1.224 1.222-2.134-2.13 1.219-1.228zM.746 16h3.173c.2 0 .388-.08.527-.219l11.335-11.32a.744.744 0 0 0 0-1.053L12.588.218a.746.746 0 0 0-1.054 0L.219 11.56a.762.762 0 0 0-.219.527v3.169c0 .412.333.745.746.745z' })
						)
					)
				)
			)),
			el('div', { className: 'vacancies-add' },
				el('button', {
					className: 'btn btn-white',
					onClick: () => {
						// flow-disable-next-line
						document.body.style.overflow = 'hidden';
						this.setState({ openModal: true });
					}
				}, 'Aggiungi'),
				openModal === true ?
					el(Alert, {
						closeActionOverlay: () => this.closeOverlay(),
						resetAction: () => this.closeOverlay(),
						viewport,
						actionTitle: 'Prezzi e disponibilità',
						handleAction: null,
						deleteError: null,
						actionTextBlack: el(VacanciesPop, { formKey, item: selectedItem !== undefined && selectedItem !== null ? selectedItem : {}, actions, jwt }, ''),
						icon: '/admin/assets/img/icons/baseline-calendar_today-24px.svg',
						alertHeight: 430,
						hideActions: true,
					}, '')
					: null));
	}
}

export default Vacancies;
