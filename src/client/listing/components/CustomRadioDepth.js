// @flow
import React from 'react';
import PropTypes from 'prop-types';

const CustomRadioDepth = (props: Object) => {
	const { title, img, list, backgroundColor, height } = props;

	return (
		<div className="custom-radio-options-wrapper box-shadow" style={{ marginTop: 76 - height }}>
			<div className="title" style={{ background: backgroundColor, minHeight: height }}>
				<div style={{ position: 'absolute', width: 'calc(100% - 30px)', bottom: 17 }}>{title}</div>
			</div>
			<div className="custom-radio-options-depth">
				{ img ?
					<div className="cont-left">
						<img src={img} alt="" />
					</div> : null }
				{ list.length > 0 ?
					<div className="cont-right">
						<ul>
							{ list.map((item, i) => (
								<li key={`crod_${i}`}>
									<div style={{ width: '100%', height: '100%', display: 'table' }}>
										<div className="cont-right-bullet">
											<svg width="12" viewBox="0 0 8 6">
												<path style={{ fill: '#21bbda' }} fillRule="evenodd" d="M5.797.229L2.97 3.669 1.416 2.007a.698.698 0 0 0-1.034 0 .822.822 0 0 0 0 1.11l2.07 2.218c.143.152.33.229.518.229a.713.713 0 0 0 .518-.23l3.345-3.995a.822.822 0 0 0 0-1.109.7.7 0 0 0-1.036 0z" />
											</svg>
										</div>
										<div className="cont-right-text">{item.text}</div>
									</div>
								</li>))}
						</ul>
					</div>
					: null }
			</div>
		</div>
	);
};

CustomRadioDepth.propTypes = {
	title: PropTypes.string,
	img: PropTypes.string,
	list: PropTypes.arrayOf(Object),
	subtitle: PropTypes.string,
	type: PropTypes.number,
};

CustomRadioDepth.defaultProps = {
	title: '',
	img: '',
	list: [],
	subtitle: '',
	type: null,
};

export default CustomRadioDepth;
