// @flow
import React from 'react';
import PropTypes from 'prop-types';

// flow-disable-next-line
import './Tag.scss';

const Tag = (props: Object) => {
	const { onClose, type, value, defaultVal, displayValue, unit, name } = props;

	if ((type === 'text' && value !== defaultVal) || (type === 'range' && (value.min !== defaultVal.min || value.max !== defaultVal.max))) {
		return (
			<div className="tag">
				{ type === 'text' ?
					<div>{displayValue !== null ? displayValue : value}</div>
					:
					<div>
						{
							value.min !== '' && value.max === '' ? `da ${displayValue.min} ${unit}`
								: value.min !== '' && value.max !== '' ? `${displayValue.min} - ${displayValue.max} ${unit}`
									: value.min === '' && value.max !== '' ? `fino a ${displayValue.max} ${unit}`
										: null
						}
					</div>
				}
				<div {...{
					className: 'tag-close',
					onClick: () => { onClose(name, defaultVal); }
				}}>
					&nbsp;
				</div>
			</div>
		);
	}
	return null;
};

Tag.propTypes = {
	onClose: PropTypes.func,
	type: PropTypes.string,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.bool,
		PropTypes.instanceOf(Object)
	]).isRequired,
	defaultVal: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.bool,
		PropTypes.instanceOf(Object)
	]),
	displayValue: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.bool,
		PropTypes.instanceOf(Object)
	]),
	unit: PropTypes.string,
	name: PropTypes.string
};

Tag.defaultProps = {
	onClose: null,
	type: '',
	defaultVal: null,
	displayValue: null,
	unit: '',
	name: ''
};

export default Tag;
