// @flow
import React from 'react';

const el = React.createElement;

const PricingCustomObject = (props: Object) => {
	const { discount, period, price, originalPrice } = props;

	return el('div', { className: 'pricing-container' },
		el('div', { className: 'discount' }, discount),
		el('div', { className: 'pricing-container-duration' }, period),
		el('div', { className: 'pricing-container-price' }, `${price} € `, el('span', {}, `${originalPrice} €`)),
		el('div', {},
			el('svg', { width: 24, height: 24, viewBox: '0 0 24 24' },
				el('circle', { className: 'ext', cx: 12, cy: 12, r: 9, stroke: 'rgb(216, 216, 223)', strokeWidth: 2 }),
				el('circle', { className: 'int', cx: 12, cy: 12, r: 4 })
			)));
};

export default PricingCustomObject;
