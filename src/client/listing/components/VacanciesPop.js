// @flow
import React from 'react';
import Form from 'client-form';
import DatePickerField from '../../components/common/CustomControls/DatePickerField';

const el = React.createElement;

class VacanciesPop extends React.Component<any, any> {

	deleteRow(index: number) {
		const { actions, jwt } = this.props;

		actions.deleteVacancies({ jwt, index })
			.then((x) => {
				console.log(x);
				/* do something?? */
			});
	}

	render() {
		const { formKey, item, actions, jwt } = this.props;

		return el(Form, {
			key: formKey,
			controls: [
				{
					control: 'text',
					name: 'id',
					type: 'hidden',
					value: item.id,
					style: { display: 'none', }
				},
				{
					control: 'external',
					component: DatePickerField,
					name: 'startDt',
					key: 'startDt',
					label: { text: 'Inizio' },
					placeholder: 'Inizio',
					value: item.startDt ? item.startDt : '',
					updateOnChange: true,
					isValid: true,
					isRequired: true,
					className: 'custom-datepicker field-container small-date',
					errorMessage: 'Campo obbligatorio'
				},
				{
					control: 'external',
					component: DatePickerField,
					name: 'endDt',
					key: 'endDt',
					label: { text: 'Fine' },
					placeholder: 'Fine',
					value: item.endDt ? item.endDt : '',
					updateOnChange: true,
					isValid: true,
					isRequired: true,
					className: 'custom-datepicker field-container small-date',
					errorMessage: 'Campo obbligatorio'
				},
				{
					control: 'text',
					name: 'price',
					onlyNumber: true,
					isRequired: true,
					label: { text: 'Prezzo' },
					placeholder: 'Prezzo',
					className: 'small-price',
					value: item.price,
					errorMessage: 'Campo obbligatorio'
				},
				{
					control: 'check',
					name: 'disponible',
					label: { text: 'Disponibile' },
					placeholder: 'Disponibile',
					value: item.disponible,
					className: 'small-check'
				},
				{
					control: 'select',
					name: 'frequency',
					label: { text: 'Frequenza' },
					options: [
						{ value: 1, label: 'Giornaliero' },
						{ value: 7, label: 'Settimanale' },
						{ value: 30, label: 'Mensile' }
					],
					className: 'small-select',
					value: item.frequency
				},
				{
					control: 'textArea',
					name: 'notes',
					label: { text: 'Note' },
					placeholder: 'Scrivi qui le tue note',
					className: 'small-notes',
					value: item.notes ? item.notes : '',
				}
			],
			sendButton: {
				text: 'Salva',
			},
			sendForm: (o) => {
				/* eslint-disable */
				const promise = new Promise((resolve, reject) => {
					actions.saveVacancies({ jwt: jwt, dataObject: o })
						.then(x => {
							setTimeout(() => {
								if (x.value.response.succeed) {
									resolve({ succeed: true, message: 'Dati salvati correttamente' });
								} else {
									reject({ succeed: false, message: 'Si è verificato un problema' });
								}
							}, 1000);
						});
				});
				return promise;
				/* eslint-enable */
			},
			formStyle: { margin: 0 },
			formClassName: 'vacancies-pop'
		});
	}
}

export default VacanciesPop;
