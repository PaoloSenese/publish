// @flow
import { createElement } from 'react';
import imported from 'react-imported-component';

const Edit = imported(() => require('./Edit'));
const Media = imported(() => require('./Media'));
const Depth = imported(() => require('./Depth'));
const Details = imported(() => require('./Details'));
const Contacts = imported(() => require('./Contacts'));
const ThankYou = imported(() => require('../thankyou/ThankYou'));

const el = createElement;

const SwitchComponents = (props: Object) => {
	const {
		device, dataObject, actions, viewport, currentStep, stepsObject, pathname, jwt, imageserver,
		history, mapTexts, editListingTexts, mediaTexts, radioContainers, citiesArray, localMode, tileLayer,
		utils, depthsObject, productPricings
	} = props;

	switch (currentStep) {
		case 'step-2':
			return el(Details, { item: dataObject, actions, jwt, pathname, device, viewport, mapTexts, editListingTexts, history });
		case 'step-3':
			return el(Media, { title: stepsObject[1].title, item: dataObject, actions, viewport, jwt, imageserver, mediaTexts, editListingTexts });
		case 'step-4':
			return el(Contacts, { viewport, actions, jwt, pathname });
		case 'step-5':
			return el(Depth, { item: dataObject, actions, viewport, jwt, mediaTexts, radioContainers, utils, depthsObject, productPricings });
		case 'step-1':
			return el(Edit, { async: true, item: dataObject, actions, jwt, pathname, device, viewport, mapTexts, editListingTexts, history, citiesArray, localMode, tileLayer, utils });
		default:
			return el(ThankYou, { async: true, item: dataObject, actions, jwt, pathname, device, viewport, mapTexts, editListingTexts, history });
	}
};

export default SwitchComponents;
