// @flow
import React from 'react';
import { SortableElement } from 'react-sortable-hoc';

class MediaItem extends React.Component<any, any> {

	constructor(props) {
		super(props);

		this.state = {
			item: props.item,
			imageUrl: `${props.imageserver}150x90/${props.item.url}`
		};
	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			item: nextProps.item,
			imageUrl: `${nextProps.imageserver}150x90/${nextProps.item.url}`
		});
	}

	rotateItem(e) {
		this.setState({
			isLoading: true
		});

		this.props.actions.rotateMedia({
			jwt: this.props.jwt,
			url: `${this.props.imageserver}raw-rotate/${e.url}`,
			id: this.props.id,
			imageType: this.props.imageType,
			prefix: this.props.prefix,
			imgObject: e
		}).then((x) => {

			this.setState({
				item: x.value.response.data,
				isLoading: false,
				imageUrl: `${this.props.imageserver}212x127/${x.value.response.data.url}`
			});

		});
	}

	render() {
		const {
			isDragging, sortStart, itemIndex, deleteItem
		} = this.props;
		const { item, imageUrl, isLoading } = this.state;
		const { imageType } = item;
		const opacity = isDragging ? 0 : 1;
		const styleItem = { opacity };

		return (
			<div style={isLoading ? {} : styleItem} className={isLoading ? 'item loader' : 'item'}>
				<img src={imageUrl} alt="" />
				{ sortStart === false ?
					<div className="item-overlay" style={sortStart === true ? { display: 'none' } : {}}>
						<div {...{
							onClick: () => { this.rotateItem(item); },
							className: 'box-shadow'
						}}>
							<img src="/admin/assets/img/media/round-rotate_right-24px.svg" alt="" />
						</div>
						<div {...{
							onClick: () => { deleteItem(item, itemIndex); },
							style: { float: 'right' },
							className: 'box-shadow'
						}}>
							<img src="/admin/assets/img/media/round-clear-24px.svg" alt="" />
						</div>
					</div>
					:
					null
				}
				{ itemIndex === 0 && imageType === 'F' ?
					<div style={{
						position: 'absolute',
						background: '#323f48',
						color: '#fff',
						top: '71px',
						width: '100%',
						textAlign: 'center',
						fontSize: 11,
						lineHeight: '19px'
					}}>
						PRINCIPALE
					</div>
					: null }
			</div>
		);
	}
}

export default SortableElement(MediaItem);
