// @flow
import React from 'react';
import { SortableContainer } from 'react-sortable-hoc';
import Dropzone from 'react-dropzone';
import request from 'superagent';

import MediaItem from './MediaItem';

// flow-disable-next-line
import '../Media.scss';

class MediaContainer extends React.Component<any, any> {

	constructor(props) {
		super(props);

		this.state = {
			items: props.items,
			overDrop: false,
		};
	}

	editItem(e) {
		this.props.onChange('editItem', e);
	}

	onDropFile(acceptedFiles) {
		if (acceptedFiles.length === 0) {
			return;
		}

		if (this.props.imgLimit - this.state.items.length === 0) {
			this.setState({
				message: 'Limite caricamento raggiunto',
			});
		} else {
			const { overDrop } = this.state.overDrop;

			this.setState({
				overDrop,
				isLoading: true,
			});

			acceptedFiles.forEach((file) => {
				try {
					const x = new Image();
					let filedata = {};
					x.onload = () => {
						const reader = new FileReader();
						reader.readAsDataURL(file);
						reader.onload = () => {
							filedata = {
								width: x.width,
								height: x.height,
								size: file.size,
								id: this.props.id,
								jwt: this.props.jwt,
								prefix: this.props.prefix,
								imageType: this.props.imageType
							};

							request.post(`/admin/api/v1/listing/uploadmedia?data=${JSON.stringify(filedata)}`)
								.attach('uploadMedia', file)
								.end((err, res) => {
									const response = JSON.parse(res.text);
									if (!response.response.succeed) {
										this.setState({
											isLoading: false,
											message: response.response.errmsg
										});
									} else {
										try {
											const { items } = this.state;
											items.push(response.response.data);

											this.setState({
												overDrop: false,
												isLoading: false,
												items,
												message: ''
											});
										} catch (e) {
											this.setState({
												isLoading: false,
												message: response.response.errmsg
											});
										}
									}
								});
						};
					};
					x.src = file.preview;
				} catch (e) {
					console.log(e);
				}
			});
		}
	}

	onDragEnter = (e) => {
		const { types } = e.dataTransfer;
		const hasFilesInEvent =	types && types.length === 1 && types[0] === 'Files';
		if (!hasFilesInEvent) {
			// eslint-disable-next-line
			return;
		}
	};

	onDragOver = (e) => {
		const { types } = e.dataTransfer;
		const hasFilesInEvent =	types && types.length === 1 && types[0] === 'Files';
		if (!hasFilesInEvent) {
			// eslint-disable-next-line
			return;
		}
	};

	onOpenClick() {
		if (this.props.imgLimit - this.state.items.length === 0) {
			this.setState({
				message: 'Limite caricamento raggiunto',
			});
		} else {
			this.setState({
				message: '',
			});
			// flow-disable-next-line
			this.dropzone.open();
		}
	}

	deleteItem(item) {
		this.setState({
			isLoading: true,
			message: '',
		});

		this.props.actions.deleteMedia({
			listingId: item.listingId,
			imageId: item.imageId,
			jwt: this.props.jwt,
		}).then(() => {

			this.setState({
				isLoading: false,
			});

			this.props.getMedia({
				id: item.listingId,
				jwt: this.props.jwt,
			});

		});
	}

	render() {
		const {
			viewport, canDrop, isOver, label, sortStart, imageType, imageserver, prefix,
			jwt, actions, imgLimit, deleteAllMedia, id, items
		} = this.props;
		const isActive = canDrop && isOver;
		const style = {
			backgroundColor: isActive && canDrop && isOver ? '#dedede' : 'transparent',
			maxWidth: viewport.width !== '' ? viewport.width > 820 ? 690 : (parseFloat(viewport.width) - 20) : 'auto',
		};

		return (
			<div>
				<div style={{ width: '100%', float: 'left', marginTop: 15, marginBottom: 15 }}>
					<div style={{ float: 'left', fontWeight: 700 }}>
						<div className="label">
							{ label }
							&nbsp;
							<span style={{ fontSize: 14 }}>
								({items.length} inserite di	{imgLimit})
							</span>
						</div>
					</div>
					{ items.length >= 1 ?
						<div {...{
							onClick: () => { deleteAllMedia(imageType); },
							className: 'delete-all noselect'
						}}>
							<span>Elimina tutte</span>
						</div>
						: null }
				</div>
				<div className={this.state.isLoading ? 'item-container loader' : 'item-container'} style={style}>
					{ items.map((item, i) => (
						<MediaItem {...{
							key: `mediaI_${i}`,
							index: i,
							itemIndex: i,
							item,
							sortStart,
							imageserver,
							imageType,
							isPrincipal: item.isPrincipal,
							jwt,
							actions,
							deleteItem: this.deleteItem.bind(this),
							prefix,
							id
						}} />
					))
					}
					<div {...{
						style: imgLimit - items.length === 0 ? { opacity: '.5' } : {},
						className: 'item add-item noselect',
						title: `Aggiungi ${label}`,
						onClick: () => { this.onOpenClick(); }
					}}>
						<svg {...{
							width: 40,
							height: 40,
							className: 'noselect'
						}}>
							<circle {...{ cx: 20, cy: 20, r: 20, fill: '#e4002b' }} />
							<rect {...{ width: 15, height: 1.5, x: 12.5, y: 19, fill: '#fff' }} />
							<rect {...{ width: 1.5, height: 15, x: 19, y: 12.5, fill: '#fff' }} />
						</svg>
					</div>

					<div style={{ marginTop: 40, color: '#e4002b', display: 'inline-block' }}>
						{this.state.message}
					</div>
					<Dropzone {...{
						className: this.state.overDrop ? 'dropzone active-dropzone' : 'dropzone',
						activeClassName: 'active-dropzone',
						// flow-disable-next-line
						ref: (node) => { this.dropzone = node; },
						onDrop: this.onDropFile.bind(this),
						onDragEnter: this.onDragEnter.bind(this),
						onDragOver: this.onDragOver.bind(this),
						disableClick: true,
						accept: 'image/*',
					}} />
					{ this.state.isLoading ?
						<div>
							<img src="/admin/assets/img/default.svg" style={{ width: 80 }} alt="" />
						</div> : null }
				</div>
			</div>
		);
	}
}

export default SortableContainer(MediaContainer);
