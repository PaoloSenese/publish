// @flow
import React from 'react';
import Contacts from './components/Contacts';

const el = React.createElement;

const Steps = (props: Object) => {
	const { currentStep, baseUrl, stepsObject, isNew, contactsTexts } = props;

	return el('div', { className: 'project-steps', style: currentStep ? {} : { display: 'none' } },
		el('ul', {},
			stepsObject.map((item) => {
				const itemStep = `step-${item.step}`;
				const href = !isNew ? `${baseUrl}${itemStep}` : '#';
				const isCurrent = itemStep === currentStep;
				return el('a', { key: itemStep, href, style: isNew ? {} : { cursor: 'pointer' } },
					el('li', {},
						el('div', {},
							el('svg', { viewBox: '0 0 25 25', className: isCurrent ? 'step-sel' : 'step', width: 25, height: 25 },
								el('circle', { cx: 12.5, cy: 12.5, r: 12 }),
								el('text', { x: '50%', y: '50%', dy: '.10em' }, item.step))),
						el('div', { className: 'step-container noselect' },
							el('div', { className: 'step-container-title noselect' }, item.title),
							el('div', { className: 'step-container-desc noselect' }, item.text))));
			})),
		el(Contacts, contactsTexts));
};

export default Steps;
