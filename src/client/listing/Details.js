// @flow
import React from 'react';
import Form from 'client-form';
import Vacancies from './components/Vacancies';
import { energy, heatingtypes, conditions, furnished, airConditioned, floors, boxTypes, gardenTypes, parkingSpaces } from '../../common/helpers/var';
// flow-disable-next-line
import './Edit.scss';

const el = React.createElement;

class Details extends React.Component<any, any> {

	state = {
		listing: this.props.item,
	};

	render() {
		const { actions, jwt, viewport } = this.props;
		const { listing } = this.state;
		const energyClass = listing.energyEfficiencyRatingId >= 1 && listing.energyEfficiencyRatingId <= 9 ? 997 :
			listing.energyEfficiencyRatingId >= 12 && listing.energyEfficiencyRatingId <= 21 ? 998 :
				listing.energyEfficiencyRatingId;

		return el(Form, {
			controls: [
				{
					control: 'label',
					content: el('div', { className: 'step-title' },
						el('div', {}, 'Dettagli'),
						el('div', {}, 'Inserisci i dettagli del tuo immobile')),
					style: { width: '100%', clear: 'both', marginBottom: 30 }
				},
				{
					control: 'text',
					type: 'hidden',
					name: 'listingId',
					value:	listing.listingId,
					style: { display: 'none' }
				},
				{
					control: 'text',
					type: 'text',
					name: 'price',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: listing.categoryTypeId === 2 ? 'Prezzo settimanale bassa stagione' : 'Prezzo',
					},
					value: listing.price ? listing.price : '',
					currency: true,
					isRequired: true,
					errorMessage: 'Campo obbligatorio',
					unit: '€',
				},
				{
					control: 'text',
					type: 'text',
					name: 'priceMax',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Prezzo settimanale alta stagione',
					},
					value: listing.priceMax ? listing.priceMax : '',
					currency: true,
					isRequired: true,
					errorMessage: 'Campo obbligatorio',
					unit: '€',
					hide: listing.categoryTypeId !== 2
				},
				{
					control: 'check',
					name: 'hidePrice',
					label: { text: 'Nascondi prezzo (trattativa riservata)' },
					value: listing.hidePrice,
					className: 'container-field-right check',
				},
				{
					control: 'external',
					component: Vacancies,
					name: 'Vacancies',
					key: 'Vacancies',
					value: listing.Vacancies,
					exclude: true,
					actions,
					viewport
				},
				{
					control: 'text',
					type: 'text',
					name: 'bodyCorporateFees',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Spese condominiali mensili',
					},
					value: listing.bodyCorporateFees ? listing.bodyCorporateFees : '',
					currency: true,
					unit: '€'
				},
				{
					control: 'text',
					type: 'text',
					name: 'surface',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Mq',
					},
					value: listing.surface ? listing.surface : '',
					isRequired: true,
					errorMessage: 'Campo obbligatorio',
					className: 'container-field-right small-field',
					unit: 'm²'
				},
				{
					control: 'text',
					type: 'text',
					name: 'yearOfBuilding',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Anno di costruzione',
					},
					value: listing.yearOfBuilding ? listing.yearOfBuilding : '',
					className: 'small-field',
				},
				{
					control: 'text',
					type: 'text',
					name: 'buildingUnit',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Unità immobiliare',
					},
					value: listing.buildingUnit ? listing.buildingUnit : '',
					className: 'container-field-right small-field',
				},
				{
					control: 'select',
					name: 'conditionId',
					label: { text: 'Condizioni' },
					options: conditions,
					default: -1,
					value:	listing.conditionId ? listing.conditionId : -1,
				},
				{
					control: 'label',
					content: 'Caratteristiche interne',
					className: 'step-separator noselect'
				},
				{
					control: 'select',
					name: 'heatingTypeId',
					label: { text: 'Riscaldamento' },
					options: heatingtypes,
					default: -1,
					value:	listing.heatingTypeId,
				},
				{
					control: 'select',
					name: 'furnishedId',
					label: { text: 'Arredato' },
					options: furnished,
					default: 0,
					value:	listing.furnishedId ? listing.furnishedId : 0,
					className: 'container-field-right'
				},
				{
					control: 'select',
					name: 'airConditionedId',
					label: { text: 'Aria condizionata' },
					options: airConditioned,
					default: 0,
					value:	listing.airConditionedId ? listing.airConditionedId : 0,
				},
				{
					control: 'plusMinus',
					type: 'text',
					name: 'roomNum',
					label: {
						text: 'N. Locali',
					},
					value: listing.roomNum ? listing.roomNum : 0,
					className: 'all-width',
					style: { maxWidth: 'unset' }
				},
				{
					control: 'plusMinus',
					type: 'text',
					name: 'bathNum',
					label: {
						text: 'N. Bagni',
					},
					value: listing.bathNum ? listing.bathNum : 0,
					className: 'all-width',
					style: { maxWidth: 'unset' }
				},
				{
					control: 'label',
					content: 'Caratteristiche esterne',
					className: 'step-separator noselect'
				},
				{
					control: 'select',
					name: 'floorNum',
					label: { text: 'Piano' },
					options: floors,
					default: -2,
					value:	listing.floorNum ? listing.floorNum : -2,
				},
				{
					control: 'plusMinus',
					type: 'text',
					name: 'levelNum',
					label: {
						text: 'Totale piani',
					},
					value: listing.levelNum ? listing.levelNum : 0,
					className: 'container-field-right',
					style: { maxWidth: 290 }
				},
				{
					control: 'select',
					name: 'boxTypeId',
					label: { text: 'Box' },
					options: boxTypes,
					default: -1,
					value: listing.boxTypeId ? listing.boxTypeId : -1,
					hideRadio: true,
					// style: { width: 'auto' },
					// className: 'custom-radio-container resize-mobile',
					className: 'small-field'
				},
				{
					control: 'text',
					type: 'text',
					name: 'mqBox',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Mq box',
					},
					value: listing.mqBox ? listing.mqBox : '',
					className: 'container-field-right small-field',
					unit: 'm²',
					hide: listing.boxTypeId === -1 || listing.boxTypeId === 0,
					hideIf: [
						{ field: 'boxTypeId', regEx: /^(-1|0)*$/ }
					]
				},
				{ control: 'label', style: { clear: 'both', width: '100%' } },
				{
					control: 'select',
					name: 'gardenTypeId',
					label: { text: 'Giardino' },
					options: gardenTypes,
					default: -1,
					value:	listing.gardenTypeId ? listing.gardenTypeId : -1,
					hideRadio: true,
					// className: 'custom-radio-container resize-mobile',
					// style: { minHeight: 73 },
					className: 'small-field'
				},
				{
					control: 'text',
					type: 'text',
					name: 'mqGarden',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Mq giardino',
					},
					value: listing.mqGarden ? listing.mqGarden : '',
					className: 'container-field-right small-field',
					unit: 'm²',
					hide: listing.gardenTypeId === -1 || listing.gardenTypeId === 0,
					hideIf: [
						{ field: 'gardenTypeId', regEx: /^(-1|0)*$/ }
					]
				},
				{ control: 'label', style: { clear: 'both', width: '100%' } },
				{
					control: 'select',
					name: 'parkingSpaceId',
					label: { text: 'Posti auto' },
					options: parkingSpaces,
					default: -1,
					value: listing.parkingSpaceId ? listing.parkingSpaceId : -1,
					hideRadio: true,
					// style: { width: 'auto' },
					// className: 'custom-radio-container resize-mobile',
					className: 'small-field'
				},
				{
					control: 'text',
					type: 'text',
					name: 'parkingNumber',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'N° posti auto',
					},
					value: listing.parkingNumber ? listing.parkingNumber : '',
					className: 'container-field-right small-field',
					hide: listing.parkingSpaceId === -1 || listing.parkingSpaceId === 0,
					hideIf: [
						{ field: 'parkingSpaceId', regEx: /^(-1|0)*$/ }
					]
				},
				{ control: 'label', style: { clear: 'both', width: '100%' } },
				{
					control: 'radio',
					name: 'hasBalcony',
					label: { text: 'Balcone' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value:	listing.hasBalcony ? listing.hasBalcony : '',
					hideRadio: true,
					className: 'custom-radio-container small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'text',
					type: 'text',
					name: 'mqBalcony',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Mq balcone',
					},
					value: listing.mqBalcony ? listing.mqBalcony : '',
					className: 'container-field-right small-field',
					unit: 'm²',
					hide: listing.hasBalcony !== true,
					hideIf: [
						{ field: 'hasBalcony', regEx: /^((?!true).)*$/, },
					]
				},
				{
					control: 'radio',
					name: 'hasTerrace',
					label: { text: 'Terrazzo' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value:	listing.hasTerrace ? listing.hasTerrace : '',
					hideRadio: true,
					className: 'custom-radio-container small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'text',
					type: 'text',
					name: 'mqTerrace',
					onlyNumber: true,
					placeholder: '',
					label: {
						text: 'Mq terrazzo',
					},
					value: listing.hasTerrace ? listing.hasTerrace : '',
					className: 'container-field-right small-field',
					unit: 'm²',
					hide: listing.hasTerrace !== true,
					hideIf: [
						{ field: 'hasTerrace', regEx: /^((?!true).)*$/, },
					]
				},
				{
					control: 'radio',
					name: 'hasElevator',
					label: { text: 'Ascensore' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value:	listing.hasElevator ? listing.hasElevator : '',
					hideRadio: true,
					className: 'custom-radio-container small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'radio',
					name: 'hasSwimmingPool',
					label: { text: 'Piscina' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value: listing.hasSwimmingPool ? listing.hasSwimmingPool : '',
					hideRadio: true,
					className: 'custom-radio-container container-field-right small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'radio',
					name: 'hasConcierge',
					label: { text: 'Portineria' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value: listing.hasConcierge ? listing.hasConcierge : '',
					hideRadio: true,
					className: 'custom-radio-container small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'radio',
					name: 'hasCellar',
					label: { text: 'Cantina' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value: listing.hasCellar ? listing.hasCellar : '',
					hideRadio: true,
					className: 'custom-radio-container container-field-right small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'radio',
					name: 'hasAttic',
					label: { text: 'Soffitta' },
					options: [
						{ value: '', label: 'n.i.', className: 'first' },
						{ value: true, label: 'Sì', className: 'central' },
						{ value: false, label: 'No', className: 'last' },
					],
					default: '',
					value: listing.hasAttic ? listing.hasAttic : '',
					hideRadio: true,
					className: 'custom-radio-container small-field',
					style: { minHeight: 73 },
				},
				{
					control: 'label',
					style: { height: 0, clear: 'both', width: '100%', },
				},
				{
					control: 'label',
					content: 'Classificazione energetica',
					className: 'step-separator noselect'
				},
				{
					control: 'select',
					name: 'energyClass',
					label: { text: 'Classe energetica' },
					options: energy.filter(o => [997, 998, 0, 9, 10, 11].indexOf(o.value) !== -1).map(item => Object.assign({}, item, { style: { width: 'auto', float: 'left', marginRight: 15 } })),
					value: energyClass,
					default: 0,
					hideRadio: false,
				},
				{
					control: 'label',
					style: { height: 0, clear: 'both', width: '100%', },
				},
				{
					control: 'radio',
					name: 'energyEfficiencyRatingId',
					label: { text: '', style: { display: 'none' } },
					options: energy.filter(o => o.type.indexOf(parseFloat(energyClass)) !== -1),
					value: `${listing.energyEfficiencyRatingId}`,
					default: 0,
					hideRadio: true,
					className: 'custom-energy resize-mobile all-width',
					hide: energyClass !== 998 && energyClass !== 997,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!(998|997)).)*$/ }
					],
					optionIf: [
						{
							field: 'energyClass',
							options: energy
						},
					],
					style: { minHeight: 49 },
				},
				{
					control: 'radio',
					name: 'isCubeMeters',
					default: false,
					label: { text: 'Unità di misura' },
					options: [
						{ value: false, label: 'kWh/m2a', style: { float: 'left', marginRight: 15 }, selectedClassName: 'option-override' },
						{ value: true, label: 'kWh/m3a', style: { float: 'left' }, selectedClassName: 'option-override' },
					],
					value: listing.isCubeMeters,
					hideRadio: false,
					hide: energyClass !== 998 && energyClass !== 997,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!(998|997)).)*$/ }
					],
				},
				{
					control: 'label',
					style: {
						clear: 'both'
					},
				},
				{
					control: 'text',
					type: 'text',
					name: 'energyEfficiencyValue',
					onlyNumber: false,
					placeholder: '000,00',
					label: {
						text: energyClass === 997 ? 'Indice Prestazione Energetica (IPE)' : 'Non rinnovabile',
						changeIf: [
							{
								field: 'energyClass',
								regEx: /^997$/,
								ifTrue: 'Indice Prestazione Energetica (IPE)',
								ifFalse: 'Non rinnovabile'
							}
						],
					},
					isRequired: false,
					value:	listing.energyEfficiencyValue.toString().replace('.', ','),
					regEx: /(^$|^[1-9]\d{0,2}(?:,\d{1,2})?$)/,
					isValid: true,
					errorMessage: 'Inserisci un valore valido',
					hide: energyClass !== 998 && energyClass !== 997,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!(998|997)).)*$/ }
					],
					className: 'small-field'
				},
				{
					control: 'text',
					type: 'text',
					name: 'energyEfficiencyValueRenew',
					onlyNumber: false,
					placeholder: '000,00',
					label: {
						text: 'Rinnovabile',
					},
					isRequired: false,
					value: listing.energyEfficiencyValueRenew.toString().replace('.', ','),
					regEx: /(^$|^[1-9]\d{0,2}(?:,\d{1,2})?$)/,
					isValid: true,
					className: 'container-field-right small-field',
					hide: energyClass !== 998,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!998).)*$/ }
					],
					errorMessage: 'Inserisci un valore valido'
				},
				{
					control: 'label',
					style: { height: 10, clear: 'both' },
				},
				{
					control: 'check',
					name: 'isQuiteZeroEnergyeState',
					label: { text: 'Edificio a energia quasi zero' },
					value:	listing.isQuiteZeroEnergyeState,
					hideCheck: true,
					style: {
						marginTop: '-20px',
						minHeight: 'auto',
					},
					hide: energyClass !== 998,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!998).)*$/ }
					],
				},
				{
					control: 'label',
					style: { height: 10, clear: 'both' },
				},
				{
					control: 'radio',
					name: 'epiEnum',
					label: { text: 'Inverno (Epi)' },
					default: '0',
					options: [
						{ value: 1, label: '', className: 'smile' },
						{ value: 2, label: '', className: 'meh' },
						{ value: 3, label: '', className: 'frown' },
					],
					value:	listing.epiEnum,
					hideRadio: true,
					className: 'smiles small-field',
					hide: energyClass !== 998,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!998).)*$/ }
					],
				},
				{
					control: 'radio',
					name: 'epeEnum',
					label: { text: 'Estate (Epe)' },
					default: '0',
					options: [
						{ value: 1, label: '', className: 'smile' },
						{ value: 2, label: '', className: 'meh' },
						{ value: 3, label: '', className: 'frown' },
					],
					value:	listing.epeEnum,
					hideRadio: true,
					className: 'smiles container-field-right small-field',
					hide: energyClass !== 998,
					hideIf: [
						{ field: 'energyClass', regEx: /^((?!998).)*$/ }
					],
				},
				{
					control: 'label',
					name: 'final',
					style: { height: 20, clear: 'both' },
				},
			],
			beforeButton: el('div', {
				className: 'btn btn-white',
				style: {
					float: 'left',
					cursor: 'pointer',
					height: 40,
					lineHeight: '40px',
					width: 100,
					textAlign: 'center'
				},
				/* eslint-disable-next-line */
				onClick: () => { location.href = '/listings'; }
			}, 'Annulla'),
			sendButton: {
				text: 'Salva e prosegui',
				style: { width: 'auto' }
			},
			sendForm: (o) => {
				console.log(o);
				/* eslint-disable */
				const promise = new Promise((resolve, reject) => {
					this.props.actions.fetchSaveListing({ jwt, dataObject: o })
						.then((x) => {
							if (x.value.response.succeed) {
								resolve({ succeed: true, message: x.value.response.message });

								if (x.value.response.data.inserted) {
									this.props.history.push(`/listing/${x.value.response.data.inserted}/step-1`);
								}

								setTimeout(() => {
									if (x.value.response.data.inserted) {
										window.location.href = `/listing/${x.value.response.data.inserted}/step-2`;
									} else {
										window.location.href = this.props.pathname.replace('step-1', 'step-2');
									}
								}, 1000);
							} else {
								reject({ succeed: false, message: x.value.response.message });
							}
						});
					});
					return promise;
					/* eslint-enable */
			},
			formClassName: 'project-edit'
		});
	}
}

export default Details;
