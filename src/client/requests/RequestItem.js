// @flow
import React from 'react';
import PropTypes from 'prop-types';
import Form from 'client-form';
import Alert from '../components/common/CustomControls/Alert';

const el = React.createElement;

class RequestItem extends React.Component<any, any> {

	static defaultProps = {
		viewport: null,
		imageserver: null,
		read: null,
		jwt: null,
		requestId: null
	};

	state = {
		expand: false,
		expandAnswer: false,
		expandRequest: false,
		openModal: false,
		deleteError: null,
		loadSpinner: false
	};

	shouldComponentUpdate(nextProps: Object, nextState: Object) {
		if (nextState.expand !== this.state.expand) return true;
		if (nextState.expandAnswer !== this.state.expandAnswer) return true;
		if (nextState.expandRequest !== this.state.expandRequest) return true;
		if (nextState.openModal !== this.state.openModal) return true;
		if (nextState.loadSpinner !== this.state.loadSpinner) return true;
		return false;
	}

	expandReply() {
		const { expand } = this.state;
		this.setState({
			expand: !expand
		});
	}

	expandAnswer() {
		const { expandAnswer } = this.state;
		this.setState({
			expandAnswer: !expandAnswer
		});
	}

	expandRequest() {
		const { expandRequest } = this.state;
		this.setState({
			expandRequest: !expandRequest
		});
	}

	openOverlay() {
		const { openModal } = this.state;
		this.setState({
			openModal: !openModal,
			deleteError: null
		});
	}

	deleteRequest() {
		const { actions, jwt } = this.props;

		this.setState({
			loadSpinner: true,
		});

		actions.deleteRequest({ jwt })
			.then((x) => {
				setTimeout(() => {
					if (x.value.response.succeed) {
						this.setState({
							loadSpinner: false,
							deleteError: false
						});
					} else {
						this.setState({
							loadSpinner: false,
							deleteError: true
						});
					}
				}, 1000);
			});
	}

	resetOverlay() {
		this.setState({
			loadSpinner: false,
			deleteError: null
		});
	}

	render() {
		const { listing, user, requestId, actions, jwt, viewport, imageserver, read, answers, utils: { makeId } } = this.props;
		const { expand, expandAnswer, openModal, loadSpinner, deleteError, expandRequest } = this.state;
		const imgWidth = viewport.width >= 560 ? 170 : viewport.width >= 375 ? 149 : 98;

		return el('div', { className: 'requests' },
			el('div', { className: 'requests-top' },
				read === false ? el('div', { className: 'new-request-label' }, 'NUOVA') : null,
				el('div', {}, el('strong', {}, listing.title)),
				el('div', { onClick: () => { this.openOverlay(); } },
					el('svg', { width: '24', height: '24', viewBox: '0 0 14 17' },
						el('g', { fill: '#949da2', fillRule: 'evenodd' },
							el('path', { d: 'M11.515 15.447a.515.515 0 0 1-.496.48H3.222a.559.559 0 0 1-.527-.5L2.045 4.67H1.024l.653 10.82c.049.805.744 1.463 1.546 1.463h7.797c.802 0 1.484-.659 1.516-1.464l.424-10.819h-1.021l-.423 10.778zM4.86 1.466c0-.239.2-.44.438-.44h3.344c.238 0 .438.201.438.44v1.016H4.86V1.466zm8.57 1.016H10.1V1.466C10.1.66 9.445 0 8.643 0H5.298C4.496 0 3.84.66 3.84 1.466v1.016H.51c-.282 0-.51.23-.51.513 0 .284.228.513.51.513h12.92c.282 0 .51-.23.51-.513a.512.512 0 0 0-.51-.513z' })
						)
					)
				)
			),
			el('div', { className: 'requests-bottom' },
				el('div', { className: 'requests-left' },
					el('img', { src: `${imageserver}${imgWidth}x98/${listing.imgUrl}`, alt: '' }),
					el('div', { className: 'requests-left-detail' },
						el('div', { style: { float: 'left', width: '100%', fontWeight: 700 } }, listing.price, ' \u20AC'),
						el('div', { style: { float: 'left', width: '100%' } },
							el('span', { style: { fontWeight: 700 } }, listing.roomsNum),
							el('span', {}, ' locali '),
							el('span', { style: { fontWeight: 700 } }, listing.surface),
							el('span', {}, ' m²'),
						),
						el('div', { style: { float: 'left', width: '100%', whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' } }, listing.address),
						el('div', { style: { float: 'left', width: '100%', fontWeight: 700 } }, listing.listingId))),
				el('div', { className: 'requests-central' },
					el('div', { className: 'requests-central-left' },
						el('div', { className: '' },
							el('svg', { width: 17, height: 24, viewBox: '0 0 17 24' },
								el('path', { fill: '#96A1B0', d: 'M17 21a1 1 0 0 1-2 0v-2a3 3 0 0 0-3-3H5a3 3 0 0 0-3 3v2a1 1 0 0 1-2 0v-2a5 5 0 0 1 5-5h7a5 5 0 0 1 5 5v2zm-8.5-9a5 5 0 1 1 0-10 5 5 0 0 1 0 10zm0-2a3 3 0 1 0 0-6 3 3 0 0 0 0 6z' })
							)
						)
					),
					el('div', { className: 'requests-central-right' },
						el('div', { className: 'requests-central-text' }, user.fullName),
						el('div', { className: 'requests-central-text' }, user.phone),
						el('div', { className: 'requests-central-text' }, el('a', { href: `mailto:${user.email}` }, user.email))
					),
					el('div', { className: expandRequest === true ? 'requests-central-answer expand-answer' : 'requests-central-answer' },
						el('div', { className: 'requests-central-answer-icon' }, 'icon'),
						el('div', { className: 'requests-central-answer-detail' },
							el('div', { className: 'requests-central-answer-date' }, user.sentDt),
							el('div', { className: 'requests-central-answer-text' }, user.text),
							el('div', { className: 'requests-central-answer-expand' },
								el('svg', { width: '24', height: '24', viewBox: '0 0 24 24', onClick: () => { this.expandRequest(); }, className: expandRequest === true ? 'rotate' : '' },
									el('rect', { fill: 'none', width: 24, height: 24, x: 0, y: 0 }),
									el('polyline', { fill: 'none', points: '6,9 12,15 18,9', style: { fill: 'none', stroke: '#39434e', strokeWidth: 1 } }))
							),
						)
					),
					answers && answers.length > 0 ?
						el('div', { className: expandAnswer === true ? 'requests-central-answer expand-answer' : 'requests-central-answer' },
							el('div', { className: 'requests-central-answer-icon' }, 'icon'),
							el('div', { className: 'requests-central-answer-detail' },
								el('div', { className: 'requests-central-answer-date' }, answers[0].sentDt),
								el('div', { className: 'requests-central-answer-text' }, answers[0].text),
								el('div', { className: 'requests-central-answer-expand' },
									el('svg', { width: '24', height: '24', viewBox: '0 0 24 24', onClick: () => { this.expandAnswer(); }, className: expandAnswer === true ? 'rotate' : '' },
										el('rect', { fill: 'none', width: 24, height: 24, x: 0, y: 0 }),
										el('polyline', { fill: 'none', points: '6,9 12,15 18,9', style: { fill: 'none', stroke: '#39434e', strokeWidth: 1 } }))
								),
							)
						) : null,
					el('div', { className: expand === true ? 'requests-under requests-under-show' : 'requests-under' },
						el(Form, {
							key: makeId(),
							controls: [{
								control: 'text',
								type: 'hidden',
								name: 'requestId',
								value: requestId,
								style: {
									display: 'none'
								}
							}, {
								control: 'textArea',
								name: 'replyBody',
								placeholder: 'Scrivi qui la tua risposta',
								value: '',
								limitChar: 1000,
								label: { style: { display: 'none' } },
								isRequired: true,
								errorMessage: 'campo obbligatorio'
							}],
							sendButton: {
								text: 'Invia risposta',
							},
							beforeButton: el('button', { className: 'btn btn-white', onClick: () => { this.expandReply(); } }, 'Chiudi'),
							sendForm: (o) => {
								const promise = new Promise((resolve, reject) => {
									actions.sendReply({
										jwt,
										dataObject: o
									}).then((x) => {
										if (x.value.response.succeed) {
											setTimeout(() => {
												resolve({
													succeed: true,
													message: x.value.response.message
												});
											}, 1000);
										} else {
											// eslint-disable-next-line
											reject({
												succeed: false,
												message: x.value.response.message
											});
										}
									});
								});
								return promise;
							}
						}))
				),
				el('div', { className: 'requests-right' },
					el('button', { className: 'btn btn-red', onClick: () => { this.expandReply(); } }, 'Rispondi')),
			),
			openModal === true ?
				el(Alert, {
					closeActionOverlay: () => { this.openOverlay(); },
					resetAction: () => { this.resetOverlay(); },
					viewport,
					actionTitle: 'Confermi l\'eliminazione?',
					handleAction: null,
					deleteError,
					actionTextRed: 'Attenzione! L\'operazione è irreversibile.',
					actionTextBlack: el('span', {},
						'Cliccando su ',
						el('strong', {}, 'conferma'),
						', la richiesta verrà eliminata',
						el('br', {}),
						'e non sarà più possibile recuperarla.'
					),
					icon: '/admin/assets/img/icons/icon-bin-big.svg',
					alertHeight: 370,
					confirmAction: () => { this.deleteRequest(); },
					loadSpinner,
					successMessage: 'Richiesta eliminata'
				}, '')
				: null
		);
	}
}

RequestItem.propTypes = {
	user: PropTypes.instanceOf(Object).isRequired,
	listing: PropTypes.instanceOf(Object).isRequired,
	actions: PropTypes.instanceOf(Object).isRequired,
	viewport: PropTypes.instanceOf(Object),
	imageserver: PropTypes.string,
	read: PropTypes.bool,
	jwt: PropTypes.string,
	requestId: PropTypes.number
};

RequestItem.defaultProps = {
	viewport: null,
	imageserver: null,
	read: null,
	jwt: null,
	requestId: null
};

export default RequestItem;
