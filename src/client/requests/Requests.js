// @flow
import React from 'react';
import RequestItem from './RequestItem';
// flow-disable-next-line
import './Requests.scss';

const el = React.createElement;

class Requests extends React.Component<any, any> {

	shouldComponentUpdate(nextProps: Object) {
		if (nextProps.dataObject !== this.props.dataObject) return true;
		if (nextProps.viewport !== this.props.viewport) return true;
		return false;
	}

	render() {
		const { dataObject, actions, jwt, viewport, imageserver, utils } = this.props;

		return el('div', { className: 'req' },
			dataObject.map(item => el(RequestItem, Object.assign({}, item, { actions, jwt, key: Math.random(), viewport, imageserver, utils }))));
	}
}

export default Requests;
