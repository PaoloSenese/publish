// @flow
import React from 'react';
import { fetch } from '../../common/components';
import * as utils from '../../common/helpers/utils';

import { userDevice } from '../helpers/DOMHelpers';
import { fetchListings } from '../../common/redux/listings/actions';
import { fetchListing } from '../../common/redux/listing/actions';

export default function UiHandlerHOC(Content: any, fetchFunctionString: string) {
	class UiHandlerComponent extends React.Component<any, any> {
		static displayName = 'UiHandlerHOC';

		doc: ?HTMLElement;

		constructor(props) {
			super(props);

			this.state = {
				device: '',
				viewport: { width: '', height: '' },
				utils
			};
		}

		componentDidMount() {
			const ui = userDevice();
			this.setUiInfos(ui);

			this.doc = document.body;
			if (!NodeList.prototype.forEach && Array.prototype.forEach) {
				// flow-disable-next-line
				NodeList.prototype.forEach = Array.prototype.forEach;
			}
		}

		setUiInfos(ui) {
			this.setState({
				device: ui.device,
				viewport: ui.viewport,
				utils
			});
		}

		render() {
			return (
				<Content {...this.props} {...this.state} />
			);
		}
	}
	let fetchFunction = null;
	if (fetchFunctionString === 'fetchListings') fetchFunction = fetchListings;
	if (fetchFunctionString === 'fetchListing') fetchFunction = fetchListing;

	const UiHandler = fetch(fetchFunction)(UiHandlerComponent);
	return UiHandler;
}
