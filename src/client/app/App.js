// @flow
import React from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

const el = React.createElement;

type AppTypes = {
	children: Element,
	favicon: string,
	title: string
};

const App = ({ children, favicon, title }: AppTypes) => el('div', {},
	el(Helmet, {
		htmlAttributes: { lang: 'it' },
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1, shrink-to-fit=no',
			},
			{ 'http-equiv': 'x-ua-compatible', content: 'ie=edge' },
		],
		link: [...favicon],
		title
	}),
	/* flow-disable-next-line */
	el('div', { className: 'admin static_header' }, children)
);

export default connect((state, store) => ({
	favicon: state.config.favicon,
	store,
}))(App);
