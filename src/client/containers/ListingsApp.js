import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import AppRoot from '../listings/ListingsPage';
import * as listingsActions from '../../common/redux/listings/actions';

function mapStateToProps(state) {
	return {
		...state,
	};
}

const actions = [listingsActions];

function mapDispatchToProps(dispatch) {
	const getActionFunctions = (act) => {
		let resultObj = {};
		act.map((action) => {
			Object.keys(action).forEach((key) => {
				if (typeof action[key] === 'function') {
					resultObj = Object.assign({}, resultObj, { [key]: action[key] });
				}
			});
			return action;
		});

		return resultObj;
	};

	const creators = getActionFunctions(actions);

	return {
		actions: bindActionCreators(creators, dispatch),
		dispatch,
	};
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(AppRoot));
