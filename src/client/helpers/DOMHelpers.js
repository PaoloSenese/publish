// User device specs detection
export function userDevice() {
	const viewport = {
		width: document.documentElement.clientWidth,
		height: document.documentElement.clientHeight,
	};

	let device = '';
	if (viewport.width > 950) {
		device = 'desktop';
	} else if (
		(viewport.width < 951 && viewport.width > 670) || (viewport.width > 950)
	) {
		device = 'tablet';
	} else {
		device = 'smartphone';
	}
	return {
		viewport, device
	};
}
