// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import configureStore from '../common/configureStore';
import Root from './containers/ListingApp';

const appElement = document.getElementById('app');
// Grab the state from a global injected into server-generated HTML
const initialState = window.__INITIAL_STATE__; // eslint-disable-line no-underscore-dangle

// Create Redux store with initial state
const store = configureStore({
	initialState,
	platformReducers: { },
	platformStoreEnhancers: { },
});

const afterInitialRender = () => {
	// flow-disable-next-line
	if (!module.hot || typeof module.hot.accept !== 'function') return;
	module.hot.accept('./containers/ListingApp', () => {
		// $FlowIssue: check it
		const NextRoot = require('./containers/ListingApp');

		if (appElement !== null) {
			ReactDOM.hydrate(
				<BrowserRouter>
					<NextRoot store={store} />
				</BrowserRouter>,
				appElement,
			);
		}
	});
};

if (appElement !== null) {
	ReactDOM.hydrate(
		<BrowserRouter>
			<Root store={store} />
		</BrowserRouter>,
		appElement,
		afterInitialRender
	);
}
