// @flow
import React from 'react';
import PropTypes from 'prop-types';

const Image = (props: Object) => {
	const { alt, className, style, size, imageserver, src } = props;
	/* eslint-disable */
	return (
		<img {...{
			src: `${imageserver}${size}/${src}`,
			className,
			alt,
			style,
			onError: (e) => { e.target.src = `${imageserver}${size}/props/placeholder.png`; }
		}} />
	);
	/* eslint-enable */
};

Image.propTypes = {
	src: PropTypes.string,
	className: PropTypes.string,
	alt: PropTypes.string,
	style: PropTypes.instanceOf(Object),
	size: PropTypes.string,
	imageserver: PropTypes.string
};

Image.defaultProps = {
	src: '',
	className: '',
	alt: '',
	style: {},
	size: '',
	imageserver: ''
};

export default Image;
