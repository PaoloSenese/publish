// @flow
import React from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';

// flow-disable-next-line
import './DatePickerField.scss';

moment.locale('it');

class DatePickerField extends React.Component<any, any> {

	state = {
		value: this.props.value !== undefined && this.props.value !== null && this.props.value !== '' ? moment(this.props.value, 'DD/MM/YYYY') : null,
	};

	handleChange(date: string) {
		this.setState({
			value: date
		});
		if (this.props.updateOnChange === true) {
			this.props.onUpdate({
				target: {
					name: this.props.name,
					value: moment(date, 'DD/MM/YYYY').format('DD/MM/YYYY'),
				}
			}, false);
		}
	}

	onBlur(event: Object) {
		if (event !== undefined) this.props.onUpdate(event, false);
	}

	render() {
		const { label, name, className, isRequired, greaterThan, errorMessage, isValid, style } = this.props;

		return (
			<div key={name} className={`container-field ${className}`} style={style}>
				<label htmlFor={name} className="field-label noselect" style={Object.assign({}, label.style, !isValid ? { color: '#e4002b' } : {})}>{label.text}{isRequired ? '*' : null}</label>
				<input type="text" name={name} id={name} style={{ opacity: 0, height: 0, position: 'absolute', width: 0 }} disabled />
				<div className={isValid ? 'field-picker-container' : 'field-picker-container field-picker-container-error'}>
					<DatePicker {...{
						name,
						selected: this.state.value,
						onChange: (e) => { this.handleChange(e); },
						onBlur: (e) => { this.onBlur(e); },
					}} />
				</div>
				<div className="validation-error noselect">
					{(isRequired || greaterThan) && !isValid ? errorMessage : '' }
					&nbsp;
				</div>
			</div>
		);
	}
}

export default DatePickerField;
