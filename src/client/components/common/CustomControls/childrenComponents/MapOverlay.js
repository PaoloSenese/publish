// @flow
import React, { createElement } from 'react';
import Form from 'client-form';

const el = createElement;

const MapOverlay = (props: Object) => {
	const { options, resetForm, hideConfirm, closeOverlay, confirmPosition, viewport, utils: { getViewport } } = props;

	return el(Form, {
		key: resetForm,
		controls: [
			{
				control: 'radio',
				name: 'confirmSelection',
				label: { text: '' },
				default: 1,
				options,
				hideRadio: false,
				value: 1,
				className: 'radio-expand'
			},
		],
		beforeButton: el('div', {
			onClick: () => { closeOverlay(); },
			className: 'btn btn-white'
		}, 'Annulla'),
		buttonContainerStyle: { float: 'right', width: 'auto', marginTop: 10 },
		sendButton: {
			className: 'btn btn-red',
			text: 'Conferma',
			style: { display: hideConfirm ? 'none' : 'inline-block' }
		},
		sendForm: (o) => {
			/* eslint-disable */
			confirmPosition(parseInt(o.confirmSelection, 10));
			return new Promise((resolve, reject) => {
				resolve({ succeed: true, message: '' });
			});
			/* eslint-enable */
		},
		formStyle: { paddingLeft: 0, width: getViewport(viewport).width > 600 ? 420 : '100%' },
	});
};

export default MapOverlay;
