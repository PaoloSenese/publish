import React, { Component, createElement, Fragment } from 'react';

import './LeafLetComp.scss';
import 'leaflet/dist/leaflet.css';

const el = createElement;
const zoomLevel = 16;
let leafLetMap = null;
let leafLetMarker = null;
let leafLetZoom = null;

class LeafLetComp extends Component {

	state = {
		controlMap: this.props.controlMap,
		mapCenter: [this.props.lat, this.props.lng],
		isMobile: false
	}

	shouldComponentUpdate(nextProps: Object, nextState: Object) {
		if (this.props.lat !== nextProps.lat) return true;
		if (this.props.lng !== nextProps.lng) return true;
		if (this.state.controlMap !== nextState.controlMap) return true;
		if (this.props.updateMarker !== nextProps.updateMarker) return true;
		if (this.state.mapCenter !== nextState.mapCenter) return true;
		if (this.state.isMobile !== nextState.isMobile) return true;
		if (this.props.controlMap !== nextProps.controlMap) return true;
		return false;
	}

	componentDidMount() {
		if (process.browser) {
			const { mapCenter, controlMap } = this.state;
			const L = require('leaflet');

			leafLetMap = L.map('map', {
				zoom: 16,
				minZoom: 5,
				maxZoom: 16,
				zoomControl: false,
				doubleClickZoom: false,
			}).setView(mapCenter, 16);

			leafLetMap.dragging.disable();
			leafLetMap.scrollWheelZoom.disable();
			leafLetMap.touchZoom.disable();

			leafLetMap.on('click', (e) => {
				const { controlMap } = this.state;
				if (controlMap) {
					window.setTimeout(() => {
						const newLatLng = new L.LatLng(e.latlng.lat, e.latlng.lng);
			    	leafLetMarker.setLatLng(newLatLng);
						this.props.dragEnd({ position: newLatLng });
					}, 50);
				}
			});

			L.tileLayer(this.props.tileLayer, {}).addTo(leafLetMap);

			const iconPerson = new L.Icon({
				iconUrl: '/admin/assets/img/pinpoint_default.png',
				iconAnchor: null,
				popupAnchor: null,
				shadowUrl: null,
				shadowSize: null,
				shadowAnchor: null,
				iconSize: new L.Point(26, 38),
				iconAnchor: new L.point(11, 36),
			});

			leafLetMarker = L.marker(mapCenter, {
				autoPan: true,
				icon: iconPerson,
				draggable: controlMap,
			});

			leafLetMarker.addTo(leafLetMap);
		}
	}

	componentWillReceiveProps(nextProps: Object) {
		const { lat, lng, viewport, updateMarker } = this.props;
		const { controlMap } = this.state;
		if (nextProps.updateMarker !== null) {
			leafLetMap.dragging.disable();
			leafLetMap.scrollWheelZoom.disable();
			leafLetMap.touchZoom.disable();
			leafLetMarker.dragging.disable();

			const newLatLng = new L.LatLng(nextProps.lat, nextProps.lng);
    	leafLetMarker.setLatLng(newLatLng);
			leafLetMap.panTo(newLatLng);
			if (leafLetZoom) leafLetZoom.remove();

			this.setState({ controlMap: false, isMobile: this.getViewport(viewport).width < 600 ? (controlMap ? false : true) : false });

			if (this.getViewport(viewport).width < 600) {
				window.setTimeout(() => {
					leafLetMap.invalidateSize();
				}, 400);
			}
		}
	}

	enableControl() {
		const { viewport } = this.props;
		const { controlMap, mapCenter } = this.state;
		this.setState({
			controlMap: !controlMap,
			isMobile: this.getViewport(viewport).width < 600 ? (controlMap ? false : true) : false
		});

		leafLetMap.dragging.enable();
		leafLetMap.scrollWheelZoom.enable();
		leafLetMap.touchZoom.enable();
		leafLetZoom = L.control.zoom({
			position: 'topleft'
		})
		leafLetZoom.addTo(leafLetMap);

		leafLetMarker.dragging.enable();
		leafLetMarker.on('dragend', (e) => {
			this.props.dragEnd({ position: e.target._latlng });
    });

		if (this.getViewport(viewport).width < 600) {
			window.setTimeout(() => {
				leafLetMap.invalidateSize();
			}, 400);
		}
	}

	mapClick(e) {
		const { localMode } = this.props;
		const { controlMap } = this.state;
		window.setTimeout(() => {
			if (controlMap) this.props.dragEnd({ position: e.latlng });
		}, 2000);
	}

	getViewport = (viewport: Object) => {
		if (viewport.height === '' && document && document.documentElement) return { width: document.documentElement.clientWidth, height: document.documentElement.clientHeight };
		return viewport;
	}

	render() {
		const { lat, lng, onClick, style, tileLayer } = this.props;
		const { controlMap, mapCenter, isMobile } = this.state;
		const mapControls = controlMap ?
			{ minZoom: 10, zoomControl: false, dragging: true, scrollWheelZoom: true }
			:
			{ maxZoom: 16, zoomControl: false, dragging: false, doubleClickZoom: false, scrollWheelZoom: false };

		return el('div', { className: isMobile ? 'leaflet-comp box-shadow leaflet-mobile' : 'leaflet-comp box-shadow', style },
			el('div', { id: 'map' }),
			controlMap ? null : el('div', { className: 'btn btn-over-map btn-small', onClick: () => this.enableControl() }, 'Modifica posizione')
		);

	}
}

export default LeafLetComp;
