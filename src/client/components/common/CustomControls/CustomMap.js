// @flow
import React, { createElement } from 'react';
import ClickOutHandler from 'react-onclickout';
import TownAutoSuggest from './TownAutoSuggest';
import MapOverlay from './childrenComponents/MapOverlay';
import LeafLetComp from './childrenComponents/LeafLetComp';
import Alert from './Alert';

// flow-disable-next-line
import './Alert.scss';
// flow-disable-next-line
import './CustomMap.scss';

let autocomplete = null;
const el = createElement;

const toRadian = degree => degree * Math.PI / 180;

const getDistance = (origin, destination) => {
	const lon1 = toRadian(origin[1]);
	const lat1 = toRadian(origin[0]);
	const lon2 = toRadian(destination[1]);
	const lat2 = toRadian(destination[0]);

	const deltaLat = lat2 - lat1;
	const deltaLon = lon2 - lon1;

	const a = Math.pow(Math.sin(deltaLat / 2), 2) + Math.cos(lat1) * Math.cos(lat2) * Math.pow(Math.sin(deltaLon / 2), 2);
	const c = 2 * Math.asin(Math.sqrt(a));
	const EARTH_RADIUS = 6371;
	return c * EARTH_RADIUS * 1000;
};

class CustomMap extends React.Component<any, any> {

	state = {
		value: this.props.value,
		townZones: [],
		options: [],
		openOverlay: null,
		message: null,
		hideConfirm: null,
		hideCancel: null,
		resetForm: null,
		icon: null,
		alertHeight: 0,
		lat: null,
		lng: null,
		geoObject: {},
		cityInArray: false,
		updateMarker: null,
		mapInfo: false
	};

	async componentDidMount() {
		const townZones = await this.props.actions.searchTown({
			search: this.getValue('suburb').value,
			match: true
		}).then((x) => {
			if (x.value.response.succeed && x.value.response.data.length === 1 && x.value.response.data[0].townZones.length > 0) {
				return x.value.response.data[0].townZones;
			} else {
				return [];
			}
		});

		this.setState({ townZones: townZones });
		this.setAutoComplete();
	}

	setZone(lat, lng) {
		const { citiesArray } = this.props;
		const cityInArray = citiesArray.indexOf(this.getValue('suburb').value) !== -1;

		this.props.actions.getZone({
			lat: lat, lng: lng, type: 'Zone'
		}).then((x) => {
			if (x.value.response.succeed) {
				const stateObject = this.stateObject({
					target: {
						name: 'townZone',
						value: parseInt(x.value.response.data.locations[0].id.replace('Z-', ''), 10)
					}
				});
				this.setState({
					value: stateObject,
					cityInArray: cityInArray
				});
			}
		});
	}

	async setAutoComplete() {
		const { citiesArray } = this.props;
		const that = this;
		const streetName = document.getElementById('streetName');
		const bounds = await this.getBounds();

		(function pacSelectFirst(input) {
			var _addEventListener = (input.addEventListener) ? input.addEventListener : input.attachEvent;

			function addEventListenerWrapper(type, listener) {
				if (type == "keydown") {
					var orig_listener = listener;
					listener = function(event) {
						var suggestion_selected = document.getElementsByClassName('pac-item-selected').length > 0;
						if (event.which == 13 && !suggestion_selected) {
							var simulated_downarrow = new KeyboardEvent('keydown', { keyCode: 40, ctrlKey: true });
							orig_listener.apply(input, [simulated_downarrow]);
						}
						orig_listener.apply(input, [event]);
					};
				}
				_addEventListener.apply(input, [type, listener]);
			}
			input.addEventListener = addEventListenerWrapper;
			input.attachEvent = addEventListenerWrapper;

			autocomplete = new window.google.maps.places.Autocomplete(
				input,
				{
					types: ['geocode'],
					componentRestrictions: { country: 'it' },
					strictBounds: true,
					bounds: bounds,
					fields: ['address_components','geometry.location']
				}
			);

		})(streetName);

		window.google.maps.event.addListener(autocomplete, 'place_changed', (e) => {
			const place = autocomplete.getPlace();

			if (place && place.address_components) {
				const geoObject = that.getGeoObject(place.address_components, that.getValue('suburb').value);
				that.setGeoObject(geoObject, { lat: place.geometry.location.lat(), lng: place.geometry.location.lng() }, false);
				if (citiesArray.indexOf(this.getValue('suburb').value) !== -1) that.setZone(place.geometry.location.lat(), place.geometry.location.lng());
				this.setState({ updateMarker: Math.random() });
			} else {
				that.geocodeByCity(true);
			}

			that.setState({ streetNameFilled: true });
		});

		if (streetName.value !== '') this.setState({ streetNameFilled: true });
		if (citiesArray.indexOf(this.getValue('suburb').value) !== -1) this.setState({ cityInArray: true });
	}

	async getBounds() {
		const townResponse = await this.props.actions.getZone({
			lat: this.getValue('lat').value,
			lng: this.getValue('lng').value,
			type: 'Town'
		}).then((x) => {
			if (x.value.response.succeed) {
				return x.value.response.data
			}
		});

		const locationResponse = await this.props.actions.getLocation({
			locationId: townResponse.locations[0].id,
			perimeter: true
		}).then((x) => {
			if (x.value.response.succeed) {
				return x.value.response.data
			}
		});

		const sw = locationResponse.location.geo.bounding_box.coordinates[0][3];
		const ne = locationResponse.location.geo.bounding_box.coordinates[0][1];

		return new google.maps.LatLngBounds(
			new google.maps.LatLng (sw[1], sw[0]),
			new google.maps.LatLng (ne[1], ne[0]),
		)
	}

	getStateValAsObject() {
		const { value } = this.state;
		return {
			coordinateEnabled: this.getValue('coordinateEnabled', value).value,
			isAddressVisible: this.getValue('isAddressVisible', value).value,
			lat: this.getValue('lat', value).value,
			lng: this.getValue('lng', value).value,
			postCode: this.getValue('postCode', value).value,
			streetName: this.getValue('streetName', value).value,
			streetNumber: this.getValue('streetNumber', value).value,
			suburb: this.getValue('suburb', value).value,
			townId: this.getValue('townId', value).value,
			townZone: this.getValue('townZone', value).value
		};
	}

	geocodeByCity(cityOnly) {
		const that = this;
		const { citiesArray, mapTexts, localMode, utils: { MakeId, notEmpty, logLocalOnly } } = that.props;
		const { postCode, suburb, streetName, streetNumber } = this.getStateValAsObject();
		const address = cityOnly || (notEmpty(postCode) && notEmpty(suburb) && !notEmpty(streetName) && !notEmpty(streetNumber))
			? suburb
			: `${streetName} ${streetNumber} ${postCode} ${suburb}`;

		/* eslint-disable-next-line */ // flow-disable-next-line
		const geocoder = new google.maps.Geocoder();
		/* eslint-disable-next-line */
		geocoder.geocode({ address, componentRestrictions: { country: 'IT' } }, (results, status) => {
			/* eslint-disable-next-line */
			if (status === google.maps.GeocoderStatus.OK) {
				let forcedResult = results.filter(o => o.formatted_address.indexOf(suburb) !== -1)[0];
				if (!forcedResult) {
					// eslint-disable-next-line
					forcedResult = results[0];
				}

				const mapCenterLat = forcedResult.geometry.location.lat();
				const mapCenterLng = forcedResult.geometry.location.lng();
				const geoObject = that.getGeoObject(forcedResult.address_components, suburb);
				if (citiesArray.indexOf(this.getValue('suburb').value) !== -1) that.setZone(forcedResult.geometry.location.lat(), forcedResult.geometry.location.lng());

				logLocalOnly({
					address: address,
					geoObject: geoObject,
					city: geoObject.suburb,
					cityOld: suburb,
					streetNameNew: geoObject.streetName,
					streetNameOld: streetName,
					streetNumberNew: geoObject.streetNumber,
					streetNumberOld: streetNumber,
					postCodeNew: geoObject.postCode,
					postCodeOld: postCode,
					newLat: mapCenterLat,
					newLng: mapCenterLng
				}, localMode);

				if (!cityOnly && (geoObject.suburb && geoObject.suburb.toLowerCase() === suburb.toLowerCase()) && notEmpty(streetName) && !geoObject.streetName && !geoObject.streetNumber) {
					that.handleOpenOverlay({
						openOverlay: true,
						message: mapTexts.addressNotFound,
						lat: mapCenterLat,
						lng: mapCenterLng,
						geoObject,
						hideConfirm: true,
						hideCancel: false,
						options: [],
						resetForm: makeId(),
						icon: '/admin/assets/img/icons/icon-plan.svg',
						alertHeight: 290
					});
				} else {
					that.setGeoObject(geoObject, { lat: mapCenterLat, lng: mapCenterLng }, true);
				}
			}
		});
	}

	setGeoObject(geoObject: Object, latlng: Object, updateMarker: boolean) {
		try {
			const geo = [];
			this.state.value.forEach((v) => {
				if (v.name === 'postCode') {
					v.value = geoObject && geoObject.postCode ? geoObject.postCode : this.getValue('postCode').value;
					geo.push(v);
				} else if (v.name === 'streetName') {
					v.value = geoObject && geoObject.streetName ? geoObject.streetName : this.getValue('streetName').value;
					geo.push(v);
				} else if (v.name === 'streetNumber') {
					v.value = geoObject && geoObject.streetNumber ? geoObject.streetNumber : this.getValue('streetNumber').value;
					geo.push(v);
				} else if (v.name === 'lat') {
					v.value = latlng.lat;
					geo.push(v);
				} else if (v.name === 'lng') {
					v.value = latlng.lng;
					geo.push(v);
				} else {
					geo.push(v);
				}
			});

			if (updateMarker) {
				this.setState({ updateMarker: Math.random() });
			} else if (!updateMarker && geo) {
				const eventObject = {
					target: {
						name: this.props.name,
						value: geo,
					}
				};
				this.props.onUpdate(eventObject, true);
			}
		} catch (e) {
			console.log(e);
		}
	}

	dragEnd(e) {
		const that = this;
		const latlng = { lat: e.position.lat, lng: e.position.lng };

		/* eslint-disable-next-line */ // flow-disable-next-line
		const geocoder = new google.maps.Geocoder();
		geocoder.geocode({ location: latlng }, (results, status) => {
			/* eslint-disable-next-line */
			if (status === google.maps.GeocoderStatus.OK) {
				let address_components = [];
				if (results.length > 0) {
					for (let i = 0; i < results.length; i++) {
						if (results[i].types.indexOf('street_address') !== -1) {
							// eslint-disable-next-line
							address_components = results[i].address_components;
							break;
						}
					}
				}

				const { mapTexts, localMode, listingId, utils: { replaceVar, makeId, logLocalOnly } } = that.props;
				const mapCenterLat = latlng.lat;
				const mapCenterLng = latlng.lng;
				let inLocalStorage = localStorage.getItem(`geoItem_${listingId}`) !== null;
				let localObject = {};
				if (inLocalStorage) {
					localObject = JSON.parse(localStorage.getItem(`geoItem_${listingId}`));
				}
				const distance = getDistance(
					[latlng.lat, latlng.lng],
					inLocalStorage ?
					[localObject.lat, localObject.lng] :
					[that.getValue('lat').value, that.getValue('lng').value]
				);
				const geoObject = that.getGeoObject(address_components, that.getValue('suburb').value);

				logLocalOnly({
					geoObject: geoObject,
					city: geoObject.suburb,
					cityOld: that.getValue('suburb').value,
					streetNameNew: geoObject.streetName,
					streetNameOld: that.getValue('streetName').value,
					streetNumberNew: geoObject.streetNumber,
					streetNumberOld: that.getValue('streetNumber').value,
					postCodeNew: geoObject.postCode,
					postCodeOld: that.getValue('postCode').value,
					newLat: mapCenterLat,
					newLng: mapCenterLng
				}, localMode);

				if (!geoObject.suburb && !geoObject.streetName && !geoObject.streetNumber && !geoObject.postCode) {
					that.handleOpenOverlay({
						openOverlay: true,
						message: mapTexts.addressNotFound,
						lat: mapCenterLat,
						lng: mapCenterLng,
						geoObject,
						hideConfirm: true,
						hideCancel: false,
						options: [],
						resetForm: makeId(),
						icon: '/admin/assets/img/icons/icon-plan.svg',
						alertHeight: 290
					});
				} else if (geoObject.suburb.toLowerCase() !== that.getValue('suburb').value.toLowerCase()) {
					that.handleOpenOverlay({
						openOverlay: true,
						message: replaceVar(mapTexts.suburbChanged, { old: that.getValue('suburb').value, new: geoObject.suburb }),
						lat: mapCenterLat,
						lng: mapCenterLng,
						geoObject,
						hideConfirm: true,
						options: [],
						resetForm: makeId(),
						icon: '/admin/assets/img/icons/icon-plan.svg',
						alertHeight: 305
					});
				} else if (geoObject.suburb.toLowerCase() === that.getValue('suburb').value.toLowerCase() && geoObject.streetName !== that.getValue('streetName').value && that.getValue('streetName').value !== '') {
					that.handleOpenOverlay({
						openOverlay: true,
						message: distance > mapTexts.maxDistanceValue ?
							replaceVar(mapTexts.tooFarPosition, { old: that.getValue('streetName').value, new: geoObject.streetName })
							:
							replaceVar(mapTexts.streetNameChanged, { old: that.getValue('streetName').value, new: geoObject.streetName }),
						lat: mapCenterLat,
						lng: mapCenterLng,
						geoObject,
						options: distance > mapTexts.maxDistanceValue ? [
							{
								value: 1,
								label: replaceVar(mapTexts.tooFarPositionOptions.first, { new: `${geoObject.streetName} ${geoObject.streetNumber}` }),
								style: { float: 'left', width: '100%' }
							},
							{
								value: 3,
								label: replaceVar(mapTexts.tooFarPositionOptions.second, { old: `${that.getValue('streetName').value} ${that.getValue('streetNumber').value}` }),
								style: { float: 'left', width: '100%' }
							},
						]
							:
							[
								{
									value: 1,
									label: replaceVar(mapTexts.streetNameOptions.first, { new: geoObject.streetName }),
									style: { float: 'left', width: '100%'	}
								},
								{
									value: 2,
									label: replaceVar(mapTexts.streetNameOptions.second, { old: that.getValue('streetName').value }),
									style: { float: 'left', width: '100%' }
								},
							],
						resetForm: makeId(),
						distance,
						icon: '/admin/assets/img/icons/icon-plan.svg',
						alertHeight: distance > mapTexts.maxDistanceValue ? 410 : 445
					});
				} else if (geoObject.suburb.toLowerCase() === that.getValue('suburb').value.toLowerCase() && geoObject.streetName === that.getValue('streetName').value && geoObject.streetNumber !== that.getValue('streetNumber').value) {
					that.handleOpenOverlay({
						openOverlay: true,
						message: replaceVar(mapTexts.streetNumberChanged, { old: that.getValue('streetNumber').value, new: geoObject.streetNumber }),
						lat: mapCenterLat,
						lng: mapCenterLng,
						geoObject,
						options: [
							{
								value: 1,
								label: replaceVar(mapTexts.streetNumberOptions.first, { new: geoObject.streetNumber }),
								style: { float: 'left', width: '100%' }
							},
							{
								value: 2,
								label: replaceVar(mapTexts.streetNumberOptions.second, { old: that.getValue('streetNumber').value }),
								style: { float: 'left', width: '100%' }
							},
						],
						icon: '/admin/assets/img/icons/icon-plan.svg',
						resetForm: makeId(),
						alertHeight: 410
					});
				} else {
					that.setGeoObject(geoObject, { lat: mapCenterLat, lng: mapCenterLng }, false);
				}
			}
		});
	}

	getGeoObject = (address_components: Array<Object>, suburb: string) => {
		const address = {};
		address_components.forEach((c) => {
			switch (c.types[0]) {
			case 'street_number':
				address.streetNumber = c.long_name;
				break;
			case 'route':
			case 'street_address':
				address.streetName = c.long_name;
				break;
			case 'administrative_area_level_3':
				address.administrative_area_level_3 = c.long_name;
				break;
			case 'locality':
				address.locality = c.long_name;
				break;
			case 'postal_code':
				address.postCode = c.long_name;
				break;
			default:
				break;
			}
		});
		address.suburb = address.locality && suburb !== address.administrative_area_level_3 ? address.locality : address.administrative_area_level_3;
		return address;
	}

	/* eslint-disable */
	changeField(e: Object) {
		const oldVal = this.getValue(e.target.name).value;
		const stateObject = this.stateObject(e);
		const newVal = this.getValue(e.target.name).value;
		//const thereIsMap = Object.getOwnPropertyNames(map).length !== 0;

		if (this.state.fieldChanged === 'streetNumber') {
			this.geocodeByCity();
		}
		//if (!thereIsMap || this.state.fieldChanged === 'streetNumber') {
			// do something?
			// I don't know...
		//}
		this.setState({ fieldChanged: null });
	}
	/* eslint-enable */

	async onChange(e: Object) {
		const oldTownId = this.getValue('townId').value;
		const oldVal = this.getValue(e.target.name).value;
		const stateObject = this.stateObject(e);
		const { townZones } = this.state;
		const newTownId = this.getValue('townId').value;
		const newVal = this.getValue(e.target.name).value;
		const { citiesArray } = this.props;
		if (oldVal !== newVal) this.setState({ fieldChanged: e.target.name });

		if (e.target.name === 'suburb' && this.getValue('suburb').value !== '') {
			if (oldTownId !== newTownId) {
				this.setGeoObject(this.state.geoObject, { lat: e.target.value.position.lat, lng: e.target.value.position.lng }, false);
				this.setState({ townZones:	e.target.value.townZones });
				const bounds = await this.getBounds();
				autocomplete.setBounds(bounds);
			}
		} else {
			this.setState({ value: stateObject });
		}
		if (this.getValue('streetName').value === '') {
			this.setState({ streetNameFilled: false });

			window.setTimeout(() => {
				document.getElementById('streetName').focus();
			}, 50);
		}
		if (citiesArray.indexOf(this.getValue('suburb').value) === -1) this.setState({ cityInArray: false });
	}

	stateObject = (e: Object) => {
		let blankAddress = false;
		if (e.target.name === 'suburb' && e.target.value.suburb !== this.getValue('suburb').value) {
			blankAddress = true;
		} else if (e.target.name === 'streetName' && e.target.value === '') {
			blankAddress = true;
		}
		const returnVal = [];
		this.state.value.forEach((v) => {
			if (v.name === e.target.name && e.target.name !== 'suburb' && v.name !== 'townId' && e.target.name !== 'townZone' && e.target.name !== 'coordinateEnabled' &&	e.target.name !== 'isAddressVisible') {
				v.value = e.target.value;
				returnVal.push(v);
			} else if (v.name === e.target.name && e.target.name === 'suburb') {
				v.value = e.target.value.suburb;
				returnVal.push(v);
			} else if (v.name === 'townId' && e.target.name === 'suburb') {
				v.value = e.target.value.townId;
				returnVal.push(v);
			} else if (v.name === e.target.name && e.target.name === 'townZone') {
				v.value.townZoneId = e.target.value;
				returnVal.push(v);
			} else if (v.name === e.target.name && e.target.name === 'coordinateEnabled') {
				v.value = e.target.checked;
				returnVal.push(v);
			} else if (v.name === e.target.name && e.target.name === 'isAddressVisible') {
				v.value = e.target.checked;
				returnVal.push(v);
			} else if (blankAddress && (v.name === 'streetName' || v.name === 'streetNumber' || v.name === 'postCode')) {
				v.value = '';
				returnVal.push(v);
			} else {
				returnVal.push(v);
			}
		});

		return returnVal;
	}

	getValue(name: string, value: ?Object) {
		if (value) return value.filter(o => o.name === name)[0];
		return this.state.value.filter(o => o.name === name)[0];
	}

	closeOverlay() {
		this.handleOpenOverlay({
			openOverlay: false,
			message: null,
			lat: null,
			lng: null,
			geoObject: null,
			hideConfirm: null,
			options: [],
			distance: null,
			updateMarker: Math.random()
		});
	}

	confirmPosition(value: number) {
		const { geoObject } = this.state;
		const { listingId } = this.props;
		const localObject = localStorage.getItem(`geoItem_${listingId}`);

		if (value === 1) {
			if (localObject !== null) localStorage.removeItem(`geoItem_${listingId}`);
			this.setGeoObject(geoObject, { lat: this.state.lat, lng: this.state.lng }, false);
		} else if (value === 2) {
			if (localObject === null) {
				localStorage.setItem(`geoItem_${listingId}`,
					JSON.stringify({
						streetName: this.getValue('streetName').value,
						streetNumber: this.getValue('streetNumber').value,
						lat: this.getValue('lat').value,
						lng: this.getValue('lng').value
					})
				);
			}

			geoObject.streetName = this.getValue('streetName').value;
			geoObject.streetNumber = this.getValue('streetNumber').value;
			this.setGeoObject(geoObject, { lat: this.state.lat, lng: this.state.lng }, false);
		} else if (value === 3) {
			if (localObject !== null) localStorage.removeItem(`geoItem_${listingId}`);
			geoObject.streetName = this.getValue('streetName').value;
			geoObject.streetNumber = this.getValue('streetNumber').value;
			this.setGeoObject(geoObject, { lat: this.getValue('lat').value, lng: this.getValue('lng').value }, false);
		}

		window.setTimeout(() => {
			this.handleOpenOverlay({
				openOverlay: false,
				message: null,
				lat: null,
				lng: null,
				geoObject: null,
				hideConfirm: null,
				options: [],
				updateMarker: Math.random(),
				distance: null
			});
		}, 50);

		const { citiesArray } = this.props;
		if (citiesArray.indexOf(this.getValue('suburb').value) !== -1) this.setZone(this.state.lat, this.state.lng);
	}

	handleOpenOverlay(state: Object) {
		if (state.openOverlay) {
			// flow-disable-next-line
			document.body.style.overflow = 'hidden';
		} else {
			// flow-disable-next-line
			document.body.style.overflow = 'unset';
		}
		// flow-disable-next-line
		if (document.activeElement) {
			// flow-disable-next-line
			document.activeElement.blur();
		}
		this.setState(state);
	}

	mapInfo() {
		this.setState({ mapInfo: true });
	}

	closeActionOverlay() {
		this.setState({ mapInfo: false });
	}

	render() {
		const { className, name, actions, jwt, isValid, mapTexts, citiesArray, localMode, viewport, tileLayer, listingId, utils, utils: { getViewport } } = this.props;
		const { openOverlay, message, hideConfirm, hideCancel, options, resetForm, icon, alertHeight, townZones, streetNameFilled, cityInArray, updateMarker, mapInfo } = this.state;

		return el('div', { id: name, className: `custom-map container-field ${className}` },
			el('div', { className: 'custom-map-fields' },
				el('div', { className: 'custom-map-field geolocation-field' },
					el('label', {
						htmlFor: 'suburb',
						style: !isValid && this.getValue('suburb').value === '' ? { color: '#e4002b' } : {},
						className: 'custom-map-label noselect'
					}, 'Comune*'),
					el('div', {
						className: 'autosuggest_cont',
						style: { marginTop: 24, borderRadius: 2, border: !isValid && this.getValue('suburb').value === '' ? '1px solid #e4002b' : '1px solid #d8d8df' }
					},
						el(TownAutoSuggest, {
							actions,
							jwt,
							townId: this.getValue('townId').value,
							description: this.getValue('suburb').value,
							onChange: (e) => { this.onChange(e); },
							citiesArray
						})
					),
					el('span', { className: 'validation-error' },
						!isValid && this.getValue('suburb').value === '' ? 'Campo obbligatorio' : ''
					)
				),
				el('div', {
					className: 'custom-map-field geolocation-field',
					style: this.getValue('suburb').value === '' ? { display: 'none' } : {}
				},
					el('label', {
						htmlFor: 'streetName',
						style: !isValid && this.getValue('streetName').value === '' ? { color: '#e4002b' } : {},
						className: 'custom-map-label noselect'
					}, 'Indirizzo*'),
					el('div', {},
						el('input', {
							type: 'text',
							placeholder: 'Indirizzo',
							id: 'streetName',
							name: 'streetName',
							style: { width: '100%', float: 'left', border: !isValid && this.getValue('streetName').value === '' ? '1px solid #e4002b' : '1px solid #d8d8df' },
							value: this.getValue('streetName').value,
							onChange: (e) => { this.onChange(e); },
							onBlur: (e) => { this.changeField(e); }
						})
					),
					el('span', { className: 'validation-error' },
						!isValid && this.getValue('streetName').value === '' ? 'Campo obbligatorio' : ''
					)
				),
				el('div', { style: { clear: 'both',	} }),
				!streetNameFilled ? null :
					el('div', { className: 'custom-map-field-s geolocation-field' },
						el('label', {
							htmlFor: 'streetNumber',
							style: { width: '100%', float: 'left', whiteSpace: 'nowrap' },
							className: 'custom-map-label noselect'
						}, 'N° civico'),
						el('div', {},
							el('input', {
								type: 'text',
								placeholder: 'N° civico',
								id: 'streetNumber',
								name: 'streetNumber',
								style: { width: '100%', float: 'left' },
								value: this.getValue('streetNumber').value,
								onChange: (e) => { this.onChange(e); },
								onBlur: (e) => { this.changeField(e); }
							})
						),
						el('span', { className: 'validation-error' }, ' ')
					),
				!streetNameFilled ? null :
					el('div', { className: 'custom-map-field-s geolocation-field' },
						el('label', {
							htmlFor: 'postCode',
							style: { width: '100%', float: 'left' },
							className: 'custom-map-label noselect'
						}, 'CAP'),
						el('div', {},
							el('input', {
								type: 'text',
								placeholder: 'CAP',
								name: 'postCode',
								style: { width: '100%', float: 'left' },
								value: this.getValue('postCode').value,
								onChange: (e) => { this.onChange(e); },
								onBlur: (e) => { this.changeField(e); }
							})
						),
						el('span', { className: 'validation-error' }, ' ')
					),
				this.state.townZones && this.state.townZones.length > 0 && streetNameFilled ?
					el('div', { className: 'custom-map-field geolocation-field' },
						el('label', { style: { width: '100%', float: 'left' }, className: 'custom-map-label noselect' },
							'Zona'
						),
						el('div', { className: cityInArray ? 'select-style select-style-disabled' : 'select-style', style: { width: '100%' } },
							el('select', {
								name: 'townZone',
								id: 'townZone',
								value: this.getValue('townZone').value.townZoneId != null ? this.getValue('townZone').value.townZoneId : ' ',
								onChange: (e) => { this.onChange(e); },
								disabled: cityInArray
							},
								el('option', { key: 'cs_0', value: ' ' }, 'Seleziona...'),
								townZones.map((item, i) => {
									switch (item.townZoneId) {
									case '0':
										return el('option', { key: `cs_${i}`, value: '0', className: 'disabled-option' }, item.description.replace(/\/ /g, ' / '));
									default:
										return el('option', { key: `cs_${i}`, value: item.townZoneId }, item.description.replace(/\/ /g, ' / '));
									}
								})
							),
							el('svg', { width: 24, height: 24, viewBox: '0 0 24 24' },
								el('rect', { fill: '#fff', width: 24, height: 24, x: 0, y: 0 }),
								el('path', { fill: '#949da2', d: 'M7.41 8.59L12 13.17l4.59-4.58L18 10l-6 6-6-6 1.41-1.41z' })
							)
						),
						el('span', { className: 'validation-error' }, ' ')
					)
					: null,
				el('div', { style: { clear: 'both', marginTop: 10 } }),
				!streetNameFilled ? null :
					el('input', {
					type: 'checkbox',
					name: 'coordinateEnabled',
					id: 'coordinateEnabled',
					checked: this.getValue('coordinateEnabled').value,
					onChange: (e) => { this.onChange(e); },
				}),
				!streetNameFilled ? null :
					el('label', { htmlFor: 'coordinateEnabled' },
					el('svg', { width: 24, height: 24, viewBox: '0 0 24 24' },
						el('path', {
							className: this.getValue('coordinateEnabled').value === true ? 'int' : '',
							d: this.getValue('coordinateEnabled').value === true ? 'M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.11 0 2-.9 2-2V5c0-1.1-.89-2-2-2zm-9 14l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z' : 'M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z'
						}),
						this.getValue('coordinateEnabled').value === true ? null :
						el('rect', { className: 'ext', width: 16, height: 16, x: 4, y: 4, style: { borderRadius: 2 } })
					),
					el('div', null, mapTexts.coordinateEnabledLabel)),
				el('div', { style: { clear: 'both', marginTop: 10 } }),
				!streetNameFilled ? null :
					el('input', {
					type: 'checkbox',
					name: 'isAddressVisible',
					id: 'isAddressVisible',
					checked: this.getValue('isAddressVisible').value,
					onChange: e => { this.onChange(e); },
				}),
				!streetNameFilled ? null :
					el('label', { htmlFor: 'isAddressVisible' },
					el('svg', { width: 24, height: 24, viewBox: '0 0 24 24' },
						el('path', {
							className: this.getValue('isAddressVisible').value === true ? 'int' : '',
							d: this.getValue('isAddressVisible').value === true ? 'M19 3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.11 0 2-.9 2-2V5c0-1.1-.89-2-2-2zm-9 14l-5-5 1.41-1.41L10 14.17l7.59-7.59L19 8l-9 9z' : 'M19 5v14H5V5h14m0-2H5c-1.1 0-2 .9-2 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2z'
						}), this.getValue('isAddressVisible').value === true ? null :
						el('rect', { className: 'ext', width: 16, height: 16, x: 4, y: 4, style: { borderRadius: 2 } })
					),
					el('div', null, mapTexts.isAddressVisibleLabel))
			),
			!streetNameFilled ? null :
				el(LeafLetComp, {
					lat: this.getValue('lat').value,
					lng: this.getValue('lng').value,
					updateMarker,
					localMode,
					viewport,
					tileLayer,
					dragEnd: e => this.dragEnd(e)
				}),
			!streetNameFilled ? null :
				el('div', { className: 'map-info', onClick: () => this.mapInfo() }, 'Info mappa'),
			mapInfo ?
				el(Alert, {
					closeActionOverlay: () => this.closeActionOverlay(),
					resetAction: () => this.closeActionOverlay(),
					viewport: viewport,
					actionTitle: 'Info mappa',
					actionTextBlack: el('div', { className: 'map-info-alert' },
						el('div', { className: 'map-info-alert-title' }, 'Istruzioni per l\'uso'),
						el('ul', {},
							el('li', {}, 'Inserisci il comune'),
							el('li', {}, 'Inserisci l\'indirizzo'),
							el('li', {}, 'Clicca su "Modifica posizione" se l\'indirizzo non corrisponde',
								el('ul', {},
									el('li', {}, 'Sposta il marcatore e posizionalo nel punto desiderato'),
									el('li', {}, 'Oppure clicca sul punto desiderato per posizionare il marcatore'),
								)
							),
						)
					),
					icon: '/admin/assets/img/icons/icon-plan.svg',
					alertHeight: 400,
					hideActions: true
				})
				: null,
			openOverlay ?
				el('div', { className: 'overlay-back alert' },
					el(ClickOutHandler, { onClickOut: () => { this.closeOverlay(); } },
						el('div', {
							className: 'overlay-cont',
							style: {
								marginTop: (parseInt(getViewport(viewport).height, 10) - 450) / 2,
							}
						},
							el('div', { className: 'alert-head' },
								el('div', { className: 'alert-close', onClick: () => { this.closeOverlay(); } }),
								el('div', { className: 'alert-title noselect' }, mapTexts.modalTitle)
							),
							el('div', { className: 'alert-icon' },
								el('img', { src: icon, alt: '' })
							),
							el('div', { className: 'overlay-data', style: { padding: '0px 15px 15px' } },
								el('div', { style: { width: '100%', display: 'inline-block' } },
									el('div', { className: 'alert-text-red noselect', style: { marginTop: 38 } }),
									el('div', {
										className: 'alert-text-black noselect',
										style: { textAlign: 'justify', marginBottom: 0 },
										dangerouslySetInnerHTML: { __html: message }
									}),
									el('div', { className: 'alert-actions' },
										el(MapOverlay, {
											hideCancel,
											hideConfirm,
											resetForm,
											options,
											viewport,
											utils,
											closeOverlay: () => this.closeOverlay(),
											confirmPosition: o => this.confirmPosition(o)
										})
									)
								)
							)
						)
					)
				)
				: null
		);
	}
}

export default CustomMap;
