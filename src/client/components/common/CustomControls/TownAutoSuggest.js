// @flow
import React, { createElement, Fragment } from 'react';
import PropTypes from 'prop-types';
import Autosuggest from 'react-autosuggest';

// flow-disable-next-line
import './TownAutoSuggest.scss';

const el = createElement;
const renderSuggestion = suggestion => (<span>{suggestion.description}</span>);

class TownAutoSuggest extends React.Component<any, any> {

	static defaultProps = {
		description: '',
		townId: '',
		actions: null,
		jwt: '',
		onChange: null,
		style: null
	};

	state = {
		value: this.props.description,
		suggestions: [],
		isLoading: false,
		townId: this.props.townId,
		townZones: []
	};

	loadSuggestions(value: string) {
		this.setState({
			isLoading: true,
		});

		if (value.length >= 3) {
			this.props.actions.searchTown({
				jwt: this.props.jwt,
				search: value,
			}).then((x) => {
				if (x.value.response.succeed && x.value.response.data.length > 0) {
					this.setState({
						isLoading: false,
						suggestions: x.value.response.data.sort((a, b) => {
							const textA = a.description.toUpperCase();
							const textB = b.description.toUpperCase();
							return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
						})
					});
				}
				return {};
			});
		}
	}

	onChange = (event: Object, { newValue }: { newValue: string }) => {
		this.setState({
			value: newValue
		});
	};

	onSuggestionsFetchRequested = ({ value }: { value: string }) => {
		this.loadSuggestions(value);
	};

	onSuggestionsClearRequested = () => {
		this.setState({
			suggestions: []
		});

		this.props.onChange({
			target: {
				name: 'suburb',
				value: {
					suburb: this.state.value,
					townId: this.state.townId,
					townZones: this.state.townZones,
					position: this.state.position
				}
			}
		});
	};

	getSuggestionValue = (suggestion: Object) => {
		this.setState({
			townId: suggestion.townId,
			townZones: suggestion.townZones,
			position: { lat: suggestion.latitude, lng: suggestion.longitude }
		});
		return suggestion.description;
	}

	render() {
		const { value, suggestions, isLoading } = this.state;
		const inputProps = {
			placeholder: 'Comune',
			value,
			onChange: this.onChange
		};

		return el(Fragment, {},
			isLoading ? el('svg', { width: 24, height: 24, viewBox: '0 0 24 24', className: 'status spin' },
				el('circle', { cx: 12, cy: 12, fill: 'none', stroke: '#d8d8df', strokeWidth: 2, r: 11, strokeDasharray: '55,20' })
			) : null,
			el(Autosuggest, {
				suggestions,
				onSuggestionsFetchRequested: this.onSuggestionsFetchRequested,
				onSuggestionsClearRequested: this.onSuggestionsClearRequested,
				getSuggestionValue: this.getSuggestionValue,
				renderSuggestion,
				highlightFirstSuggestion: true,
				inputProps
			})
		);
	}
}

TownAutoSuggest.propTypes = {
	description: PropTypes.string,
	townId: PropTypes.string,
	actions: PropTypes.instanceOf(Object),
	jwt: PropTypes.string,
	onChange: PropTypes.func,
	style: PropTypes.instanceOf(Object)
};

TownAutoSuggest.defaultProps = {
	description: '',
	townId: '',
	actions: null,
	jwt: '',
	onChange: null,
	style: null
};

export default TownAutoSuggest;
