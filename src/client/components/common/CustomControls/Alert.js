// @flow
import React from 'react';
import ClickOutHandler from 'react-onclickout';

// flow-disable-next-line
import './Alert.scss';

const el = React.createElement;

export default class Alert extends React.Component<any, any> {

	componentDidMount() {
		/* flow-disable-next-line */
		document.body.style.overflow = 'hidden';
	}

	componentWillUnmount() {
		/* flow-disable-next-line */
		document.body.style.overflow = 'unset';
	}

	getViewport = (viewport: Object) => {
		if (viewport.height === '' && document && document.documentElement) return { width: document.documentElement.clientWidth, height: document.documentElement.clientHeight };
		return viewport;
	}

	render() {
		const {
			closeActionOverlay, actionTitle, resetAction,
			deleteError, confirmAction, whichAction, actionTextRed,
			actionTextBlack, successMessage, icon, loadSpinner, viewport,
			alertHeight, hideConfirm, hideActions
		} = this.props;
		const sendButtonValue = deleteError === null ? 'Conferma' : deleteError === true ? 'Errore nel salvataggio dei dati' : successMessage;
		const isSent = deleteError === null && loadSpinner === true;

		return el('div', { className: 'overlay-back alert' },
			el(ClickOutHandler, { onClickOut: () => { closeActionOverlay(); } },
				el('div', {
					className: 'overlay-cont',
					style: {
						marginTop: (parseInt(this.getViewport(viewport).height, 10) - alertHeight) / 2,
						height: alertHeight,
					}
				},
				el('div', { className: 'alert-head' },
					el('div', { className: 'alert-close', onClick: () => { closeActionOverlay(); } }),
					el('div', { className: 'alert-title noselect' }, actionTitle)),
				el('div', { className: 'alert-icon' }, el('img', { src: icon, alt: '' })),
				el('div', { className: 'overlay-data' },
					el(ClickOutHandler, { onClickOut: () => { resetAction(); } },
						el('div', { style: { width: '100%', display: 'inline-block' } },
							actionTextRed !== '' && actionTextRed !== null && actionTextRed !== undefined ? el('div', { className: 'alert-text-red noselect' }, actionTextRed) : null,
							actionTextBlack !== '' && actionTextBlack !== null && actionTextBlack !== undefined ? el('div', { className: 'alert-text-black noselect' }, actionTextBlack) : null,
							hideActions ? null :
								el('div', { className: 'alert-actions' },
									el('button', {
										style: { paddingLeft: 40, paddingRight: 40, display: hideConfirm ? 'none' : 'block' },
										className: `btn ${deleteError === false ? 'btn-succeed' : 'btn-red'}`,
										onClick: () => { confirmAction(whichAction); }
									},
									el('svg', { width: 24, height: 24, viewBox: '0 0 24 24', className: isSent !== false ? 'spin' : '' },
										isSent === true ?
											el('circle', { cx: 12, cy: 12, fill: 'none', stroke: '#fff', strokeWidth: 2, r: 11, strokeDasharray: '55,20' })
											:
											deleteError === false ?
												el('polyline', { fill: 'none', points: '4,12 9,18 21,6', style: { fill: 'none', stroke: '#fff', strokeWidth: 2 } })
												:
												deleteError === true ?
													el('path', { fill: '#fff', d: 'M1 21h22L12 2 1 21zm12-3h-2v-2h2v2zm0-4h-2v-4h2v4z' })
													:
													null),
									sendButtonValue),
									deleteError === null && isSent === false ?
										el('button', { style: { border: 'none!important' }, className: 'btn btn-white', onClick: () => { closeActionOverlay(); } }, 'Annulla')
										: null)))))));
	}
}
