// @flow
import React from 'react';
// flow-disable-next-line
import './ThankYou.scss';

const el = React.createElement;

class ThankYou extends React.Component<any, any> {

	render() {
		const { dataObject } = this.props;
		console.log(dataObject);

		return el(React.Fragment, {}, el('div', { className: 'thanks' }, 'ThankYou'));
	}
}

export default ThankYou;
