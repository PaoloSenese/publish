// @flow
import React from 'react';
import sjwt from 'jsonwebtoken';
import Form from 'client-form';

import Image from '../components/common/image/Image';
import Info from './components/Info';
// eslint-disable-next-line
import { parkingSpaces, gardenTypes, airConditioned, energy, tipologies, floors, heatingtypes, categories, occupationState, conditions, furnished } from '../../common/helpers/var';
import Alert from '../components/common/CustomControls/Alert';
import ItemViews from './components/ItemViews';

const el = React.createElement;

class ListingItem extends React.PureComponent<any, any> {

	state = {
		handleAction: false,
		deleteError: null,
		loadSpinner: false,
		alertHeight: 365,
		icon: '',
		whichAction: '',
		actionTextBlack: null,
		actionTextRed: null,
		actionTitle: null,
		onOffId: this.props.utils.makeId(),
		listingEnabled: this.props.listing.listingEnabled,
		upgrade: false
	};

	getValue(name: string) {
		return this.props.listing.geolocation.filter(o => o.name === name)[0].value;
	}

	async preview() {
		/* eslint-disable */
		this.setState({
			loadSpinner: true,
		});
		const { jwt, actions } = this.props;
		const { user } = sjwt.decode(jwt);
		user.agencyLogoLarge = null;
		user.agencyLogo = null;
		user.agencyLogoSmall = null;

		const listing = await this.props.actions.getListingFull({
			id: this.props.listing.listingId,
			jwt,
			pathLength: 5,
			currentStep: 'step-3',
		}).then(e => e.value.response.data.dataObject);

		const {
			airConditionedId,
			bathNum,
			bodyCorporateFees,
			boxTypeId,
			buildingUnit,
			categoryTypeId,
			conditionId,
			contractTypeId,
			depth,
			description,
			energyEfficiencyRatingId,
			energyEfficiencyValue,
			energyEfficiencyValueRenew,
			epeEnum,
			epiEnum,
			floorNum,
			furnishedId,
			gardenTypeId,
			geolocation,
			hasAttic,
			hasBalcony,
			hasCellar,
			hasConcierge,
			hasElevator,
			hasPurchaseOption,
			hasSwimmingPool,
			hasTerrace,
			heatingTypeId,
			hidePrice,
			insertDt,
			isCubeMeters,
			isQuiteZeroEnergyeState,
			lastModifiedDt,
			levelNum,
			listingEnabled,
			listingId,
			media,
			mqBox,
			mqGarden,
			mqBalcony,
			mqTerrace,
			occupationStateId,
			parkingNumber,
			parkingSpaceId,
			price,
			priceMax,
			propertyTypeId,
			publisherListingId,
			roomNum,
			surface,
			yearOfBuilding,
		} = listing;

		const town = await actions.searchTown({
			search: geolocation[7].value,
			match: true,
		}).then(x => x.value.response.data[0]);

		const { province } = town;

		const mediaArray = [];
		media.map((item) => {
			mediaArray.push({
				fileType: item.imageType === 'F' ? 'photo' : 'floorplan',
				order: item.order,
				isMainPhoto: item.isPrincipal,
				file: `/${item.url}`
			});
		});

		const listingObject = {
			ac_desc: airConditionedId !== 0 ? airConditioned.filter(o => o.value === airConditionedId)[0].label : 'non indicato',
			availability_status: occupationStateId !== -1 ? occupationState.filter(o => o.value === occupationStateId)[0].label : 'non indicato',
			balcony_mq: mqBalcony,
			bathrooms: bathNum,
			body_corporate_fee_frequency: 'Monthly',
			body_corporate_fees: bodyCorporateFees,
			building_condition: conditionId !== -1 ? conditions.filter(o => o.value === conditionId)[0].label : 'non indicato',
			category: categoryTypeId === 1 ? 'Residential' : categoryTypeId === 2 ? 'Commercial' : 'Vacancies',
			category_type_id: categoryTypeId,
			channel: contractTypeId === 1 ? 'Rent' : 'Buy',
			classification_enum: 1,
			coordinate_enabled: true,
			coordinate_precision: 1,
			country_code: 'IT',
			custom_listings_appearance_enum: null,
			de_description: description.filter(o => o.name === 'listingDescDe')[0].value,
			depth_product_enum: depth.description.toUpperCase(),
			depth_product_value: 2000,
			en_description: description.filter(o => o.name === 'listingDescEn')[0].value,
			energy_classification_label: null,
			energy_efficiency_not_renew_value: energyEfficiencyValue,
			energy_efficiency_rating_desc: energy.filter(o => o.value === energyEfficiencyRatingId)[0].label,
			energy_efficiency_renew_value: energyEfficiencyValueRenew,
			energy_efficiency_unit: isCubeMeters ? 'kWh/m3a' : 'KWh/m2a',
			energy_efficiency_value: energyEfficiencyValue,
			energyepe: epeEnum,
			energyepi: epiEnum,
			es_description: description.filter(o => o.name === 'listingDescEs')[0].value,
			fr_description: description.filter(o => o.name === 'listingDescFr')[0].value,
			furnished_desc: null,//furnished.filter(o => o.name === furnishedId)[0].label,
			garage_count: boxTypeId,
			garage_size: mqBox,
			garden_size: mqGarden,
			garden_type: gardenTypeId !== -1 ? gardenTypes.filter(o => o.value === gardenTypeId)[0].label : 'non indicato',
			has_attic: hasAttic,
			has_cellar: hasCellar,
			has_concierge: hasConcierge,
			has_elevator: hasElevator,
			has_purchase_option: hasPurchaseOption,
			has_swimming_pool: hasSwimmingPool,
			heating_type: heatingtypes.filter(o => o.value === heatingTypeId)[0].label,
			house_size: surface,
			is_geo_verified: null,
			is_new_buinding: null,
			is_quite_zero_energy_estate: isQuiteZeroEnergyeState,
			it_description: description.filter(o => o.name === 'listingDescIt')[0].value,
			latitude: geolocation.latitude,
			level: levelNum,
			level_description: floorNum ? floors.filter(o => o.value === floorNum)[0].label : null,
			listing_id: listingId,
			listing_status: true,
			longitude: geolocation.longitude,
			map_center_latitude: geolocation.latitude,
			map_center_longitude: geolocation.longitude,
			map_zoom_level: 13,
			media: {
				files: mediaArray
			},
			monthly_rent: bodyCorporateFees,
			number_of_units: buildingUnit,
			open_parking_spaces_count: parkingNumber,
			parking_space_desc: parkingSpaceId !== -1 ? parkingSpaces.filter(o => o.value === parkingSpaceId)[0].label : 'non indicato',
			parking_space_number: parkingNumber,
			post_code: geolocation.filter(o => o.name === 'postCode')[0].value,
			price: price !== null ? parseInt(price.replace(/\./g, ''), 10) : null,
			price_max: priceMax !== null ? parseInt(priceMax.replace(/\./g, ''), 10) : null,
			property_type: tipologies.filter(o => o.value === propertyTypeId)[0].label,
			province_id: `IT-${province.region.description.toUpperCase().substring(0, 3)}-${province.provinceCode}`,
			province_short_code: province.provinceCode,
			publisher_category: 'Private',
			publisher_country_code: 'IT',
			publisher_is_active: true,
			publisher_logo_large: user.agencyLogoLarge ? user.agencyLogoLarge : '/agencylogo/private/small/placeholder.gif',
			publisher_logo_medium: user.agencyLogo ? user.agencyLogo : '/agencylogo/private/small/placeholder.gif',
			publisher_logo_small: user.agencyLogoSmall ? user.agencyLogoSmall : '/agencylogo/private/small/placeholder.gif',
			publisher_name: '',
			publisher_phone: user.phone,
			publisher_placeholder_logo_small: '/agencylogo/agency/small/placeholder.gif',
			region_id: `IT-${province.region.description.toUpperCase().substring(0, 3)}`,
			region_name: province.region.description.toUpperCase(),
			rent_availability: '',
			rooms: roomNum,
			show_address: !geolocation.filter(o => o.name === 'isAddressVisible')[0].value,
			show_price: !hidePrice,
			show_street_view: true,
			street_name: geolocation.filter(o => o.name === 'streetName')[0].value,
			street_number: geolocation.filter(o => o.name === 'streetNumber')[0].value,
			suitable_for_holiday: false,
			terrace_mq: mqTerrace,
			title: `${tipologies.filter(o => o.value === propertyTypeId)[0].label} in ${categories.filter(o => o.value === categoryTypeId)[0].label}`,
			town_id: `IT-${province.region.description.toUpperCase().substring(0, 3)}-${town.townId}`,
			town_name: town.description,
			weekly_rent: 43847,
			year_of_building: yearOfBuilding
		};

		const newObject = Object.keys(listingObject).reduce((acc, key) => {
			const accX = acc;
			if (listingObject[key] !== undefined && listingObject[key] !== null && listingObject[key] !== '') accX[key] = listingObject[key];
			return accX;
		}, {});

		console.log('newObject', newObject);
		this.props.actions.preview({ jwt: this.props.jwt, listingObject: { listing: newObject } }).then((x) => {
			this.setState({
				loadSpinner: false,
			});
			console.log(x.value.response.data);
			// flow-disable-next-line
			document.getElementById('data').value = JSON.stringify(x.value.response.data);
			// flow-disable-next-line
			document.getElementById('myForm').submit();
		});
		/* eslint-enable */
	}

	openActionOverlay(which: string) {
		// flow-disable-next-line
		document.body.style.overflow = 'hidden';
		this.setState({
			handleAction: true,
			whichAction: which,
			actionTitle: which === 'delete' ? 'Confermi l\'eliminazione?' : 'Confermi l\'archiviazione?',
			actionTextRed: 'Attenzione! L\'operazione è irreversibile.',
			actionTextBlack: which === 'delete' ?
				<span>
					Cliccando su&nbsp;
					<strong>conferma</strong>
					, il progetto verrà eliminato
					<br />
					e non sarà più possibile recuperarlo.
				</span> :
				<span>
					Cliccando su&nbsp;
					<strong>conferma</strong>
					, il progetto verrà archiviato
					<br />
					e non sarà più possibile ripristinarlo.
					<br />
				</span>,
			icon: which === 'delete' ? '/admin/assets/img/icons/icon-bin-big.svg' : '/admin/assets/img/icons/group-7.svg',
			deleteError: null,
			alertHeight: 365,
		});
	}

	closeActionOverlay() {
		// flow-disable-next-line
		document.body.style.overflow = 'unset';
		if (this.state.handleAction === true) {
			this.setState({
				handleAction: false,
				whichAction: null,
				actionTitle: null,
				actionTextRed: null,
				actionTextBlack: null,
				deleteError: null
			});
		} else if (this.state.upgrade === true) {
			this.setState({
				upgrade: false
			});
		}
	}

	resetAction() {
		if (this.state.deleteError !== null) {
			// flow-disable-next-line
			document.body.style.overflow = 'unset';
			this.setState({
				handleAction: false,
				whichAction: null,
				actionTitle: null,
				actionTextRed: null,
				actionTextBlack: null,
				deleteError: null,
			});
		}
	}

	confirmAction(whichAction: string) {
		this.setState({
			loadSpinner: true
		});

		try {
			this.props.actions.changeStatus({
				id: this.props.listing.listingId,
				action: whichAction,
				jwt: this.props.jwt,
			})
				.then((x) => {
					console.log(x);
					if (!x.value.response.succeed) {
						this.setState({
							loadSpinner: false,
							deleteError: true,
							listingEnabled: whichAction === 'enable',
						});
					} else {
						this.setState({
							loadSpinner: false,
							deleteError: false,
							listingEnabled: whichAction === 'enable',
						});
					}
					location.href = this.props.history.location.pathname;
				}).catch((x) => {
					console.log(x);
				});
		} catch (e) {
			console.log(e);
		} finally {

		}

	}

	handleSubmit(ev: Object) {
		ev.preventDefault();
		this.preview();
	}

	upgrade() {
		// flow-disable-next-line
		document.body.style.overflow = 'unset';
		this.setState({
			upgrade: true
		});
	}

	render() {
		/* eslint-disable-next-line */
		const { listing, imageserver, viewport, actions, jwt, onList, admin, previewUrl, depthsObject, radioContainers, utils: { formatDateIt } } = this.props;
		const {
			handleAction, deleteError,
			actionTitle, actionTextRed, actionTextBlack, whichAction,
			loadSpinner, icon, alertHeight, onOffId, listingEnabled, upgrade
		} = this.state;
		const {
			isArchived, listingId, picture,
			depth, price, insertDt, totalLeads,
			propertyTypeId, contractTypeId, geolocation, roomNum, surface, endDt
		} = listing;

		const productSpec = radioContainers.radioDepth.filter(o => o.productType === depth.productTypeId)[0];

		return (
			<div className="project-item">
				<div className="item-top">
					{ isArchived === 'true' || (!depth || !depth.productTypeId) ?
						null
						:
						<div className="item-action item-action-no-clear">
							<Form {...{
								key: Math.random(),
								controls: [
									{
										control: 'check',
										name: `enable_${listingId}`,
										label: { text: listingEnabled ? 'Online' : 'Offline' },
										default: false,
										value: listingEnabled,
										style: { marginTop: 0, paddingTop: 0 },
										customSvg: {
											svgProps: { width: 36, height: 24, viewBox: '0 0 36 24' },
											forTrue: el(React.Fragment, {},
												el('rect', { width: 30, height: 16, rx: 8, ry: 8, x: 4, y: 4, style: { fill: 'rgb(0, 132, 255)', strokeWidth: 2, stroke: 'rgb(0, 132, 255)' } }),
												el('circle', { cx: 26, cy: 12, r: 7, style: { fill: 'rgb(255, 255, 255)' } })),
											forFalse: el(React.Fragment, {},
												el('rect', { width: 30, height: 16, rx: 8, ry: 8, x: 4, y: 4, style: { fill: 'rgb(216, 216, 223)', strokeWidth: 2, stroke: 'rgb(216, 216, 223)' } }),
												el('circle', { cx: 12, cy: 12, r: 7, style: { fill: 'rgb(255, 255, 255)' } }))
										}
									},
								],
								formStyle: { height: 22, minHeight: 22, maxHeight: 22 },
								formClassName: 'form-on-off',
								updateOnChange: () => {
									this.props.actions.changeStatus({
										id: listingId,
										action: listingEnabled ? 'disable' : 'enable',
										jwt: this.props.jwt,
									})
										.then((x) => {
											console.log(x);
											if (!x.value.response.succeed) {

											} else {

											}
										}).catch((x) => {
											console.log(x);
										});
								},
							}} />
						</div>
					}
					<div className="item-top-title">
						{tipologies.filter(o => o.value === propertyTypeId)[0].label} in {contractTypeId === 1 ? 'Affitto' : 'Vendita'} a {geolocation.filter(o => o.name === 'suburb')[0].value}
					</div>
					<div style={{ float: 'right', position: 'relative' }}>
						<Info {...{
							text: el('ul', {},
								el('li', { onClick: () => { this.openActionOverlay('delete'); } }, 'Elimina'),
								el('li', { onClick: () => { this.openActionOverlay('archive'); } }, 'Archivia'),
								el('li', { onClick: () => { this.preview(); } }, 'Anteprima'))
						}} />
					</div>
					<a {...{ href: `/listing/${listingId}/step-1` }}>
						<div style={{ float: 'right' }}>
							<svg width="16" height="16" viewBox="0 0 16 16" style={{ marginTop: 8 }}>
								<path fill="#1A1F24" fillRule="evenodd" d="M1.492 12.394l8.296-8.316 2.129 2.131-8.306 8.3H1.492v-2.115zM12.061 1.798L14.2 3.934l-1.224 1.222-2.134-2.13 1.219-1.228zM.746 16h3.173c.2 0 .388-.08.527-.219l11.335-11.32a.744.744 0 0 0 0-1.053L12.588.218a.746.746 0 0 0-1.054 0L.219 11.56a.762.762 0 0 0-.219.527v3.169c0 .412.333.745.746.745z" />
							</svg>
						</div>
					</a>
				</div>
				<div className="item-container">
					<div className="item-image">
						<Image {...{ src: picture, alt: '', size: '222x133', imageserver, }} />
						<div className="item-head-title">
							{ depth.productTypeId ?
								<div className="item-head-depth noselect" style={{ marginLeft: 0, background: productSpec.backgroundColor }}>
									{depth.description}
								</div>
								:
								null
							}
							{ isArchived !== 'true' ?
								null
								:
								<div className="item-head-archive noselect">
									ARCHIVIATO
								</div>
							}
						</div>
						<div {...{ onClick: () => { this.preview(); }, className: loadSpinner ? 'item-image-preview item-image-preview-load' : 'item-image-preview' }}>
							{
								!loadSpinner ?
									'Vedi anteprima'
									:
									<svg width="48" height="48" viewBox="0 0 48 48" className="spin">
										<circle {...{ cx: 24, cy: 24, fill: 'none', stroke: '#fff', strokeWidth: 2, r: 22, strokeDasharray: '80,20' }} />
									</svg>
							}
						</div>
						<form id="myForm" method="POST" target="_blank" action={previewUrl}>
							<div className="btn btn-link-red">
								<input id="data" name="data" style={{ display: 'none' }} defaultValue="" />
							</div>
						</form>
					</div>
					<div className="item-characteristics">
						<div className="item-characteristics-first-row">
							<strong>
								{price}
								&nbsp;&euro;
							</strong>
						</div>
						<div className="item-characteristics-row">
							<strong>{roomNum}</strong> locali <strong>{surface}</strong> m²
						</div>
						<div className="item-characteristics-row">
							{`${this.getValue('streetName')}, ${this.getValue('streetNumber')} ${this.getValue('postCode')} ${this.getValue('suburb')}`}
						</div>
						<div className="item-characteristics-row">
							<strong>
								{listingId}
							</strong>
						</div>
						<div className="item-characteristics-row item-characteristics-gray">
							Inserito: {formatDateIt(insertDt)}&nbsp;
						</div>
						{ depth.productTypeId ?
							<div className="item-characteristics-row item-characteristics-gray">
								Scadenza: {formatDateIt(endDt)}&nbsp;
							</div> : null }
					</div>
					<div className="item-characteristics item-requests">
						<ItemViews {...{
							className: 'item-view noselect',
							label: 'Visite',
							value: totalLeads && totalLeads.listingViews ? totalLeads.listingViews.toString() : null,
							icon: '/admin/assets/img/icons/icon-preview.svg'
						}} />
						{ totalLeads.specificRequests > 0 ?
							<ItemViews {...{
								className: 'item-view link-red noselect',
								label: 'Richieste',
								value: totalLeads && totalLeads.specificRequests ? totalLeads.specificRequests.toString() : null,
								onClick: () => { this.getRequests(listingId); },
								icon: '/admin/assets/img/icons/envelope-regular.svg'
							}} />
							:
							<ItemViews {...{
								className: 'item-view noselect',
								label: 'Richieste',
								value: totalLeads && totalLeads.specificRequests ? totalLeads.specificRequests.toString() : null,
								icon: '/admin/assets/img/icons/envelope-regular.svg'
							}} />
						}
						<ItemViews {...{
							className: 'item-view noselect',
							label: 'Vis. Telefono',
							value: totalLeads && totalLeads.phoneReveals ? totalLeads.phoneReveals.toString() : null,
							icon: '/admin/assets/img/icons/baseline-phone-24px.svg'
						}} />
					</div>
					<div className="item-actions">
						{ isArchived === 'true' ?
							null
							:
							<React.Fragment>
								<div className="item-actions-text">Migliora la visibilità</div>
								<button {...{ onClick: () => { this.upgrade(); }, className: 'btn btn-red' }}>
									Più visibile
								</button>
								{ upgrade ?
									<Alert {...{
										closeActionOverlay: () => { this.closeActionOverlay(); },
										viewport,
										actionTitle: 'Aumenta la visibilità',
										resetAction: () => { this.resetAction(); },
										confirmAction: () => { this.confirmAction(whichAction, listingId); },
										whichAction: '',
										actionTextRed: '',
										actionTextBlack: '',
										icon: '',
										alertHeight,
										hideActions: true
									}} />
									: null
								}
							</React.Fragment>
						}
					</div>
				</div>
				{ handleAction ?
					<Alert {...{
						closeActionOverlay: () => { this.closeActionOverlay(); },
						viewport,
						actionTitle,
						resetAction: () => { this.resetAction(); },
						handleAction,
						deleteError,
						confirmAction: () => { this.confirmAction(whichAction); },
						whichAction,
						actionTextRed,
						actionTextBlack,
						successMessage: whichAction === 'delete' ? 'Progetto eliminato' : 'Progetto archiviato',
						icon,
						loadSpinner,
						alertHeight,
					}} />
					: null
				}
			</div>
		);
	}
}

export default ListingItem;
