// @flow
import React from 'react';
import PropTypes from 'prop-types';
import sjwt from 'jsonwebtoken';

import UiHandlerHOC from '../app/UiHandler';
import App from '../app/App';
import Header from '../components/common/header/Header';
import Listings from './Listings';
import Requests from '../requests/Requests';

// flow-disable-next-line
import './ListingsPage.scss';

const el = React.createElement;

class ListingsPageComponent extends React.Component<any, any> {

	state = {
		location: this.props.history.location,
		dataObject: this.props.listings.dataObject,
		paging: this.props.listings.paging,
		loadSpinner: null,
	};

	componentWillReceiveProps() {
		const pathArray = window.location.pathname.split('/');

		if (pathArray[1] !== 'thank-you' && pathArray[1] !== 'requests' && Number.isNaN(Number(pathArray[2]))) {
			const wrongPaging = this.props.listings.paging.total <= 10 && this.props.listings.paging.currentPage > 1;
			if (wrongPaging) window.location.href = `/listings/page-1${window.location.search}`;

			if (this.props.history.location.search !== this.state.location.search || this.props.history.location !== this.state.location) {

				this.setState({
					location: this.props.history.location,
					loadSpinner: true,
				});

				const { jwt } = this.props.config;
				const { imageserver } = this.props.listings;
				const { pathname, search } = this.props.history.location;
				const uri = `${pathname}${search}`;

				this.props.actions.fetchListings({ jwt, imageserver, uri })
					.then((e) => {
						this.setState({
							dataObject: e.value.dataObject,
							paging: e.value.paging,
							loadSpinner: false,
						});
					});
			}
		}
	}

	render() {
		const { actions, device, viewport, history, listings, config, utils } = this.props;
		const { jwt, imageserver } = config;
		const { listingLeads, depthsObject, depths } = listings;
		const { loadSpinner, paging, dataObject } = this.state;
		const { user } = sjwt.decode(jwt);
		const { pathname } = this.props.history.location;

		return el(App, Object.assign({}, this.props, { title: 'Publisher - Annunci' }),
			el(Header, {
				imageserver,
				user,
				device,
				adminUrl: config.admin,
				links: [
					{ name: 'Annunci', path: '/listings', sub: [] },
					{ name: 'Richieste', path: '/requests', sub: [] },
					{ name: 'Profilo', path: '/profile', sub: [] }
				]
			}),
			el('div', { className: pathname === '/thank-you' ? 'container container-thanks' : 'container' },
				el('div', { className: loadSpinner ? 'projects loader' : 'projects' },
					el('div', { className: 'main-title' }, pathname === '/listings' ? 'I miei annunci' : 'Le mie richieste'),
					pathname === '/listings' ? el('a', { href: '/listing/new' },
						el('div', { className: 'btn btn-red btn-new' })) : null,
					pathname === '/thank-you' ? null : el('div', { className: 'sub-nav' },
						el('a', { href: '/listings' },
							el('div', { className: history.location.pathname.indexOf('listings') !== -1 ? 'sub-nav-listings sub-nav-sel' : 'sub-nav-listings' },
								`Annunci (${paging ? paging.total : ''})`)),
						el('a', { href: '/requests' },
							el('div', { className: history.location.pathname.indexOf('requests') !== -1 ? 'sub-nav-requests sub-nav-sel' : 'sub-nav-requests' },
								'Richieste (0)'))),
					pathname === '/requests' ? el(Requests, { dataObject, actions, jwt, viewport, imageserver, utils })
						:
						pathname === '/profile' ? 'Profilo'
							:
							el(Listings, {
								dataObject,
								actions,
								jwt,
								device,
								viewport,
								history,
								config,
								depthsObject,
								paging,
								listingLeads,
								depths,
								loadSpinner,
								utils
							}))));
	}
}

ListingsPageComponent.propTypes = {
	device: PropTypes.string.isRequired,
	actions: PropTypes.instanceOf(Object).isRequired,
};

const ListingsPage = UiHandlerHOC(ListingsPageComponent, 'fetchListings');
export default ListingsPage;
