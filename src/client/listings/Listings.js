// @flow
import React, { createElement } from 'react';
import PropTypes from 'prop-types';

import Form from 'client-form';
import ListingItem from './ListingItem';
import Contacts from '../listing/components/Contacts';
import Pager from '../components/common/CustomControls/Pager';
import Alert from '../components/common/CustomControls/Alert';

// flow-disable-next-line
import './Listings.scss';

const el = createElement;

const handleScroll = () => {
	try {
		const elm = document.getElementById('this-sticky');
		if (elm != null && document != null && document.body != null) {
			if (elm.getBoundingClientRect().top <= 15) {
				elm.style.position = 'fixed';
				elm.style.top = '0px';
				elm.style.left = '0px';
				elm.style.borderRadius = '0px';
				elm.classList.add('box-shadow');
				// flow-disable-next-line
				document.getElementById('replacer').style.display = 'inline-block';
			}
			// flow-disable-next-line
			if (document.getElementById('replacer').getBoundingClientRect().top > 15) {
				elm.style.position = 'static';
				elm.style.top = 'auto';
				elm.style.borderRadius = '4px';
				elm.classList.remove('box-shadow');
				// flow-disable-next-line
				document.getElementById('replacer').style.display = 'none';
			}
		}
	} catch (e) {
		console.log(e);
	}
};

const breakUpQueryString = (list, queryObject) => {
	list.map((item) => {
		const name = item.split('=')[0];
		const value = item.split('=')[1];
		switch (name) {
		case 'active':
			queryObject.listingEnabled = value;
			break;
		case 'prod':
			queryObject.productTypeId = value;
			break;
		case 'id':
			queryObject.listingId = value;
			break;
		default:
			break;
		}
	});
	return queryObject;
};

const rewriteName = (name, i, value) => {
	switch (name) {
	case 'listingEnabled':
		return i === 0 ? `active=${value}` : `&active=${value}`;
	case 'productTypeId':
		return i === 0 ? `prod=${value}` : `&prod=${value}`;
	case 'listingId':
		return i === 0 ? `id=${value}` : `&id=${value}`;
	default:
		return '';
	}
};

const parseFormObject = (f: Object) => {
	let returnString = '';
	let intCount = 0;
	f.map((item) => {
		if (item.value !== false && item.value !== null && item.value !== undefined && item.value !== '') {
			returnString += rewriteName(item.name, intCount, item.value);
			intCount += 1;
		}
	});
	return returnString;
};

class Listings extends React.Component<any, any> {

	state = {
		randomKey: this.props.utils.makeId(),
		listingEnabled: undefined,
		productTypeId: '',
		errorOverlay: false,
		listingId: undefined,
	};

	componentWillReceiveProps(nextProps: Object) {
		const { location } = nextProps.history;
		const queryString = location.search.replace('?', '');
		let queryObject = {};

		if (queryString.indexOf('&') === -1 && location.search !== '') {
			queryObject = breakUpQueryString([queryString], queryObject);
		} else if (queryString.indexOf('&') !== -1 && location.search !== '') {
			queryObject = breakUpQueryString(queryString.split('&'), queryObject);
		}
		const { listingEnabled, productTypeId, listingId } = queryObject;

		this.setState({
			randomKey: this.props.utils.makeId(),
			listingEnabled,
			productTypeId: productTypeId !== undefined ? productTypeId : '',
			listingId
		});
	}

	componentDidMount() {
		// window.addEventListener('scroll', handleScroll);
	}

	closeOverlay() {
		// flow-disable-next-line
		document.body.style.overflow = 'unset';
		this.setState({
			errorOverlay: false,
		});
	}

	resetButton() {
		if (this.state.editSucceed !== null) {
			this.setState({
				editSucceed: null,
			});
		}
	}

	changePage(page: string) {
		if (this.props.history.location.search === '') {
			this.props.history.push(`/listings/page-${page}`);
		} else {
			this.props.history.push(`/listings/page-${page}${this.props.history.location.search}`);
		}
	}

	render() {
		const { actions, jwt, dataObject, device, viewport, config, depthsObject, history, paging, depths, utils } = this.props;
		const { imageserver, radioContainers, contactsTexts, preview, previewUrl, admin } = config;
		const { listingEnabled, errorOverlay, listingId } = this.state;

		return el('div', { className: 'projects-list' },
			el('div', {},
				/* eslint-disable-next-line */
				false ?
					el('div', { id: 'this-sticky', className: 'this-sticky this-sticky-projects' },
						el('div', { className: 'this-sticky-cont' },
							el(Form, {
								key: this.state.randomKey,
								controls: [
									{
										control: 'radio',
										name: 'productTypeId',
										label: { style: { display: 'none' } },
										options: [
											{ value: '', label: 'Tutti', style: { width: 60, float: 'left' } },
											{ value: 20, label: 'Gold', style: { width: 90, float: 'left' } },
											{ value: 17, label: 'Smart', style: { width: 65, float: 'left' } },
										],
										hideRadio: false,
										style: {
											minHeight: 'auto',
											marginLeft: 0,
											maxHeight: 27
										},
										value: this.state.productTypeId ? this.state.productTypeId : '',
										className: 'radio-as-check',
										uncheck: true,
										default: ''
									},
									{
										control: 'label',
										content: '|',
										className: 'separator'
									},
									{
										control: 'check',
										name: 'listingEnabled',
										label: { text: 'Solo pubblicati' },
										hideCheck: true,
										style: {
											minHeight: 'auto',
											width: 130,
											marginRight: 0,
											paddingTop: 0
										},
										value: listingEnabled
									},
									{
										control: 'text',
										name: 'listingId',
										placeholder: 'Codice',
										label: { style: { display: 'none' } },
										value: listingId,
										onlyNumber: true,
										style: { minHeight: 30 }
									}
								],
								updateOnChange: (f) => {
									console.log(f);
									this.props.history.push(`/listings/page-1?${parseFormObject(f)}`);
								},
								formStyle: { float: 'left', marginTop: 5, paddingLeft: 0 },
							})
						)
					)
					: null,
				el('div', { id: 'replacer', style: { width: '100%', height: 60, background: 'transparent', marginBottom: 0, display: 'none' } }),
				el('div', { className: 'project-list-container' },
					el('ul', { className: this.state.loadSpinner ? 'loader' : '' },
						!dataObject || !Array.isArray(dataObject) || dataObject.length === 0 ?
							el('div', { style: { width: '100%', height: 150, textAlign: 'center', lineHeight: '150px' } }, 'Nessun progetto')
							:
							dataObject.map(item => el('li', { key: `pik_${item.listingId}` },
									el(ListingItem, {
										jwt,
										listing: item,
										imageserver,
										actions,
										onList: true,
										device,
										viewport,
										admin,
										preview,
										previewUrl,
										depthsObject,
										radioContainers,
										history,
										utils
									})
								)
							))
				),
				el('div', { style: { padding: 11, height: 52 } },
					el(Pager, {
						currentPage: paging ? parseInt(paging.currentPage, 10) : 1,
						total: paging ? paging.total : null,
						itemPerPage: 10,
						onClick: (e) => { this.changePage(e); },
					})
				)
			),

			errorOverlay ? el(Alert, {
				closeActionOverlay: () => { this.closeOverlay(); },
				resetAction: () => { this.closeOverlay(); },
				viewport,
				actionTitle: 'Operazione non valida',
				handleAction: errorOverlay,
				deleteError: null,
				actionTextRed: '',
				actionTextBlack: 'Il progetto selezionato non ha prodotti di visibilità.',
				icon: '/admin/assets/img/icons/denied.svg',
				alertHeight: 300,
				hideConfirm: true,
			}) : null,
			el(Contacts, contactsTexts)
		);
	}
}

Listings.propTypes = {
	actions: PropTypes.instanceOf(Object).isRequired,
	dataObject: PropTypes.instanceOf(Object).isRequired,
};

export default Listings;
