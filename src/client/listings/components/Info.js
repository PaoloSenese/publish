// @flow
import React from 'react';
import PropTypes from 'prop-types';
import ClickOutHandler from 'react-onclickout';

const { Fragment, createElement } = React;
const el = createElement;

class Info extends React.Component<any, any> {

	static defaultProps = {
		text: null,
	};

	state = {
		openInfo: false
	}

	openInfo(val: boolean) {
		this.setState({
			openInfo: val,
		});
	}

	render() {
		const { text } = this.props;
		const { openInfo } = this.state;

		return el(Fragment, {},
			el('div', { style: { float: 'right' } },
				el('svg', { onClick: () => { this.openInfo(true); }, width: 24, height: 24, viewBox: '0 0 24 24', style: { margin: '5px 10px', cursor: 'pointer' } },
					el('path', { d: 'M0 0h24v24H0z', fill: 'none' }),
					el('path', { d: 'M6 10c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-6 0c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z' }))),
			openInfo ?
				el(ClickOutHandler, { onClickOut: () => { this.openInfo(false); } },
					el('div', { className: 'profiled-overlay-container' },
						el('div', { className: 'profiled-overlay box-shadow noselect', style: { background: '#fff', marginTop: 30 } },
							el('div', { style: { fontWeight: 300, fontSize: 12, textAlign: 'justify', lineHeight: '16px' } }, text))))
				:
				null);
	}

}

Info.propTypes = {
	text: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.bool,
		PropTypes.instanceOf(Object)
	]),
};

Info.defaultProps = {
	text: null,
};

export default Info;
