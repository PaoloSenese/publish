// @flow
import { createElement } from 'react';
import PropTypes from 'prop-types';

const el = createElement;

const ItemViews = (props: Object) => {
	const { className, label, value, onClick, icon } = props;
	return el('div', { className, onClick: (o) => { onClick(o); } },
		el('div', { className: 'item-view-last' }, el('img', { src: icon, alt: '' })),
		el('div', { className: 'item-view-first' }, value),
		el('div', { className: 'item-view-central' }, label));
};

ItemViews.propTypes = {
	className: PropTypes.string,
	icon: PropTypes.string,
	label: PropTypes.string,
	value: PropTypes.string,
	onClick: PropTypes.func,
};

ItemViews.defaultProps = {
	className: '',
	icon: '',
	label: '',
	value: '',
	onClick: null,
};

export default ItemViews;
