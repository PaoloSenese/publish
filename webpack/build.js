import PluginError from 'plugin-error';
import log from 'fancy-log';
import webpack from 'webpack';
import makeWebpackConfig from './makeConfig';

const build = (done) => {
  const config = makeWebpackConfig({ isDevelopment: false });
  webpack(config, (fatalError, stats) => {
    const jsonStats = stats.toJson();

    // We can save jsonStats to be analyzed with
    // github.com/robertknight/webpack-bundle-size-analyzer.
    // $ webpack-bundle-size-analyzer ./bundle-stats.json
    // const fs = require('fs');
    // fs.writeFileSync('./bundle-stats.json', JSON.stringify(jsonStats));

    const buildError = fatalError || jsonStats.errors[0];
    if (buildError) {
      throw new PluginError('webpack', buildError);
    }

    log(`[webpack] ${stats}`);

    done();
  });
};

export default build;
