import path from 'path';
import constants from './constants';

const entries = (options) => {
  const { isDevelopment, serverIp } = options;

  const entryPoints = {
    listing: isDevelopment ? [
      'babel-polyfill',
      `webpack-hot-middleware/client?path=http://${serverIp}:${constants.HOT_RELOAD_PORT}/__webpack_hmr`,
      path.join(constants.SRC_DIR, 'client/indexListing.js'),
    ] : [
      'babel-polyfill',
      path.join(constants.SRC_DIR, 'client/indexListing.js'),
    ],
    listings: isDevelopment ? [
      'babel-polyfill',
      `webpack-hot-middleware/client?path=http://${serverIp}:${constants.HOT_RELOAD_PORT}/__webpack_hmr`,
      path.join(constants.SRC_DIR, 'client/indexListings.js'),
    ] : [
      'babel-polyfill',
      path.join(constants.SRC_DIR, 'client/indexListings.js'),
    ],
  };

  return entryPoints;
};

export default entries;
