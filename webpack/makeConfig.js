import ExtractTextPlugin from 'extract-text-webpack-plugin';
import WebpackIsomorphicToolsPlugin from 'webpack-isomorphic-tools/plugin';
import UglifyJsPlugin from 'uglifyjs-webpack-plugin';
import autoprefixer from 'autoprefixer';
import webpack from 'webpack';
import path from 'path';
import constants from './constants';
import webpackIsomorphicAssets from './assets';
import entries from './entries';

const webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(webpackIsomorphicAssets);

// cheap-module-eval-source-map, because we want original source, but we don't
// care about columns, which makes this devtool faster than eval-source-map.

// const devtools = 'eval-source-map';
const devtools = 'cheap-module-source-map';

const loaders = {
  css: '',
  scss: '!sass-loader',
  sass: '!sass-loader?indentedSyntax',
};

const serverIp = process.env.HOT_RELOAD_URL || '127.0.0.1';

const makeConfig = (options) => {
  const { isDevelopment } = options;

  const stylesLoaders = Object.keys(loaders).map((ext) => {
    const prefix = 'css-loader!postcss-loader';
    const extLoaders = prefix + loaders[ext];
    const loader = isDevelopment
      ? `style-loader!${extLoaders}`
      : ExtractTextPlugin.extract({ fallback: 'style-loader', use: extLoaders });
    return {
      loader,
      test: new RegExp(`\\.(${ext})$`),
      exclude: /\leaflet.css$/,
    };
  });

  const config = {
    mode: isDevelopment ? 'development' : 'production',
    stats: 'verbose',
    optimization: {
      splitChunks: false,
      minimizer: !isDevelopment ? [new UglifyJsPlugin({
        uglifyOptions: {
          compress: true,
          screw_ie8: true, // eslint-disable-line camelcase
          warnings: false, // Because uglify reports irrelevant warnings.
        },
      })] : [],
    },
    cache: isDevelopment,
    devtool: isDevelopment ? devtools : '',
    entry: entries({ isDevelopment, serverIp }),
    module: {
      rules: [
        {
          loader: 'url-loader',
          test: /\.(gif|jpg|png|svg)(\?.*)?$/,
          options: {
            limit: 10000,
          },
        },
        {
          loader: 'url-loader',
          test: /favicon\.ico$/,
          options: {
            limit: 1,
          },
        },
        {
          loader: 'url-loader',
          test: /\.(ttf|eot|woff|woff2)(\?.*)?$/,
          options: {
            limit: 100000,
          },
        },
        {
          loader: 'babel-loader',
          test: /\.(js|jsx)$/,
          exclude: constants.NODE_MODULES_DIR,
          options: {
            cacheDirectory: true,
            presets: [['env', { modules: false }], 'react', 'stage-1'],
            env: {
              production: {
                plugins: ['transform-react-constant-elements', 'babel-plugin-syntax-dynamic-import'],
              },
							development: {
								plugins: ['transform-react-constant-elements', 'babel-plugin-syntax-dynamic-import'],
							}
            },
          },
        },
        {
          test: /\leaflet.css$/,
          use: [
            { loader: 'style-loader' },
            { loader: 'css-loader' }
          ]
        },
        ...stylesLoaders,
      ],
    },
    output: isDevelopment
      ? {
        path: constants.BUILD_DIR,
        filename: '[name].js',
        chunkFilename: '[name]-[chunkhash].js',
        publicPath: `http://${serverIp}:${constants.HOT_RELOAD_PORT}/build/`,
      }
      : {
        path: constants.BUILD_DIR,
        filename: '[name]-[chunkhash].js',
        chunkFilename: '[name]-[chunkhash].js',
        publicPath: '/admin/',
      },
    plugins: (() => {
      const plugins = [
        new webpack.LoaderOptionsPlugin({
          minimize: !isDevelopment,
          debug: isDevelopment,
          // Webpack 2 no longer allows custom properties in configuration.
          // Loaders should be updated to allow passing options via loader options in module.rules.
          // Alternatively, LoaderOptionsPlugin can be used to pass options to loaders
          hotPort: constants.HOT_RELOAD_PORT,
          postcss: () => [autoprefixer({ browsers: 'last 3 version' })],
        }),
        new webpack.DefinePlugin({
          'process.env': {
            IS_BROWSER: true, // Because webpack is used only for browser code.
            IS_SERVERLESS: JSON.stringify(process.env.IS_SERVERLESS || false),
            NODE_ENV: JSON.stringify(
              isDevelopment ? 'development' : 'production',
            ),
          },
        }),
				new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /it/)
      ];
      if (isDevelopment) {
        plugins.push(
          new webpack.HotModuleReplacementPlugin(),
          new webpack.NoEmitOnErrorsPlugin(),
          webpackIsomorphicToolsPlugin.development()
        );
      } else {
        plugins.push(
          new ExtractTextPlugin({
            filename: '[name]-[hash].css',
            disable: false,
            allChunks: true,
          }),
          webpackIsomorphicToolsPlugin
        );
      }
      return plugins;
    })(),
    performance: {
      hints: false,
    },
    resolve: {
      extensions: ['.js'],
      modules: [constants.SRC_DIR, 'node_modules'],
      alias: {
        react$: require.resolve(path.join(constants.NODE_MODULES_DIR, 'react')),
      },
    },
  };

  return config;
};

export default makeConfig;
