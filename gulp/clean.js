import del from 'del';
import gulp from 'gulp';

gulp.task('clean', () => del(['build/*', 'errorShots', 'v8-compile*', 'ts-node-*']));
